#!/usr/bin/env python3

from pathlib import Path
from sys import argv

import ruamel.yaml

from config import CACHE_DIR

PROJECT_PATH = Path(__file__).parent.absolute()

CLUSTER_YAML = f"""
cluster_name: florida_link

autoscaling_mode: default

max_workers: 1000
min_workers: 1000
initial_workers: 1000

provider:
  type: local
  head_ip: localhost
  worker_ips:

auth:
  ssh_user:
  ssh_private_key: "~/.ssh/id_rsa"

docker:
  image: python:latest
  container_name: florida_link
  head_run_options:
    - "--shm-size 8G"
    - "-v /tmp/florida_data/app/:/app/"
    - "-v /tmp/florida_data/config.py:/config.py"
    - "-v /tmp/florida_data/Pipfile:/Pipfile"
    - "-v {PROJECT_PATH}:/files/"
    - "-v {CACHE_DIR}:/cache/"
    - "-e FLORIDALINK_DIR=/cache"
    - "-e PYTHONPATH=/"
  worker_run_options:
    - "--shm-size 8G"
    - "-v /tmp/florida_data/app/:/app/"
    - "-v /tmp/florida_data/config.py:/config.py"
    - "-v /tmp/florida_data/Pipfile:/Pipfile"
    - "-e PYTHONPATH=/"
  pull_before_run: True

head_setup_commands:
  - "apt update -y"
  - "apt install -y rsync"
  - "pip install pipenv"
  - "pipenv install --skip-lock && pipenv run pip freeze | xargs pip install"

worker_setup_commands:
  - "pip install pipenv"
  - "pipenv install --skip-lock && pipenv run pip freeze | xargs pip install"

head_start_ray_commands:
  - "ray stop"
  - "ulimit -c unlimited && ray start --include-webui True --head --port=6379 --autoscaling-config=~/ray_bootstrap_config.yaml"

file_mounts:
  /tmp/florida_data/app/: {PROJECT_PATH}/app
  /tmp/florida_data/config.py: {PROJECT_PATH}/config.py
  /tmp/florida_data/Pipfile: {PROJECT_PATH}/Pipfile
"""

if __name__ == "__main__":
    try:
        _, ssh_user, *worker_ips = argv
    except:
        print(
            f"Usage: ./{Path(__file__).name} SSH_USER WORKER_IP_OR_HOSTNAME1 WORKER_IP_OR_HOSTNAME2 ..."
        )
        exit(1)
    cluster_config = ruamel.yaml.round_trip_load(CLUSTER_YAML)
    cluster_config["auth"]["ssh_user"] = ssh_user
    cluster_config["provider"]["worker_ips"] = worker_ips

    with open("cluster.yaml", "w") as file:
        ruamel.yaml.round_trip_dump(cluster_config, file, indent=2, block_seq_indent=2)
