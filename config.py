import logging
from os import environ
from pathlib import Path

log_format = "{asctime} {levelname} [{name}] {message}"
datefmt = "%H:%M:%S"
logging.basicConfig(format=log_format, datefmt=datefmt, style="{")

CACHE_DIR = Path(environ.get("FLORIDALINK_DIR") or "~/.cache").expanduser()

ROOT = CACHE_DIR / "link"
DATA_URL = "http://flvoters.com/download/"
