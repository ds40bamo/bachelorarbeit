duplicated or missing information will produce incorrect or misleading statistics (”garbage in, garbage out”)
wide range of possible data inconsistencies
often a significant portion of the cleaning and transformation work has to be done manually or by low-level programs that are difficult to write and maintain
typically a wrapper per data source for extraction and a mediator for integration

data cleaning approach should satisfy several requirements:
    - should detect and remove allmajor errors and inconsistencies
    - should not be performed in isolation but together with schema-related data transformations based on comprehensive metadata
    - mapping functions for data cleaning and other data transformations should be specified in a declarative way and be reusable for other data sources

single-source problems:
    - depends on schema and integrity constraints controlling permissible data values
    - no schema -> few restrictions on what data can be entered and stored -> high probability of errors and inconsistencies
    - schema problems because lack of appropriate model or application integrity constraints
    - instance-specific problems because errors and inconsistencies, which cannot be prevented at the schema level
    - problem scopes:
        - attribute (field)
            - missing values
            - misspellings
            - abbreviations
            - misfielded values
        - record
            - violated attribute dependencies
        - record type
            - word transpositions
            - duplicated records
            - contradicting records
        - source
            - wrong references
    - uniqueness constraints specified at the schema level do not prevent duplicated instance

multi-source problems:
    - data in the sources may be represented differently, overlap or contradict -> large degree of heterogeneity
    - schema problems:
        - naming conflicts -> the same name is used for different objects (homonyms) or different names are used for the same object (synonyms)
        - structural conflicts -> different representations of the same object in different sources, (e.g. attribute vs. table representation)
    - instance problems (all of single-source):
        - different value representations
        - different interpretation of the values
        - information provided at different aggregation levels (e.g., sales per product vs. sales per product group)
        - information refer to different points in time
    - major problem:
        - identify overlapping data (object identity problem)
        - duplicate information should be purged out and complementing information should be consolidated and merged
    - schema level:
        - name conflicts (Customer/Client, Cid/Cno, Sex/Gender)
        - structural conflicts (different representations for names and addresses)
    - instance level:
        - different gender representations (”0”/”1” vs.  ”F”/”M”)
        - duplicate record

data cleaning approaches:
    - data analysis:
        - manual inspection
        - metadata
        - detect data quality problems
        - data profiling:
            - derives information (data type, length, value range,discrete values and their frequency, variance, uniqueness, occurrence of null values, typical string pattern)
        - data mining:
        - discover specific data patterns in large data sets (relationships holding between several attribute)
    - definition of transformation workflow and mapping rules
        - schema translation
    - verification
    - transformation
    - backflow of cleaning data
    - conflict resolution:
        - extracting values from free-form attributes
        - validation and correction
            - dictionary lookup
        - standardization
            - convert attribute values to consistent and uniform format
        - duplicate eliminationtask is typically performed after most other transformation and cleaning steps


