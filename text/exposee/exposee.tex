\documentclass{article}


\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{textcomp}
\usepackage{libertine}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{sidecap}
\usepackage{float}
\usepackage{cite}
\usepackage[nottoc,notlot,notlof]{tocbibind}
\usepackage{listings}
\usepackage{amsthm}


\newtheorem{exmp}{Beispiel}


\title{Exposee zur Bachelorarbeit \\ Erschließung und Evaluierung von Matching-Datensätzen mit personenbezogenen Daten}
\author{Dominik Schwabe}
\date{\today}

\sloppy
\hyphenpenalty=10000

\begin{document}
  \maketitle
  \newpage
  \tableofcontents
  \newpage

  \section{Einleitung}
    Heutzutage werden Daten an vielen Orten von verschiedenen Institutionen gespeichert.
    Um besser Analysen durchführen zu können, sollen diese Daten häufig zusammengeführt werden.
    Dabei müssen in den Datenbasen Einträge erkannt werden, welche auf dieselbe Entität verweisen, damit Duplikate eliminiert bzw. die Information der Einträge zusammengeführt werden können.
    Dies ist keine triviale Aufgabe, da häufig keine eindeutige Kennung existiert und die Datenbasen verschiedene Datenschemen und Formate verwenden.
    Zusätzlich können die Daten falsch oder zum Beispiel durch Rechtschreibfehler oder alternative Schreibweisen verunreinigt sein.
    Diese Aufgabe ist als ``record linkage'', ``merge/purge processing'' oder auch ``object identity problem'' bekannt.

    Häufig soll solch ein Verfahren für die Erhebung von Statistiken, auf vertrauliche Daten angewendet werden.
    Zum Beispiel können die Daten eines Finanz- und eines Gesundheitsinstituts genutzt werden, um Zusammenhänge zwischen den finanziellen Mitteln und dem Gesundheitszustands von Personen zu erkennen.
    Dabei sind die sensiblen Daten der Personen, wie Name und Adresse, für die statistische Auswertung unwichtig, aber für einen qualitativ hochwertigen Linkage-Prozess unabdingbar.
    Der Austausch der vertraulichen Daten verstößt dabei gegen Datenschutzgesetze, wie zum Beispiel das Bundesdatenschutzgesetz\footnote{https://www.datenschutz.org/bdsg/ [zugegriffen: 02.06.2019]} (BSDG) in Deutschland, die EU-Datenschutzgrundverordnung\footnote{https://www.datenschutz.org/eu-datenschutzgrundverordnung/ [zugegriffen: 02.06.2019]} (EU-DSGVO) für die Europäische Union oder ``The Act on the Protection of Personal Information''\footnote{https://www.dlapiperdataprotection.com/index.html?t=law&c=JP&c2= [zugegriffen: 02.06.2019]} (APPI) in Japan.
    Um dennoch diese Daten für den Linkage-Prozess nutzen zu können, müssen sie in ein für den Prozess geeignete Transformation gebracht werden, welche keinen Rückschlüsse auf die ursprünglichen Daten zulässt\cite{pprlHandbook}.

    Linkage-Verfahren, bei welchen verschlüsselte Attribute als Grundlage für das Matching dienen, werden ``privacy-preserving record linkage'' (PPRL) genannt und sind Gegenstand aktueller Forschung.
    So beschäftigt sich unter anderem auch das Big Data center ScaDS Dresden/Leipzig mit der Entwicklung von Ansätzen, bei welchem zusätzlich personenbezogene Daten vertraulich behandelt werden müssen\cite{pprlFranke}.

    Ziel ist der Einsatz dieser Verfahren im Bereich der Medizin, um Zusammenhänge zwischen Krankheiten und Lebensstilen erkennen zu können.
    Vor der Anwendung müssen die Verfahren allerdings ausreichend getestet und im Hinblick auf verschiedene Aspekte, wie Skalierbarkeit, Effizienz und Qualität evaluiert werden.
    Dafür sind umfangreiche und möglichst echte Datensätze notwendig, welche zudem domänenspezifische Merkmale, wie Tippfehler, fehlende Werte und Duplikate besitzen.

    Solche Daten sind auf Grund des Datenschutzes nur sehr beschränkt verfügbar und können auch nicht von den Institutionen für Testzwecke ausgehändigt werden.
    Eine Lösung für dieses Problem ist die Generierung der Daten anhand von Statistiken und dem bewussten Einbauen von Fehlern\cite{syntheticData}.
    Dabei ist es schwierig die Daten so zu generieren, dass die Verteilung und Verwendung von Attributwerten, sowie die Verteilung von Fehlern realistisch ist.

    Dagegen beschäftigt sich diese Arbeit mit der Suche und Beschaffung von geeigneten Datensätzen, sowie dem Linkage dieser Daten, zur Erstellung eines Matching-Datensatzes nach dem Stand aktueller Forschung.
  \section{Zielsetzung}
    Erste Aufgabe ist es online nach Datenbanken zu suchen, welche Personendaten frei zur Verfügung stellen, so wie möglichst die für das Linkage gewünschten Eigenschaften, wie Tippfehler, fehlende Werte, Duplikate und großer Umfang an Attributwerten besitzen.
    Zudem sollten die Datensätze möglichst umfangreich sein, um die Skalierbarkeit der PPRL-Verfahren testen zu können.
    Außerdem müssen die verwendeten Datensätze leicht zugänglich sein, damit die Erstellung des Matching-Datensatz ohne großen Aufwand reproduziert werden kann.
    Anschließend sollen die Datenbanken dahingehend geprüft werden, inwiefern sie den Anforderungen genügen und welche Paare von Datenbanken für ein Matching geeignet sind.
		Nach der Auswahl der Datenbanken, erfolgt die Erstellung einer Software, welche die Datensätze beschafft und extrahiert.
		Die Datensätze sollen zudem in ein einfaches und flexibles Format überführt werden, sowie nur die für das Linkage relevanten Attribute enthalten.
    Als zweite Aufgabe sollte eine geeignete Softwarelösung für das Linkage entworfen werden um so den Gold-Standard zu erhalten.
    Hierbei ist besonders die Qualität des Matchings von Bedeutung.
    Damit die Qualität der PPRL-Verfahren möglichst präzise ermittelt werden kann, sollen Statistiken über die erstellten Datensätze generiert werden.
		Dazu gehört die Ermittlung der Anzahl der Datensätze und Duplikate, sowie die Überlappungen, Art und Anzahl der Attribute.
		Zudem soll die Anzahl und Verteilung der Werte für die synthetische Datengeneration und die Anzahl fehlerhafter und fehlender Werte bestimmt werden.
		Gegebenenfalls erfolgt eine Gegenüberstellung mit bestehenden Datensätzen.
  \section{Zeitplan}
    \subsection*{Einarbeitung und Erstellung eines Entwurfs}
      Bis Anfang Juni 2019
    \subsection*{Implementierung}
      Bis Anfang September 2019
    \subsection*{Verfassen der Arbeit}
      Bis Anfang Oktober 2019
    \subsection*{Vortrag für Bachelor-Seminar}
      Anfang Juni 2019
  \newpage
  \bibliography{references}
%  \bibliographystyle{siam}
  \bibliographystyle{geralpha}
\end{document}

