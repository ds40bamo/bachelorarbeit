\chapter{Grundlagen}
\label{chap:basics}
\paragraph{Anmerkung}
Wenn keine ausdrückliche Quelle angegeben ist, so entstammen die Informationen dieses Kapitels dem Buch ``\citetitle{christen2012concepts}''~\cite{christen2012concepts}.

Duplikateneliminierung bezeichnet das Problem der Identifizierung, Zusammenführung und Vereinigung von Datensätzen in einer oder mehreren Datenbanken, welche sich auf dieselbe Entität beziehen~\cite{dunn1946record,fellegi1969theory}.
Das Problem der Duplikateneliminierung ist unter dem Namen ``record linkage'', ``entity resolution'' oder auch ``object identification'' bekannt.
Beispiele für Entitäten sind hierbei Personen, Produkte, Dokumente oder Geschäfte.

Deduplizierung bezeichnet den Sonderfall, bei der die Duplikateneliminierung nur auf eine Datenbank angewendet wird.

Ein Duplikat ist ein Paar $(u, v)$ von Datensätzen, für das gilt:
  \begin{itemize}
    \item $u$ ist nicht $v$
    \item $u$ und $v$ beziehen sich auf dieselbe Entität
  \end{itemize}

Folgende Probleme sind bei der Duplikateneliminierung in Datenbanken zu berücksichtigen:

\paragraph{Das Fehlern einer eindeutiger Kennung}
Die Datenbanken besitzen meist keine übergreifend eindeutige Kennung, welche die Zusammenführung der Datensätze ermöglicht.
Selbst bei Vorliegen von eindeutigen Kennungen ist Duplikatenfreiheit nicht gewährleistet.
Als Folge dessen muss die Zusammenführung anhand von Attributen entschieden werden, welche die Entitäten beschreiben (\zB Name, Adresse, Geburtsdatum).
Dabei wird angenommen, dass Datensätze mit ähnlichen Attributen eher zusammengehören als Datensätze mit unterschiedlichen Attributen.
Eine zusätzliche Herausforderung ist dabei, dass die Attribute einer Entität sich in verschiedenen Datensätzen, die zu dieser gehören, unterscheiden können, da Informationen veraltet, unvollständig oder durch Fehler verunreinigt sein können (\zB Änderung der Adresse durch Umzug, Änderung des Nachnamens durch Heirat).

\paragraph{Berechnungskomplexität}
Um bei der Duplikateneliminierung für jedes Paar von Datensätzen die Entscheidung treffen zu können, ob es sich um ein Duplikat handelt oder nicht, muss potenziell jeder Datensatz einer Datenbank mit jedem Datensatz der anderen Datenbank verglichen werden.
Die Komplexität der Berechnung ist demzufolge quadratisch zu der Größe der Datenbanken, also $\mathcal{O}(n \cdot m)$ (mit $n$ und $m$ entsprechend die Anzahl der Datensätze in den Datenbanken).
Besonders ist hierbei, dass die Anzahl der wahren Duplikate im Allgemeinen nur linear mit der Große der kleineren Datenbank wächst, also $\mathcal{O}(\min(n, m))$.
Demzufolge lässt sich eine Reduzierung der Komplexität durch die effiziente Entfernung von Datensatzpaaren erreichen, bei denen es sich nur sehr unwahrscheinlich um Duplikate handelt.

\paragraph{Fehlender Goldstandard}
Ob zwei Datensätze Duplikate sind, lässt sich in der Regel nicht eindeutig bestimmen, da für die Entscheidung nur Attribute genutzt werden können, welche die Entitäten oberflächlich beschreiben.
Die Entscheidung, ob zwei Datensätze Duplikate sind, hängt stark von der verfügbaren Information und der potenziellen Anzahl von Duplikaten sowie dem Klassifikator ab, der die Entscheidung trifft.
Ein Klassifikator, der auf einem Datensatz gute Ergebnisse liefert, kann auf einem anderen schlechte Ergebnisse liefern.

\paragraph{Schemamatching}
Bevor man Datenbanken deduplizieren kann, müssen zusammengehörige Attribute identifiziert werden.
Dabei können sich Attributnamen, Attributformate und Wertebereiche unterscheiden (\zB Sex vs.\ Gender, DD/MM/YYYY vs.\ MM/DD/YYYY, 0/1 vs.\ m/w).
Zudem können Informationen auf verschiedene Felder aufgespalten oder in einem vereinigt sein (\zB full\_name vs.\ first\_name | middle\_name | last\_name).

\begin{figure}
	\centering
	\recordLinkage
	\caption{Prozess der Duplikateneliminierung}
	\label{fig:process_dedupe}
\end{figure}

\paragraph{Prozess der Duplikateneliminierung}
Abbildung~\ref{fig:process_dedupe} zeigt den Prozess der Duplikateneliminierung.
Die Datenbanken werden zuerst vorverarbeitet, um Fehler zu korrigieren, Schemamatching durchzuführen und die Daten zu standardisieren.
Im Indexing-Schritt werden viele Paare gefiltert, die keine Duplikate sind.
Ungefilterte Paare werden anschließend verglichen und klassifiziert.
Paare, bei denen sich der Klassifikator unsicher ist, können manuell überprüft werden.
Optional erfolgt am Ende eine Evaluation, bei der die Qualität der Duplikateneliminierung beurteilt wird.
Die einzelnen Schritte werden in den folgenden Kapiteln ausführlich beschrieben.

\newpage
\section{Datenvorverarbeitung}
Bevor die Daten dedupliziert werden können, müssen diese angemessen gesäubert und standardisiert sowie Inkonsistenzen angemessen aufgelöst werden.
Die Notwendigkeit der Vorverarbeitung ergibt sich durch das zufällige und systematische Auftreten von Fehlern bei der Erhebung der Daten und der Verwendung unterschiedlicher Datenbankenschemas.
Beispielsweise können Inhalte von geschriebenen Formularen mittels optischer Zeichenerkennung oder durch Abtippen mittels Tastatur in digitale Formate überführt werden.
Hierbei können Fehler vermehrt auftreten, wenn die erhoben Daten mündlich übermittelt werden.
Je nach Eingabemethode können unterschiedliche Fehler statistisch unterschiedlich häufig auftreten.
Die Kenntnis der benutzten Eingabemethode kann hierbei die Berichtigung der Fehler erleichtern.
Besonders schwierig wird die Berichtigung, wenn die Daten über viele verschiedene Zentren über verschiedene Methoden erhoben wurden.
Der Prozess der Vorverarbeitung besteht im Allgemeinen aus den folgenden Schritten:
\begin{enumerate}
	\item Entfernen von Zeichen und Worten die keine Information tragen
	\item Berichtigung von Rechtschreibfehler und Auflösen von Abkürzungen
	\item Aufteilungen von komplexen Attributen in wohldefinierte atomare Attribute
	\item Überprüfung der Korrektheit von Attributwerten
\end{enumerate}

\newpage
\section{Indexing}
Bei der Duplikateneliminierung muss potenziell jeder Datensatz einer Datenbank mit jedem Datensatz der anderen Datenbank verglichen werden.
Die Komplexität der Berechnung ist demzufolge quadratisch zu der Größe der Datenbanken.
Dabei besitzt nur eine geringe Menge der Datensatzpaare überhaupt eine Ähnlichkeit in ihren Attributwerten und bei einer noch viel geringeren Menge handelt es sich tatsächlich um Duplikate.
Indexing-Techniken lösen dieses Problem, indem sie Paare herausfiltern, welche nur unwahrscheinlich Duplikate sind.
Dadurch wird die Anzahl der notwendigen Vergleiche stark reduziert, wobei nur eine vertretbar kleine Menge von Duplikaten gefiltert wird.
\subsection{Blocking}
Jeder Datensatz wird anhand von einem geeigneten Kriterium in einen oder mehrere Blöcke eingefügt.
Die Datenbanken werden dabei in kleinere Blöcke aufgeteilt.
Anschließend müssen nur noch die Datensätze miteinander verglichen werden, welche mindestens einmal in denselben Block eingefügt wurden.
Im Folgenden werden zwei häufig verwendete simple Blockingverfahren vorgestellt:
\paragraph{Standardblocking}
Jeder Datensatz wird anhand eines Blockingkeys in einen oder mehrere Mengen/Blöcke eingefügt.
Ein Blockingkey ist dabei ein Attribut, welches durch eine Transformation eines oder mehrerer Attribute erhalten wird.
Abbildung~\ref{fig:standardBlocking} zeigt ein Beispiel für das Standardblocking.
Wird zum Beispiel das Geburtsdatum als Blockingkey verwendet, so werden alle Datensätze, bei denen dasselbe Geburtsdatum angegeben wurde, in denselben Block eingefügt.
Folglich werden nur Paare verglichen, bei denen die Geburtsdaten übereinstimmen.
Die Größe der generierten Blöcke ist dabei stark abhängig von der Anzahl und der Verteilung der möglichen Attributwerte. \\
\begin{figure}[H]
	\centering
	\standardBlocking
	\caption{Beispiel für Standardblocking, die Datensätze mit gleichem Blockingkeywert werden in denselben Block eingefügt. Anschließend werden Kandidatenpaare daraus generiert.}
	\label{fig:standardBlocking}
\end{figure}
\paragraph{Sorted Neighborhood Blocking}
Die Datenbanken werden anhand eines Sortingkeys sortiert.
Ein Sortingkey ist dabei analog zum Blockingkey beim Standardblocking ein Attribut, welches durch eine Transformation eines oder mehrerer Attribute erhalten wird.
Anschließend werden alle Datensätze, welche sich in einer bestimmten Entfernung voneinander befinden miteinander verglichen.
Wird beispielsweise das Geburtsdatum als Sortingkey verwendet, so werden Datensätze, bei denen zeitlich nahe beieinander liegende Geburtsdaten angegeben wurden, miteinander verglichen.
Dadurch werden auch Duplikate gefunden, bei denen sich das Geburtsdatum aufgrund von Fehlern um wenige Tage unterscheidet.
\bigskip

Die Wahl des Blockingkeys beziehungsweise Sortingkeys ist abhängig von der Anzahl der fehlenden Werte und der Häufigkeit der Attributwerte.
Ist die Zahl der fehlenden Werte groß, so werden viele Duplikate gefiltert.
Tritt ein Attributwert besonders häufig auf, so werden viele Datensätze in denselben Block eingefügt und die eigentlich gewünschte Reduzierung der Vergleiche ist nur sehr gering.
Folglich gibt es einen Kompromiss zwischen der Anzahl und der Größe der generierten Blöcke.
Im Allgemeinen gilt es als bester Ansatz eine simple Blocking-Technik wie Standardblocking zu wählen und diese mehrmals für verschiedene Blockingkeys anzuwenden.
Dadurch erhöht sich die Wahrscheinlichkeit, dass Duplikate mindestens einmal in denselben Block eingefügt und somit später verglichen werden.

\subsection{Generierung von Blockingkeys}
Oft sind Transformationen der Attributwerte besser als Blockingkey geeignet als die Attributwerte selbst.
Dabei hängt die Wahl der Transformation von dem Attributtyp ab.
Zum Beispiel wird mit phonetischen Codierungen versucht, Worte, welche ähnlich ausgesprochen werden, auf denselben Wert abzubilden, um dadurch robuster gegenüber alternativen Schreibweisen zu sein.
\paragraph{Soundex}
Worte werden mittels Regeln in eine Form transformiert, welche ihre Aussprache basierend auf dem amerikanischen Englisch repräsentiert. \\
\begin{table}[H]
	\centering
	\begin{tabular}{lll}
		\toprule
		A, E, I, O, U, Y, H, W & $\rightarrow$ & 0 \\
		B, F, P, V             & $\rightarrow$ & 1 \\
		C, G, J, K, Q, S, X, Z & $\rightarrow$ & 2 \\
		D, T                   & $\rightarrow$ & 3 \\
		L                      & $\rightarrow$ & 4 \\
		M, N                   & $\rightarrow$ & 5 \\
		R                      & $\rightarrow$ & 6 \\
		\bottomrule
	\end{tabular}
	\caption{Soundex Ersetzungsregeln}
	\label{tab:soundex_substitution}
\end{table}

\begin{description}
	\item Regeln:
	      \begin{enumerate}
		      \item Ersetzte alle Buchstaben bis auf den ersten entsprechend der Regeln in Tabelle~\ref{tab:soundex_substitution}.
		      \item Entferne alle nullen (Vokale und vokalähnliche Zeichen).
		      \item Entferne alle Wiederholungen derselben Zahl.
		      \item Füge an das Ende des Codes Nullen an oder entferne Zahlen vom Ende, bis der Code aus 3 Zahlen besteht.
	      \end{enumerate}
\end{description}
Aufgrund der einfachen Regeln ist Soundex sehr effizient, aber auch sehr grob.
Zudem werden bei Unterschieden im ersten Buchstaben zwangsläufig unterschiedliche Codierungen generiert werden.
\paragraph{NYSIIS}
Ähnlich zum Soundex werden bei NYSIIS verschiedene Transformationsregel auf ein Wort angewandt.
Dabei besteht die resultierende Codierung vollständig aus Buchstaben.
Zudem werden sowohl einzelne Buchstaben als auch Gruppen von Buchstaben abhängig von ihrer Position (Wortanfang, Wortmitte, Wortende) ersetzt.
\paragraph{Double-Metaphone}
Durch diese Transformation werden auch Namen berücksichtigt, welche aus verschiedenen Sprachen stammen.
Ähnlich zum NYSIIS werden hierbei die Positionen von Buchstaben und Buchstabengruppen innerhalb des Wortes berücksichtigt.
Zudem wird eine zweite Codierung generiert, welche Alternative Schreibweisen berücksichtigt.

\newpage
\section{Vergleich der Datensätze}
Möchte man Duplikate finden, so müssen die Datensätze zwangsläufig miteinander vergleichen werden.
Dabei ist es plausibel, dass es sich bei zwei Datensätze, welche sich in vielen Attributen gleichen, um Duplikate handelt.
Wenn diese sich hingegen in vielen Attributen unterscheiden, so ist es plausibel, dass es sich bei ihnen nicht um Duplikate handelt.
Solch ein simpler Vergleich ist allerdings in den meisten Fällen nicht sinnvoll, da sich Attributwerte durch Fehler oder alternative Repräsentationsformen unterscheiden können.
Zum Beispiel ähneln sich die Namen ``Jacqueline'' und ``Jackelinne'' sehr. Die Namen ``Jacqueline'' und ``William'' sind dabei stark verschieden.
Wenn man in beiden Fällen die Namen miteinander auf einfache Gleichheit vergleicht, so ergibt sich der gleiche Wert.
Weiterhin können Werte über Zeit ihre Gültigkeit verlieren.
So kann eine Person zum Beispiel ihren Wohnort wechseln, wodurch sich ihre Adresse ändert.
Dabei ziehen die meisten Personen an einen Ort, der nahe an dem vorherigen liegt.
Um Werte besser vergleichen zu können, ist es sinnvoll, abhängig vom Datentyp geeignete Ähnlichkeits- beziehungsweise Distanzmaße zu definieren.
\paragraph{Ähnlichkeitsmaß}
Ein Ähnlichkeitsmaß $sim$ berechnet abhängig von den zu vergleichenden Ausdrücken einen Wert, der die Ähnlichkeit der beiden Ausdrücke auf eine bestimmte Weise beschreibt.
Dabei gilt $sim(x, y) = 1$, wenn $x$ und $y$ gleich sind und $sim(x, y) = 0$, wenn $x$ und $y$ stark verschieden sind.
In allen anderen Fällen wird ein Wert zwischen null und eins generiert.
Dabei sind die Werte ähnlicher, je näher der generierte Wert an eins liegt.
\paragraph{Distanzmaß}
Ähnlich wie Ähnlichkeitsmaße generiert ein Distanzmaß $dist$ abhängig von den zu vergleichenden Ausdrücken einen Wert.
Dabei ist dieser Wert nahe null, wenn die Ausdrücke ähnlich sind und eins oder eine positive Zahl fern von null, wenn die Werte verschieden sind.
Zusätzlich sind Distanzmaße symmetrisch ($dist(x,y) = dist(y,x)$) und erfüllen die Dreiecksungleichung ($dist(x,y) + dist(y,z) \geq dist(x,z)$).
\subsection{Vergleichsmaße}
\paragraph{Exakt}
Diese Maß ist wie folgt definiert:
\begin{equation}
	sim_{exakt}(x, y)=
	\begin{cases}
		1, & \text{falls } x = y \\
		0, & \text{sonst}
	\end{cases}
\end{equation}
\paragraph{Editierdistanz}
Dieses Distanzmaß ermittelt die minimalen Kosten, die notwendig sind, um eine Zeichenkette in eine andere umzuwandeln.
Dabei müssen vorher die möglichen Operationen zusammen mit ihren Kosten festgelegt werden.
\paragraph{Levenshtein-Distanz}
Als Spezialfall der Editierdistanz berechnet die Levenshtein-Distanz die minimale Anzahl an Editieroperationen (Kosten pro Operation: 1), die notwendig sind, um eine Zeichenkette in eine andere zu transformieren.
\begin{description}
	\item mögliche Editieroperationen:
	      \begin{itemize}
		      \item Einfügen eines Zeichens
		      \item Löschen eines Zeichens
		      \item Austausch eines Zeichens durch ein anderes
	      \end{itemize}
\end{description}
Um die Levenshtein-Distanz in ein Ähnlichkeitsmaß umzuwandeln wird folgende Transformation angewandt:
\begin{equation}
	sim_{levenshtein}(x, y) := 1.0 - \frac{dist_{levenshtein}(x, y)}{\max(|x|, |y|)}
\end{equation}
\paragraph{Damerau-Levenshtein Distanz}
Die Damerau-Levenshtein Distanz definiert zusätzlich zu den Operationen der Levenshtein-Distanz die Möglichkeit benachbarte Zeichen zu vertauschen (Kosten: 1).
Für die Umwandlung der Damerau-Levenshtein-Distanz in ein Ähnlichkeitsmaß gilt analog zur Levenshtein-Distanz:
\begin{equation}
	sim_{damerau-levenshtein}(x, y) := 1.0 - \frac{dist_{damerau-levenshtein}(x, y)}{\max(|x|, |y|)}
\end{equation}
\paragraph{Jaro und Jaro-Winkler Vergleichsfunktionen}
Diese Funktionen wurden speziell für den Vergleich von Namen entwickelt.
Die Jaro Funktion ist für beliebige Zeichenketten $x$ und $y$ wie folgt definiert:
\begin{equation}
	sim_{jaro}(x, y) = \frac{1}{3}\left(\frac{c}{|x|}+\frac{c}{|y|}+\frac{c - t}{c}\right)
\end{equation}
\begin{description}
	\item wobei gilt:
	      \begin{itemize}
		      \item $c$ ist die Anzahl der Zeichen, die gleich sind und maximal $\left\lfloor {\frac {\max(|x|,|y|)}{2}}\right\rfloor -1 $ Zeichen voneinander entfernt sind.
		      \item $t$ ist die Anzahl benachbarter Zeichen, die in den Zeichenketten vertauscht sind.
	      \end{itemize}
\end{description}
Die Jaro-Winkler Funktion erweitert die Jaro Funktion folgendermaßen:
\begin{itemize}
	\item Erhöhe die Ähnlichkeit abhängig von der Länge des längsten übereinstimmenden Präfixes der Zeichenketten.
	\item Erhöhe die Ähnlichkeit, wenn beide Zeichenketten länger als 5 Zeichen sind und ohne den längsten übereinstimmenden Präfix mindestens 2 Zeichen gleich sind und mindestens die Hälfte der übrigen Zeichen der kürzeren Zeichenkette übereinstimmen.
	\item Erhöhe die Ähnlichkeit, wenn die Zeichenketten Zeichen enthalten, die ähnlich sind und somit eher miteinander ersetzt werden.
\end{itemize}
\paragraph{Monge-Elkan[sim]}
Diese Funktion dient dem Vergleich von Listen von Token.
Dabei wird für jedes Token in der ersten Liste ein passendes Token in der zweiten Liste anhand eines zweiten Ähnlichkeitsmaßes $sim$ gesucht, welches $sim$ maximiert, wobei die Token der zweiten Liste mehrfach verwendet werden können.
Das Ähnlichkeitsmaß ergibt sich dann aus dem Durchschnitt der gefundenen Ähnlichkeiten.
Seien $x$ und $y$ Listen. Dann ist das Monge-Elkan-Ähnlichkeitsmaß folgendermaßen definiert:
\begin{equation}
	sim_{monge\_elkan[sim]}(x, y) = \frac{1}{|x|}\sum^{|x|}_{i=1}{\max^{|y|}_{j=1}}\ sim(x_i, y_j)
\end{equation}
Dabei bezeichnet $x_i$ das Token der Liste $x$ an Position $i$ und $y_j$ das Token der Liste $y$ an Position $j$.
Ein Nachteil des Monge-Elkan-Ähnlichkeitsmaßes ist, dass dieses nicht symmetrisch ist.
Wählt man beispielsweise $x = \text{[\textquotesingle{a}\textquotesingle]}$ und $y = \text{[\textquotesingle{a}\textquotesingle, \textquotesingle{b}\textquotesingle, \textquotesingle{c}\textquotesingle, \textquotesingle{d}\textquotesingle, \textquotesingle{e}\textquotesingle, \textquotesingle{f}\textquotesingle]}$ und $sim = sim_{exakt}$, so gilt:
\begin{equation}
	sim_{monge\_elkan[sim_{exakt}]}(x, y) = 1.0 \qquad\text{und}\qquad sim_{monge\_elkan[sim_{exakt}]}(y, x) = 0.167
\end{equation}
Eine einfache Möglichkeit die Symmetrie herzustellen ist die Anwendung folgende Transformation:
\begin{equation}
	sim_{symmetric\_monge\_elkan[sim]}(x, y) = \frac{1}{2}(sim_{monge\_elkan[sim]}(x,y) + sim_{monge\_elkan[sim]}(y,x))
\end{equation}
\paragraph{Datum}
Die Idee beim Vergleich von Daten ist es, die Differenz dieser in Tagen zu berechnen und anschließend durch eine maximal tolerable Zahl von Tagen $d_{max}$ zu teilen.
Das Ähnlichkeitsmaß ergibt sich dann folgendermaßen:
\begin{equation}
	sim_{date}(x, y) = 1.0 - \min\left(\frac{|x - y|}{d_{max}}, 1.0\right)
\end{equation}
Da Daten häufig in Formaten wie \zB MM/DD/YYYY dargestellt werden und diese variieren, werden für bestimmte Sonderfälle andere Werte berechnet:
\begin{itemize}
	\item Wähle einen festen Wert, wenn Monat und Tag vertauscht sind, aber die Daten im Jahr übereinstimmen (\zB $0.5$).
	\item Wähle einen festen Wert, wenn Tag und Jahr gleich sind und sich die Daten im Monat unterscheiden (\zB $0.75$).
\end{itemize}
\paragraph{Haversine-Distanz}
Dieses Distanzmaß berechnet die kürzeste Distanz zwischen zwei Punkten auf der Oberfläche der Erde bzw.\ einer Kugel, welche durch ihre Längen- und Breitengrade gegeben sind.
Der berechnete Wert liegt dabei zwischen null und eins.
Sei $x_{lat}$ der Breitengrad und $x_{lng}$ der Längengrad des Punktes $x$ und $y_{lat}$ der Breitengrad und $y_{lng}$ der Längengrad des Punktes $y$.
Die Haversine-Distanz wird durch folgende Formel berechnet:
\begin{multline}
	dist_{haversine}(x_{lat}, x_{lng}, y_{lat}, y_{lng}) = \frac{2}{\pi}\cdot\arcsin([\frac{1}{4}\sin^{2}(y_{lat} - x_{lat}) \\ + \frac{1}{4}\cos(x_{lat}) \cdot \cos(y_{lat}) \cdot \sin^{2}(y_{lng} - x_{lng})]^\frac{1}{2})
\end{multline}
\newpage
\section{Klassifikation}
Beim Vergleich der Datensätze wird für jedes Paar von Datensätzen ein Vergleichsvektor berechnet, welcher die berechneten Ähnlichkeiten enthält.
Für jedes Paar wird anhand des entsprechenden Vektors bestimmt, ob es sich um ein Duplikat handelt beziehungsweise jedes Paar wird einer der zwei Klassen ``Duplikat'' oder ``kein Duplikat'' zugewiesen.
Optional kann ein Paar der Klasse ``mögliches Duplikat'' zugeordnet werden.
Diese Vektoren müssen von einem zweiten besseren Klassifikator beziehungsweise von einem Menschen klassifiziert werden, welcher diese dann jeweils einer der beiden Klassen ``Duplikat''oder ``kein Duplikat'' zuweist.
\subsection{Überwachte Klassifikation}
Überwachte Klassifikatoren benötigen Trainingsdaten beziehungsweise eine repräsentative Menge von Vergleichsvektoren, für die der Duplikatenstatus bekannt ist.
Wenn die Qualität der Trainingsdaten gut ist, sind überwachte Klassifikatoren hinsichtlich der Qualität ihre Klassifizierung unüberwachten deutlich überlegen.

\paragraph{Anmerkung}
Wenn keine ausdrückliche Quelle angegeben ist, so entstammen die Informationen dieser Sektion dem Buch ``\citetitle{james2013introduction}''~\cite{james2013introduction}.

\subsubsection{Entscheidungsbäume}
Im Folgenden bezeichnet $[X_1, X_2, \ldots, X_p]$ den Vergleichsvektor für eine Paar $X$, und $X_{j}$ den Wert des $j$-ten Arguments.
Das Argument wird im Folgenden auch als Prediktor bezeichnet.
Zudem bezeichnet $T$ die Menge der Trainingsdaten der aktuellen Teilung.
\paragraph{Training}
Wähle von allen möglichen Kombinationen von Prediktoren $j$ und Werten $s$ die Kombination, bei der die Mengen $\{X \in T | X_{j} < s\}$ und $\{X \in T | X_{j} \geq s\}$ die größte Reduktion in einem geeigneten Kriterium zur Folge haben.
Diese binäre Teilung wird rekursiv auf die Menge der Trainingsdaten und die dabei entstehenden Mengen angewandt, bis alle entstandenen Mengen weniger als eine vorher definierte Zahl von Beobachtungen enthalten.
Der Vorgang kann als Baum dargestellt werden, wobei die Kanten die Teilungsregeln und die Knoten die Mengen darstellen, die sich bei rekursiven Anwenden der Regeln bis zu ihnen ergeben haben.
\paragraph{Kriterien}
Sei $K$ die Anzahl der Klassen und $\hat{p}_{mk}$ der Anteil der Trainingsdaten, welche zur $k$-ten Klasse gehören und in der $m$-ten Menge, die durch die Teilung entstanden ist sind.
Die folgenden Kriterien werden häufig benutzt, um die Güte einer Teilung zu beschreiben: \\
\\
\phantom{5mm}\textbf{Gini-Koeffizient}
\begin{equation}
	G = \sum_{k=1}^{K}\hat{p}_{mk}(1-\hat{p}_{mk})
\end{equation}
\phantom{5mm}\textbf{Entropie}
\begin{equation}
	D = - \sum_{k=1}^{K}\hat{p}_{mk}\log\hat{p}_{mk}
\end{equation}
\paragraph{Klassifizierung}
Auf eine Beobachtung werden dieselben Regeln rekursiv angewandt, welche auf die Trainingsdaten beim Trainings des Baums angewandt wurden.
Dadurch wandert die Beobachtung im Baum hinab, bis sie ein Blatt erreicht.
Anschließend wird ihr die häufigste Klasse der Trainingsinstanzen zugewiesen, welche sich am Ende des Trainings in diesem Blatt befanden.
\subsubsection{Random-Forest}
\paragraph{Training}
Es werden mehrere Entscheidungsbäume trainiert.
Jeder Baum erhält dabei eine zufällige Teilmenge der Trainingsdaten.
Außerdem werden beim Training eines Baumes bei jeder Teilung der aktuellen Menge nur eine Teilmenge aller Prediktoren berücksichtigt.
Diese Teilmenge wird für jede Teilung zufällig ausgewählt, wobei die Größe der Teilmenge vor dem Training festgelegt wird.
\paragraph{Klassifizierung}
Der Beobachtung wird von jedem Entscheidungsbaum eine Klasse zugewiesen.
Dabei erhält die Beobachtung am Ende die Klasse, die ihr am häufigsten zugewiesen wurde.
\subsection{Active-Learning}
Sind keine Trainingsdaten vorhanden, um einen Klassifikator zu trainieren, können diese gewonnen werden, indem man eine Teilmenge der Paare, welche klassifiziert werden sollen, manuell klassifiziert.
Diese Teilmenge sollte möglichst umfangreich und repräsentativ für den Datensatz sein.
Dabei ist die Entscheidung nur für wenige Paare schwer und für viele eindeutig.
Um die manuelle Klassifizierung zu beschleunigen, kann nebenbei ein Modell trainiert werden, welches anhand der bereits klassifizierten Paare schwer zu klassifizierende Kandidaten auswählt, die dann manuell klassifiziert werden müssen.
Dieses Modell kann dann genutzt werden, um eine Entscheidung für die restlichen Paare zu treffen.
\begin{description}
	\item \textbf{Schritte}
	      \begin{enumerate}
		      \item Trainiere ein Modell auf den bisherigen Trainingsdaten.
		      \item Klassifiziere alle Instanzen, welche noch nicht in das Modell aufgenommen wurden.
		      \item Ein menschlicher Nutzer klassifiziert Instanzen, für die die Entscheidung am schwierigsten war.
		      \item Füge die klassifizierten Instanzen zur Trainingsmenge hinzu.
		      \item Gehe zu Schritt 1 oder breche ab, wenn ein bestimmtes Kriterium erreicht ist.
	      \end{enumerate}
\end{description}
\subsection{Transitive Hülle}
Wurden Paare $(x, y)$ und $(y, z)$ als Duplikat klassifiziert, aber das Paar $(x, z)$ nicht, so widerspricht dies der Intuition.
Die Abbildung~\ref{fig:transitive_closure} stellt ein Beispiel für diese Inkonsistenz dar.
Die gestrichelten Kanten müssten existieren, damit diese Inkonsistenz gelöst wäre.
Alternativ könnten Kanten entfernt werden, um die Inkonsistenz zu lösen.
Demzufolge sollte für einige Paare der Duplikatenstatus so geändert werden, sodass keine solche Widersprüche mehr bestehen. \\
Die Wahl eines Algorithmus für die Lösung dieses Problems ist davon abhängig, ob es Beschränkungen für die maximale Anzahl an Duplikate gibt, die pro Eintrag möglich sind.\\
\begin{figure}[H]
	\centering
	\transitiveClosure
	\caption{Beispiel für ein Ergebnis einer Klassifizierung, die Zahlen an den Kanten gibt an, als wie ähnlich die Einträge von dem Klassifikator eingestuft wurden. Gestrichelte Kanten bedeuten, dass der Klassifikator diese als kein Duplikat klassifiziert hat.}
	\label{fig:transitive_closure}
\end{figure}
\subparagraph{Einschränkungen}
\begin{itemize}
	\item \textbf{one-to-one}: Ein Eintrag in Datenbank 1 hat maximal ein Duplikat in Datenbank 2 und umgekehrt.
	\item \textbf{one-to-many}: Ein Eintrag in Datenbank 1 hat maximal ein Duplikat in Datenbank 2, aber ein Duplikat in Datenbank 2 kann mehrere Duplikate in Datenbank 1 besitzen.
	\item \textbf{many-to-many}: Ein Eintrag in Datenbank 1 kann mehrere Duplikate in Datenbank 2 besitzen und umgekehrt.
\end{itemize}
Folgende algorithmischen Ansätze sind gängige Lösungsmöglichkeiten des Problems:
\subsubsection{greedy}
Die Kandidatenpaare werden anhand ihrer Ähnlichkeit sortiert.
Danach wird die List sequenziell von dem Paar mit der höchsten Ähnlichkeit abgearbeitet.
Dabei wird ein Paar aufgenommen, wenn keines der beiden Einträge bereits über ein anderes Paar aufgenommen wurde.
Dadurch gibt es für jeden Eintrag maximal einen anderen Eintrag, sodass diese als Duplikat klassifiziert wurden.
Bei diesem Ansatz ist es problematisch, dass das Ergebnis vom Sortieralgorithmus und von der Position der Paare in der Eingabeliste abhängig ist, da es undefiniert ist, an welcher Stelle sich Paare mit der gleichen Ähnlichkeit in der sortierten Liste befinden.
Wendet man diesen Ansatz für das Beispiel in Abbildung~\ref{fig:transitive_closure} an, so wird das Paar (e2, e3) als Duplikat klassifiziert.
Alle anderen Paare werden als kein Duplikat klassifiziert.
\subsubsection{optimal}
Ähnlich wie beim greedy-Ansatz wird das Problem gelöst, indem für jeden Eintrag maximal ein anderer Eintrag zulässig ist, sodass diese als Duplikat klassifiziert werden.
Dabei ist es das Ziel, die Paare so auszuwählen, dass die Summe der Ähnlichkeiten maximiert wird.
Der gängigste Lösungsalgorithmus für dieses Problem ist der Hungarian-Algorithmus~\cite{kuhn1955hungarian}.
Wendet man diesen Ansatz für das Beispiel in Abbildung~\ref{fig:transitive_closure} an, so werden die Paare (e1, e2) und (e3, e4) als Duplikat klassifiziert.
Alle anderen Paare werden als kein Duplikat klassifiziert.

\subsubsection{CS-SN Ansatz}
\begin{algorithm}
	\caption{\textsc{CS-SN}}
	\label{alg:CSSN}
	\SetAlgoLined
	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
	\Input{Instanzen $R$\\symmetrische Distanzfunktion $d$\\positive Konstante $c$\\positive natürliche Zahl $K$}
	\Output{Menge von Teilmengen von $R$}
	\BlankLine
	\Begin{
		\nl$F \leftarrow \{e \in R \mid ng(e) > c \}$ \\
		\nl$C \leftarrow \{\{e\} \mid e \in F\}$ \\
		\nl$L \leftarrow R \setminus F$ \\
		\nl\While{$|L| > 0$}{
			\nl{}wähle eine Element $e$ aus $L$ \\
			\nl$S \leftarrow \{e\}$ \\
			\nl$best \leftarrow S$ \\
			\nl\While{$|L \setminus S| > 0$ \normalfont{\textbf{ and }} $|S| < K$ \normalfont{\textbf{ and }} $S \cap F = \emptyset$}{
				\nl$best \leftarrow S$ \\
				\nl$oldS \leftarrow S$ \\
				\nl$S \leftarrow S \cup \{\argmin_{x \in R \setminus S} d(e, x)\}$ \\
				\nl\While{$oldS \neq S$}{
					\nl$oldS \leftarrow S$ \\
					\nl$S \leftarrow \{x \in R \mid \exists y \in S \colon d(x, y) \leq \max_{z \in S}{d(y, z)} \}$
				}
			}
			\nl$C \leftarrow C \cup \{best\}$ \\
			\nl$L \leftarrow L \setminus best$ \\
		}
		\nl\Return $C$
	}
\end{algorithm}
Dieser Ansatz~\cite{chaudhuri2005robust} kann bei einer many-to-many Einschränkung angewendet werden.
Der Vorteil dieses Ansatzes gegenüber anderen Ansätzen, welche bei einer many-to-many Einschränkung angewendet werden liegt darin, dass die Cluster nicht anhand eines globalen Schwellwerts, sondern basierend auf allen Ähnlichkeitswerten und der lokalen Umgebung eines Paares gebildet werden.
Er basiert auf den folgenden zwei Beobachtungen:
\begin{itemize}
	\item Instanzen die sich alle auf dieselbe Entität beziehen, sind näher zueinander, als zu anderen Instanzen.
	\item Die Umgebung von Duplikaten ist spärlich.
\end{itemize}
Aus diesen Beobachtungen können zwei Kriterien abgeleitet werden.
Sei $R$ die Menge aller Instanzen und $d: R \times R \rightarrow [0, 1]$ eine symmetrische Distanzfunktion.
Für $x \in R$ sei $nn(x) = \min_{y \in R \setminus \{x\}}{d(x, y)}$ die Distanz zwischen $x$ und seinem nächsten Nachbarn und $ng(x) = |\{y \in R \mid d(x, y) < p \cdot nn(v)\}|$, die Anzahl der Instanzen um $x$ innerhalb eines Radius von $p \cdot nn(x)$, wobei $p$ eine Konstante größer eins ist.
Außerdem sei $agg$ eine Aggregatsfunktion und $c$ eine Konstante größer null.
\paragraph{kompakte Menge}
$S \subseteq R$ ist eine \emph{kompakte Menge}, wenn für alle beliebigen Instanzen $x, y \in S$ und $z \in R \setminus S$ gilt:
\begin{equation}
	d(x, y) < d(x, z)
\end{equation}
\paragraph{spärliche Nachbarschaft}
Eine Menge $S$ von Instanzen ist \emph{spärlich}, wenn eine der folgenden Eigenschaften gilt:
\begin{itemize}
	\item $|S| = 1$
	\item $agg(\{ng(x) \mid x \in S\}) < c$
\end{itemize}
\bigskip
\paragraph{Problemformulierung}
Seien Instanzen $R$, symmetrische Distanzfunktion $d$, Aggregationsfunktion $agg$, eine positive Konstante $c$ und eine positive natürliche Zahl $K$ gegeben.
Zerlege $R$ in eine minimale Zahl von Teilmengen, sodass für jede dieser Teilmengen $T$ gilt.
\begin{itemize}
	\item $T$ ist kompakt
	\item $T$ ist spärlich
	\item $|T| \leq K$
\end{itemize}
Es lässt sich zeigen, dass es für jede Probleminstanz eine eindeutige Lösung gibt.
\paragraph{Algorithmus}
Für die Implementierung des Algorithmus wird die Einschränkung getroffen, dass $agg = \max$ ist.
Dadurch gilt für eine Teilmenge $S \subseteq R$, dass $S$ spärlich ist, genau dann, wenn gilt:
\begin{equation}
	\forall e \in S \colon \{e\} \text{ ist spärlich}
\end{equation}
Folglich kann die Spärlichkeit einfach überprüft werden, indem am Anfang alle Instanzen gefiltert werden, die nicht spärlich sind.
Für die Funktion $ng$ wird $p = 2$ festgelegt. \\
Algorithmus~\ref{alg:CSSN} beschreibt ein mögliches Vorgehen zum Lösen des Problems.
In Zeile 1 werden alle Instanzen $e$ gefiltert, für die $ng(e)$ größer als $c$ ist.
In der Menge $C$ werden alle gefundenen Cluster aufgenommen, also auch die in Zeile 1 gefilterten Elemente.
Die Menge $L$ enthält alle Instanzen, welche noch keinem Cluster zugewiesen wurden.
Diese Instanzen sind alle spärlich.
Danach wird ein zufälliges Element aus $L$ gewählt, welches ein neues Cluster $S$ bildet.
Dieses Cluster ist bereits kompakt und spärlich.
Anschließend wird in Zeile 8 bis 14 versucht, dieses Cluster zu vergrößern, um ein spärliches kompaktes Cluster zu erhalten, welches gerade so kleiner als $K$ ist.
Um das nächstgrößere Cluster zu finden, für das dies gilt, wird jede Instanz $r$ aus $R$ aufgenommen, für die es eine Instanz $s$ des Clusters $S$ gibt, sodass $r$ näher zu $s$ ist, als $s$ zu der von $s$ am weitesten entfernten Instanz im Cluster.
Das neue Cluster wurde gefunden, wenn sich bei erneuter Anwendung dieser Bildungsvorschrift das Cluster nicht mehr verändert.
Sollte dabei keine Instanz, welche in Zeile 1 gefiltert wurde, hinzugefügt werden oder das Cluster kleiner als $K$ sein, so wurde ein valides größeres Cluster gefunden.
Ansonsten wird das letzte gefundene valide Cluster in die Menge $C$ der Cluster aufgenommen und die Instanzen werden aus der Menge der übrigen Instanzen $L$ entfernt.
Dabei wird, unabhängig von welcher Instanz eines kompakten spärlichen Clusters man beginnt, immer dasselbe spärliche kompakte Cluster gefunden.
\newpage
\section{Evaluation}
\begin{figure}[H]
	\centering
	\begin{tabular}{|l|c|c|}
		\cline{2-3}
		\multicolumn{1}{c|}{}           & Duplikat             & kein Duplikat        \\
		\cline{1-3}
		klassifiziert als Duplikat      & richtig positiv (TP) & falsch positiv (FP)  \\
		\cline{1-3}
		klassifiziert als kein Duplikat & falsch negativ (FN)  & richtig negativ (TN) \\
		\cline{1-3}
	\end{tabular}
	\caption{mögliche Ausgänge einer Duplikateneliminierung}
\end{figure}
Um die Qualität der Duplikateneliminierung beurteilen zu können, ist es notwendig, diese an einem Datensatz zu testen, für den der wahre Duplikatenstatus der Paare bekannt ist.
Solche Daten können gewonnen werden, indem man zum Beispiel eine Teilmenge der zu deduplizierenden Paare auswählt und diese manuell dedupliziert.
Dabei ist zu beachten, dass eine zufällige Teilmenge nicht repräsentativ für den Datensatz ist, da die Zahl der Paare, welche Duplikate sind, linear und die, die keine sind, quadratisch wächst.
Außerdem ist die manuelle Beurteilung durch einen menschlichen Gutachter nicht perfekt, da die Informationen meistens nicht ausreicht, um eine informierte Entscheidung für schwer zu klassifizierende Paare zu treffen.
Hat man einen solchen Datensatz erstellt, ergeben sich folgende vier Klassen, die zur Beurteilung der Duplikateneliminierung genutzt werden: \\
\begin{tabularx}{\linewidth}{l X}
	                               &                                                                           \\
	\textbf{richtig Positive (TP)} & Paare, die als Duplikate klassifiziert wurden und wirklich Duplikate sind \\
	                               &                                                                           \\
	\textbf{falsch Positive (FP)}  & Paare, die als Duplikate klassifiziert wurden, aber keine sind            \\
	                               &                                                                           \\
	\textbf{richtig Negative (TN)} & Paare die nicht als Duplikate klassifiziert wurden und auch keine sind    \\
	                               &                                                                           \\
	\textbf{falsch Negative (FN)}  & Paare, die nicht als Duplikate klassifiziert wurden, aber welche sind     \\
	                               &                                                                           \\
\end{tabularx}
Folgende Maße eignen sich zur Beurteilung des Ergebnisses der Duplikateneliminierung:
\paragraph{Precision}
\begin{equation}
	prec = \frac{TP}{TP + FP}
\end{equation}
\paragraph{Recall}
\begin{equation}
	rec = \frac{TP}{TP + FN}
\end{equation}
\paragraph{F-measure}
\begin{equation}
	fmeas = 2 \cdot \frac{prec \cdot rec}{prec + rec}
\end{equation}
