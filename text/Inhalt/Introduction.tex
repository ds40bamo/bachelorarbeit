\chapter{Einleitung}
\section{Motivation}
Heutzutage werden Daten an vielen Orten von verschiedenen Institutionen erhoben und gespeichert.
Dabei können abhängig von den Schemabeschränkungen (\zB Datenstrukturen, beschränkter Wertebereich, Zulässigkeit fehlender Werte, Gültigkeitsüberprüfungen) und Eingabemethoden (\zB Tastatur, optische Zeichenerkennung) zufällig Fehler bei der Erhebung der Daten erzeugt werden~\cite{rahm2000data}.
Das Fehlen einer Typ- und Gültigkeitsüberprüfung kann dazu führen, dass die spätere Nutzung oder Analyse der Daten erschwert wird.
Ein mögliches Beispiel hierfür ist die Überprüfung einer E-Mail-Adresse.
Durch eine Typüberprüfung können unbeabsichtigte Fehler wie die Eingabe von ``name@domain'' mit ``nameqdomain'' vermieden werden.
Eine Gültigkeitsüberprüfung der Mailadresse durch das Senden einer Bestätigungsmail sichert zusätzlich die Gewissheit, dass die zu der Person passende Adresse eingegeben wurde.
Ist der Einsatz eine Gültigkeitsüberprüfung nicht möglich, sind Rechtschreibfehler und Informationsverlust durch das Zulassen von \zB Abkürzungen oder alternativen Schreibweisen unvermeidbar.
Beispielsweise ist der Wertebereich für zulässige Personennamen nahezu unbeschränkt.
So gibt es Namen mit verschiedenen Schreibweisen (\zB Shawn, Sean) und Abkürzungen (\zB Maximilian, Max).
In besonderen Fällen ist es zusätzlich zulässig, dass kein Wert angegeben werden muss.
Zum Beispiel besitzt jede Person einen Vornamen, aber nur wenige Personen besitzen einen oder mehrere Mittelnamen.
Sollten fehlende Werte zulässig sein, so ist die Gefahr des Informationsverlustes größer, da es möglich wird, vorhandene Werte vorzuenthalten.

Zudem gibt es Fehler, die bei der Eingabe der Daten nur bedingt vermieden werden können.
Beispielsweise könnte zur Eingabe ein Webformular verwendet werden, welches Vorname, Nachname, Namenszusatz, Geburtsdatum und Adresse erwartet.
Dabei können Daten in falsche Felder eingetragen werden.
Wird beispielsweise das Geburtsdatum in das Feld Vorname eingetragen und umgekehrt, so kann dies einfach erkannt werden, da sich die Wertebereiche dieser Felder stark unterscheiden.
Die Person kann dann darauf hingewiesen werden, die Fehler selbst zu korrigieren.
Wird wiederum der Nachname in das Feld Vorname eingetragen und umgekehrt, so kann dies nur schwer erkannt werden, da sich die Wertebereiche überschneiden.
In solchen Fällen können allenfalls statistische Methoden verwendet werden, um die Person auf eine mögliche Falscheingabe hinzuweisen, jedoch nicht die Abgabe des Formulars verweigert werden.
Zudem sind bei schwachen Schemabeschränkungen mehrere Repräsentationsformen für die gleiche Information möglich.
So gibt es beispielsweise für die Information ``3'' im Feld Namenszusatz mehrere richtige Möglichkeiten (\zB III, 3, three) und mehrere falsche Möglichkeiten (\zB 111, lll), diese einzugeben.
Da manche Daten über Zeit an Gültigkeit verlieren, reicht es nicht, sich einmalig von der Gültigkeit dieser Daten zu überzeugen.
In solchen Fällen ist es notwendig in wiederholten Abständen eine Gültigkeitsüberprüfung durchzuführen (\zB Namensänderung bei Heirat, Adressänderung durch Umzug).

In Szenarios, in denen jeder Entität maximal ein Datensatz einer Datenbank zugeordnet wird, können mehrere Datensätze zu einer Entität existieren.
Das Problem des Findens von Datensätzen, welche zu einer Entität gehören~\cite{dunn1946record,fellegi1969theory}, ist von besonderer Bedeutung, weil es nicht nur innerhalb einer Datenbank, sondern auch beim Vereinigen, Kombinieren und Aggregieren von Datensätzen aus mehreren Datenbanken auftritt.
Dadurch ergeben sich zusätzliche Probleme.
Daten können in verschiedene Datenbanken unterschiedlich repräsentiert werden (\zB Geschlecht: 0/1 vs.\ m/w) oder sich widersprechen (\zB Anmeldedatum der einen Datenbank vor Geburtsdatum der anderen Datenbank).
Felder mit unterschiedlichem Namen können sich auf dieselbe Information beziehen bzw. Felder mit gleichen Namen können sich auf unterschiedliche Information beziehen.
Daten, die in einer Datenbank in einem Feld vereinigt sind, können in einer anderen Datenbank über mehrere Felder aufgeteilt sein.

Bei der Vereinigung der Daten stellt sich die Frage, welche Attribute gültig sind und somit im neuen Datensatz aufgenommen werden sollten~\cite{bleiholder2009data}.
Werden solche Duplikate nicht zusammengeführt, wird die statistische Analyse der Daten verzerrt.
Außerdem können zusätzliche Kosten entstehen, wenn mit jedem Datensatz Dienstleistungen verbunden sind.

Die Duplikateneliminierung ist keine einfache Aufgabe, da häufig keine eindeutige Kennung existiert und die Datenbanken verschiedene Datenschemen und Formate verwenden.
Duplikateneliminierung muss demzufolge anhand der Attributwerte der Datensätze durchgeführt werden.
Wenn die Daten verunreinigt sind, kann dies dazu führen, dass Datensätze zusammengeführt werden, die nicht zusammen gehören.
Starke Schemabeschränkungen sowie Typ- und Gültigkeitsüberprüfung bei der Datenerhebung sind deshalb von besonderer Bedeutung bei der Qualität der Duplikateneliminierung.

In dieser Arbeit liegt der Fokus auf der Zusammenführung von Personendaten.

Die Zusammenführung von Personendaten bietet besonders im Gesundheitssektor großes Potenzial, da die Daten über viele spezialisierte Institute verteilt sind.
Durch die Anwendung von Duplikateneliminierung würden statistische Analysen in der Medizin und Wirtschaft ermöglicht werden, um beispielsweise komplexe Zusammenhänge zwischen Krankheiten oder Einkaufsverhalten erkennen zu können.
Allerdings muss aufgrund des Datenschutzes auf Transformation der Attributwerte gearbeitet werden.
Sind die Eliminierungsverfahren schlecht, sind keine Analysen auf den aggregierten Daten möglich.

Aufgrund des Datenschutzes sind Personendaten nur beschränkt verfügbar.
Deshalb ist es schwer, Datenbanken zu finden, welche Daten über dieselben Personen enthalten und für welche sich somit eine Zusammenführung lohnt.
Zusätzlich ist es für eine qualitative Aussage notwendig, dass diese Datensätze über domänenspezifisch erwartbare Merkmale wie Rechtschreibfehler, fehlende Werte und Duplikate verfügen,
Folglich sind qualitativ hochwertige duplikateneliminierte Personendatensätze fast nicht verfügbar.

Um sich von der Tauglichkeit von Eliminierungsverfahren überzeugen zu können, müssen diese ausreichend getestet und im Hinblick auf verschiedene Aspekte wie Skalierbarkeit, Effizienz und Linkage-Qualität evaluiert werden.
Diese Tests müssen auf mehreren verschiedenen Datensätzen durchgeführt werden, weil ein Verfahren auf einem Datensatz gut Ergebnisse und auf einem anderen schlechte liefern kann.
Da es fast keine duplikateneliminierte Personendatensätze gibt, ist es nicht möglich, diese Verfahren ausreichend zu untersuchen und mit anderen Verfahren zu vergleichen.

Eine mögliche Lösung für die mangelnde Verfügbarkeit der Datensätze ist es, die Daten anhand von Statistiken zu generieren und bewusst Fehler einzubauen ~\cite{christen2009accurate}.
Dabei ist es schwierig, die Daten so zu generieren, dass die Verteilung und Verwendung von Attributwerten sowie die Verteilung von Fehlern realistisch ist.

Eine andere Möglichkeit besteht in der Beschaffung und der Vereinigung von geeigneten echten Datensätzen.
Hierbei ist die Verteilung von Fehlern und Inkonsistenzen erwartungsgemäß realistisch, da diese durch Probleme bei der Datenerhebung entstanden sind.
Allerdings ist der Prozess der Datenerhebung unbekannt, durch den sich Verunreinigungen ergeben, unbekannt, weshalb die Vereinigung der Daten nicht optimal gelöst werden kann.
Zudem ist abhängig vom Datensatz Domainwissen notwendigen, um informiert entscheiden zu können, ob sich Datensätze auf dieselbe Entität beziehen oder nicht.

Diese Arbeit beschäftigt sich mit dem Auffinden solcher Personendaten und dem Zusammenführen ausgewählter Daten.
Bei der Zusammenführung werden bereits entwickelte Verfahren benutzt.
Dies ist zusätzlich problematisch, da aufgrund der Beschränktheit der Verfahren und der ungenügenden qualitativen Einschätzung im Bereich der Personendaten nicht garantiert werden kann, dass dadurch ein qualitativ hochwertiger Datensatz erstellt wird.
Zusätzlich gibt es für häufig auftretende Probleme bei der Datenqualität von Personendaten keine oder nur ungenügend zufriedenstellende Lösungen.
Beispielsweise gibt es kein Verfahre, welches genutzt werden kann, um äquivalente Namen zu erkennen, welche keine Ähnlichkeiten miteinander besitzen (\zB Margaret/Peggy\footnote{\url{https://dmnes.wordpress.com/2017/01/25/how-do-you-get-peggy-from-margaret} \lastVisited}, Robert/Bob \footnote{\url{https://www.quora.com/Why-is-Bob-a-nickname-for-Robert} \lastVisited}).

Sind umfangreiche Datensätze verfügbar, könnten neue Verfahren durch Analysen und Experimente auf diesen Daten entwickelt werden.
Zusätzlich kann mittels der Datensätze untersucht werden, welche Fehler häufig auftreten.

Hauptsächlich sollen die duplikateneliminierten Datensätze verwendet werden, um Verfahren zu evaluieren, wo bei der Zusammenführung zusätzlich der Datenschutz beachtet werden muss~\cite{vatsalan2013taxonomy}.
In solchen Fällen kann meist nur auf Transformationen der Attributwerte bei der Eliminierung gearbeitet werden, welche keine Rückschlüsse auf identifizierende Informationen zulassen.

\newpage
\section{Ziele der Arbeit}
Erste Aufgabe ist es, online nach Datenbanken zu suchen, welche Personendaten frei zur Verfügung stellen und die für die Evaluierung gewünschten Eigenschaften wie Rechtschreibfehler, fehlende Werte, Duplikate, Schema- und Wertinkonsistenzen sowie einen großen Umfang an Einträgen besitzen.
Zudem sollten die Datenbanken für die Duplikateneliminierung geeignete Attribute zur Verfügung stellen.
Außerdem müssen die verwendeten Datensätze leicht zugänglich sein, damit die Erstellung des deduplizierten Datensatzes ohne großen Aufwand reproduziert werden kann.
Anschließend sollen Datenbanken ausgewählt werden, an denen die Duplikateneliminierung durchgeführt werden soll.
Nach der Auswahl der Datenbanken erfolgt die Erstellung einer Software, welche die Datensätze beschafft und extrahiert.
Die Datensätze sollen zudem in ein einfaches und flexibles Format überführt werden, welches die für die Vereinigung relevanten Attribute enthält.
Als zweite Aufgabe sollte eine existierende geeignete Softwarelösung für die Vereinigung gefunden oder gegebenenfalls entworfen werden, um so den Gold-Standard zu erhalten.
Hierbei ist besonders die Qualität der Vereinigung von Bedeutung.
Um synthetische Verfahren geeignet motivieren zu können, sollen Statistiken über die erstellten Datensätze generiert werden.
Dazu gehört die Ermittlung der Anzahl der Datensätze und Duplikate sowie die Überlappungen, Art und Anzahl der Attribute.
Zudem soll die Anzahl und Verteilung der Werte für die synthetische Datengeneration und die Anzahl fehlerhafter und fehlender Werte bestimmt werden.
Gegebenenfalls erfolgt eine Gegenüberstellung mit bestehenden Datensätzen.

\section{Aufbau der Arbeit}
\paragraph{Kapitel~\ref{chap:basics}} beschreibt die Grundlagen der Duplikateneliminierung, welche für das Verständnis der Arbeit notwendig sind.
\paragraph{Kapitel~\ref{chap:related}} stellt verwandte Arbeiten vor, in denen Duplikateneliminierungen an Personendaten durchgeführt oder deskriptive Statistiken über relevante Datensätze gesammelt wurden.
\paragraph{Kapitel~\ref{chap:main}} präsentiert Datensätze, welche für die Duplikateneliminierung geeignet sind.
Zudem wird erläutert, wie die Daten vorverarbeitet werden und der Prozess der Duplikateneliminierung angewendet wird.
\paragraph{Kapitel~\ref{chap:evaluation}} zeigt die Ergebnisse von Tests, welche an einer Teilmenge der Daten durchgeführt wurden. Es werden die vorgestellten Verfahren hinsichtlich ihrer Tauglichkeit untersucht.
\paragraph{Kapitel~\ref{chap:ausblick}} fasst diese Arbeit kurz zusammen und schlägt Möglichkeiten vor, wie die Ergebnisse dieser Arbeit für weitere Projekte genutzt werden können.
