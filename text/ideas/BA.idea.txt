record linkage (Def): task of identifying, matching and merging records corresponding to the same entity from one or more data sources
contrained matching problem: 1-1 Matching oder kein Matching
Datenbanken sehr wichtig, Qualität der gespeicherten Information für System bestimmt Kosten von System
Entitäten der realen Welt haben mehrere Repräsentationen in Datenbasen, Probleme beim Linken:
    - kein allgemeiner Key (sonst desterministic record linkage)
    - Fehler (typografisch, kopieren, unvollständige Informationen, keine Standardformate, unterschiedliche Semantik, fehlende Integrität) (Daten werden nicht auf Qualität überprüft)

Daten Heterogenität:
    - strukturell -> unterschiedliche Struktur der Tupel in verschiedenen Datenbasen (z.B. addr oder street + city Felder)
    - lexikalisch -> selbe Struktur, aber verschiedene Repräsentation

Vorverarbeitung der Daten:
    - Daten Darstellung vereinheitlichen
    - Schritte (ETL):
        - Parsing
            - macht es einfacher zu standardisieren und matchen
        - Transformation
            - an Datentyp anpassen
            - Umbenennung
            - Decodierung bzw. Extraktion
            - Abhängigkeiten überprüfen -> Information aus mehreren Feldern berücksichtigen
            - Range checking
        - Standardisierunge

Bestimmen der Felder, die zu vergleichen sind:
    - Ähnliche Namen
    - generiere eine Signatur aus allen Werten -> Felder mit ähnliche Signatur beschreiben wahrscheinlich das Selbe
    
Field Matching:
    - hauptsächlich Stringvergleichstechniken

    - Charakter basierte Metriken:
        - Editierdistanz (Minimale Anzahl Editieroperationen)
            - Operationen: Insert, Delete, Replace
            - Levenshtein distance, wenn alle 3 Operationen 1 kosten
            - Zeit: O(Produkt Länge beider Strings)
        - gap penalty (affine gap distance)
            - gut, falls Teile des Strings abgeschnitten oder verkürzt
            - Operationen: open gap, extend gap
            - Kosten open gap normalerweise kleiner als extend gap
            - Zeit: O(a * Produkt Länge beider Strings) (a maximale Länge einer Gap)
        - smith waterman distance
            - mismatch am Anfang und Ende des Strings billiger
            - bessere lokale Ausrichtung
            - Zeit: O(Produkt Länge beider Strings)
        - Jaro-Winkler distance metric
            - Vergleich von Vor- und Nachnamen
            - Zeit: O(Produkt Länge beider Strings)
        - q-gram distance
            - Substrings der Länge q
            - q-1 padding character an Anfang und Ende
            - Zeit: O(max{|String1|, |String2|}) bei Verwendung von Hashtable

    - Token basierte Metriken:
        - atomic string:
            - string bestehend aus alphanumerischen Zeichen, begrenzt von puntuation
            - match, falls gleich oder einer Präfix vom Anderen
            - similarity: Anzahl matching atomic strings geteilt durch durchschnittliche Anzahl atomic strings
        - WHIRL:
            - kombiniert Kosinus Ähnlichkeit mit tf.idf
            - Titel kommen oft vor und werden deshalb niedrig gewichtet
            - Problem: Rechtschreibfehler
                - Lösung: SoftTF.IDF (auch ähnliche Worte werden betrachtet)
        - q-grams + tf.idf:
            - WHIRL + behandelt Rechtschreibfehler
            - q-gramme als Token anstatt Wörter

    - phonetische Metriken
        - Soundex:
            - hauptsächlich für Nachnamen (kaukasisch)
            - ersetzt Buchstaben mit Nummern
        - NYSIIS:
            - erhält Information über Position der Vokale im gegesatz zu Soundex
            - ersetzt Konsonanten mit anderen ähnlichen Buchstaben
        - ONCA:
            - Schritte:
                - britische Version der NYSIIS Methode zur Kompression
                - dann Soundex
        - Metaphone:
            - bessere Alternative zu Soundex
        - Double Metaphone:
            - mehrere Encodings

    - Numerische Metriken
        - range queries
        - Zahl als String auffassen und obere Metriken anwenden

    - Jaro-Winkler am besten für Namen
    - Monge-Elkan beste durchschnittliche Performance
    - SoftTF.IDF beste Metrik

Matching:
    - Probabilistisch
        - Bayesian inference (Bayes Decision Rule for Minimum Error)
            - Standardmethode
            - Match, falls Wahrscheinlichkeit für Match größer als Wahrscheinlichkeit für kein Match, sonst kein Match
            - comparison vector x mit Größe n
            - likelihood ratio
            - Naive Bayes für p(x|M) und p(x|U)
                - Annahme: p(xi|M) und p(xj|M) unabhängig, falls i != j (analog für U)
                - Klassifizierung
                    - Training Data (vorgelabelde record pairs)
                    - expectation maximization 
                        xi = 1, falls Feld i matcht, xi = 0 sonst
        - Fehlende Werte (Du Bois)
            - comparison vector x mit Größe 2n
            - yi = 1 -> Feld i ist vohanden, yi = 0 -> Feld ist nicht

        - Bayes Decision Rule for Minimum Cost

        - Decision with a Reject Region:
            - 3. Klasse (Reject class) welche Paare aufnimmt, für die likelihood ratio nahe am threshold ist

    - supervised and semi-supervised learning
        - CART Algorithmus -> geringste Error Rate
        - SVM -> learn how merge matching results
        - Graph erzeugen wo Matches zusammengelinkt werden und dann Transitivität (same connected Component are identical)
            - Graphpartitionierung um inconsistencies zu minimieren NP-vollständig
        - große Menge an Trainingsdaten notwendig
    
    - active-learning-based techniques
        - ALIAS (nutzt reject region)
            - Paare die nicht klar zugewiesen werden können brauchen Mensch
            - bekommt geringe Menge an Trainingsdaten
    
    - distance based techniques
        - Ansätze:
            - betrachte Eintrag als langes Feld und wende Distanzmetrik an
                - WHIRL (+ q-grams) empfohlen
            - Distanz zwischen jedem Feld berechnen (jedes Feld eigene Metrik) und anschließend weighted distance
            - ranked list merging:
                - ranke records nach ihrer Ähnlichkeit zum Rekord anhand eines Feldes
                - auf alle Felder anweden -> n ranked lists
                - erzeuge eine ranked Liste aus allen n listen (minimum aggregate rank distance)
                - get top-k matching records
                - Hungarian Algorithm
                - Successive Shortest Paths Algorithm
            - einziger globaler threshold schlecht
                - besser angepasster threshold für jedes Objekt nach Chaudhuri

    - regelbasierte Ansätze
        - Distanz basiert, wobei Distanz 0 oder 1 ist
        - z.B. wenn Namen ähnlich und Adresse gleich, dann match
        - AJAX declarative language for data cleaning programs
        
    - unsupervised learning
        - clustering algorithmen
        - co-training -> nur wenig gelabelde Daten verwenden
        - TAILOR
        - Latent Dirichlet Allocation

Effizienz
    - nested loop zu teuer
    - einzelne Vergleiche teuer
    - Anzahl Vergleiche verringern
        - standard
            - Hashfunktion auf jeden record
            - identische records haben selben bucket
        - blocking
            - ein Feld wird ausgewählt und gehasht
            - kann zu Erhöhung falschen mismatches führen
            - Fehler mildern indem blocking mehrmals (auf verschiedenen Feldern ausgeführt wird)
        - sorted neighborhood:
            - Schritte:
                - create Key
                - sort data
                - merge -> window
            - Keyauswahl entscheidend
        - clustering and canopies
            - bestimmen der connected Components
            - wenn a nicht mit b matcht, dann auch nicht mit anderen aus dem Cluster von b
            - canopies:
                - billige Metrik um grob zu Clustern
                - teure Metrik um grobe Cluster fein zu Clustern
                - z.B. Editierdistanz, tf.idf, string length + number of common q-grams
        - set joins
            - Mengen Operationen beschleunigen
    - Record Vergleiche
        - feture subset selection
        - purning off the decision trees
            - avoid overfitting
            - higher accuracy

Tools
    - Febrl
    - TAILOR
    - WHIRL
    - BigMatch


