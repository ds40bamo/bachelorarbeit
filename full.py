# basic parameters
date1 = "20130531"
date2 = "20200229"
num_workers = 30  # num_workers has to be 1 if the cluster is not running
deduplicate = False

from app.classify import DedupeClassifier, LinkClassifier
from app.cluster import dedupe_cluster, link_cluster
from app.config import (DEFAULT_DEDUPE_CONFIG, DEFAULT_FILTER_CONFIG,
                        DEFAULT_LINK_CONFIG)
from app.data import download
from app.evaluation import Evaluation
from app.export import export
from app.index import Keys
from app.link import dedupe, link, no_dedupe
from app.merge import (combine_merge, dedupe_merge, merge_link_results,
                       merge_match_files)
from app.preprocessing import (clean_matches, combine_classify, combine_train,
                               parse)
from app.review import TrainDataReviewer
from app.utils import parse_date


def full_dedupe(date1, date2, dedupe_classifier, dedupe_config, num_workers):
    dedupe(date1, dedupe_classifier, dedupe_config, num_workers=num_workers)
    dedupe(date2, dedupe_classifier, dedupe_config, num_workers=num_workers)

    dedupe_cluster(date1, 3, 3)
    dedupe_cluster(date2, 3, 3)

    dedupe_merge(date1)
    dedupe_merge(date2)


download(date1)
download(date2)

parse(date1)
parse(date2)

combine_train(date1, date2)

keys = Keys.load("20130531", "20200229")
key = keys.get(0.985)

DEFAULT_DEDUPE_CONFIG.set_key(key)
DEFAULT_LINK_CONFIG.set_key(key)
DEFAULT_FILTER_CONFIG.set_key(key)

gen_train = Evaluation(
    "20130531",
    "20200229",
    ["hol", "cal", "bak", "sum", "hen", "mon", "fra", "ind"],
    DEFAULT_LINK_CONFIG,
)

if DedupeClassifier.exists(date1, date2):
    dedupe_classifier = DedupeClassifier.load("20130531", "20200229")
else:
    dedupe_classifier = gen_train.to_classifier(date1, date2, DEFAULT_DEDUPE_CONFIG)
    dedupe_classifier.save()

if LinkClassifier.exists(date1, date2):
    link_classifier = LinkClassifier.load(date1, date2)
else:
    link_classifier = gen_train.to_classifier(date1, date2, DEFAULT_LINK_CONFIG)
    link_classifier.save()

match_clf = TrainDataReviewer.load("20130531", "20200229", "matches").train_classifier(
    DEFAULT_FILTER_CONFIG
)

del gen_train

if deduplicate:
    full_dedupe(date1, date2, dedupe_classifier, DEFAULT_DEDUPE_CONFIG, num_workers)
else:
    no_dedupe(date1)
    no_dedupe(date2)

combine_classify(date1, date2)
clean_matches(date1, date2, match_clf)
link(date1, date2, link_classifier, DEFAULT_LINK_CONFIG, num_workers=num_workers)

link_cluster(date1, date2)
merge_link_results(date1, date2)
merge_match_files(date1, date2)
combine_merge(date1, date2)

export(date1, date2, "~/output", with_merged=True)
