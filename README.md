# FloridaLink
This projects generates evaluation datasets for record linkage tasks from the voterfiles of Florida.  
**Requirements**:
- at least 16GB Memory
- python 3.8
- pipenv

## Setup
Setup virtual environment:
```
pipenv sync
```

All commands have to be executed using pipenv:
```
pipenv run <command>
```

Install basic models:
```
./models_setup.sh
```

A full linkage example can be found inside the file [full.py](https://git.informatik.uni-leipzig.de/ds40bamo/bachelorarbeit/-/blob/master/full.py)

## Data
The files are stored in `~/.cache`. The location can be changed by setting the `FLORIDALINK_DIR` environment variable.  
The used Data can be found under: [**http://flvoters.com/download/**](http://flvoters.com/download/)  
Use this site to find available dates.

### Download the Data
Download the voterfiles from 31.05.2013:
```python
from app.data import download
download("20130531") # many valid formats are possible like "05.31.2013" or "05/31/2013"
```

## Preprocessing
This applies some transformations, corrections and rules to standardize the data.  
To use it, the geocoding service needs to run.  

**Requirements**:
- docker
- docker-compose

After installing docker and docker-compose and running docker, navigate to `geocoder` from the project root and run
```
docker-compose up
```
It takes some time for the download and setup of the address data.  
When the geocoder is running the files can be parsed:

```python
from app.preprocessing import parse
parse("20130531")
```

## Training

To extract train data from the files run:
```python
from app.preprocessing import combine_train
combine_train(date1, date2)
```

The Data is extracted by checking for equal voter-ids between the files.

### Configs
Configs are needed to load data, train classifiers and compare the data.
The specify which fields are loaded and what fields are compared.


#### Predicate config
It specifies the predicates from which candidates are generated when learning a blockingkey.
Available predicates can be found under `app.index.methods`

```python
from app.config import PredicateConfig
pconf = PredicateConfig()
pconf.add("first_name", "first_name", "exact")    # compare first_name of both files with exact
pconf.add("middle_name", "middle_name", "exact")
pconf.add("last_name", "last_name", "soundex")
pconf.add("birth_date", "birth_date", "exact")
pconf.add("birth_date", "birth_date", "month_year")
pconf.add("gender", "gender", "exact")
pconf.add_major_field("first_name")  # to filter uninteresting pairs, take only pairs, which have different first_name
pconf.add_major_field("last_name")   # or last_name
pconf.add_major_field("birth_date")  # or birth_date
pconf.add_list_field_left("middle_name", "|")  # the field middle_name is transformed from string to list on separator "|" (e.g. "hello|world" -> ["hello", "world"])
pconf.add_list_field_right("middle_name", "|")
pconf.add_list_field_left("name_suffix", " ")
pconf.add_list_field_right("name_suffix", " ")
pconf.add_list_field_left("last_name", "|")
pconf.add_list_field_right("last_name", "|")
```

#### Dedupe config
Available Metrics for comparison are found under `app.compare.metrics`

```python
from app.config import DedupeConfig

def _fullname(data):
    return (data.first_name.map(lambda x: [x]) + data.middle_name + data.last_name).map(
        lambda l: [e for e in l if e != ""]
    )

fullname = {
    "dependencies": {"first_name", "middle_name", "last_name"}, # necessary to make sure, that the data is loaded
    "function": _fullname, # function that return the data
}

dconf = DedupeConfig()
dconf.add("first_name", "first_name", "jaro_winkler_empty_no_info")
dconf.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
dconf.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dconf.add(("latitude", "longitude"), ("latitude", "longitude"), "haversine")
dconf.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dconf.add_complex_field("fullname", fullname) # add a attribute, that is build by combining other attributes
dconf.add_list_field("middle_name", "|")
dconf.add_list_field("name_suffix", " ")
dconf.add_list_field("last_name", "|")
```

#### Link config
Available Metrics for comparison are found under `app.compare.metrics`
```python
from app.config import LinkConfig
from app.utils import fullname

lconf = LinkConfig()
lconf.add("first_name", "first_name", "jaro_winkler_empty_no_info")
lconf.add("middle_name", "middle_name", "symetric_monge_elkan[jaro_winkler]")
lconf.add("last_name", "last_name", "symetric_monge_elkan_empty_no_info[jaro_winkler]")
lconf.add(("latitude", "longitude"), ("latitude", "longitude"), "haversine")
lconf.add("fullname", "fullname", "symetric_monge_elkan_empty_no_info[jaro_winkler]")
lconf.add_complex_field_left("fullname", fullname)
lconf.add_complex_field_right("fullname", fullname)
lconf.add_list_field_left("middle_name", "|")
lconf.add_list_field_right("middle_name", "|")
lconf.add_list_field_left("name_suffix", " ")
lconf.add_list_field_right("name_suffix", " ")
lconf.add_list_field_left("last_name", "|")
lconf.add_list_field_right("last_name", "|")
lconf.add_major_field("first_name")
lconf.add_major_field("last_name")
```

### Block learning

**Train and save**:
```python
from app.index import Keys
keys = Keys.learn("20130531", "20200229")
keys.save()
```

**Load after program restart**:
```python
keys = Keys.load(date1, date2)
```

**Get a blockingkey by recall**:
```python
key = keys.get(0.985) # it takes the key with a recall greater than the given recall
```

### Filter Classifier
Since the trainings data is sampled from Pairs that have the same voter-id, some pairs that would be determined as matches could be non-matches and vice verca.
Therefore two classifiers are trained, that filter some these pairs.
This requires manual labeling of the pairs.

**Create**:
```python
lconf.set_key(key)

from app.review import TrainDataReviewer
date1 = "20130531"
date2 = "20200229"
match_review = TrainDataReviewer.create(date1, date2, "matches") # non_match_review = TrainDataReviewer.create(date1, date2, "non_matches")
match_review.lable_loop() # non_match_review.lable_loop()
match_review.save() # non_match_review.save()
```

**Train**:
```python
match_review = TrainDataReviewer.load(date1, date2, "matches") # non_match_review = TrainDataReviewer.load(date1, date2, "non_matches")
match_clf = match_review.train_classifier(lconf) # non_match_clf = non_match_review.train_classifier(lconf)
```

### Linkage and Dedupe Classifier
```python
from app.classify import LinkClassifier

lconf.set_key(key)
dconf.set_key(key)

link_classifier = LinkClassifier.build(date1, date2, match_clf, non_match_clf, lconf)
link_classifier.save()

dedupe_classifier = DedupeClassifier.build(date1, date2, match_clf, non_match_clf, dconf)
dedupe_classifier.save()
```

## Deduplication
```python
from app.link import dedupe
from app.cluster import dedupe_cluster
from app.merge import dedupe_merge

dedupe_classifier = DedupeClassifier.load(date1, date2)
dedupe(date1, dedupe_classifier, dconf)
dedupe(date2, dedupe_classifier, dconf)

dedupe_cluster(date1, 3, 3) # CS-SN clustering to apply transitive closure
dedupe_cluster(date2, 3, 3)

dedupe_merge(date1) # merge the data to prepare linkage input
dedupe_merge(date2)
```

## Prepare Linkage

```python
from app.preprocessing import combine_classify, clean_matches
combine_classify(date1, date2)
clean_matches(date1, date2, match_clf)
```

## Link
```python
from app.link import link
from app.cluster import link_cluster
from app.merge import merge_link_results, merge_match_files, combine_merge

link_classifier = LinkClassifier.load(date1, date2)

link(date1, date2, link_classifier, lconf)
link_cluster(date1, date2, "hungarian") # clustering to apply transitive closure

merge_link_results(date1, date2) # combine the results of linkage
merge_match_files(date1, date2) # combine the linkage that can be determined by using the voter-id
combine_merge(date1, date2) # combine the outputs of the previous steps for final output file
```

## Evaluate
This is a optional step that can be used to evaluate the linkage results and generate better classifiers.

```python
from app.evaluation import Evaluation
gen_train = Evaluation(date1, date2, ["hol", "cal", "bak", "sum", "hen", "mon", "fra", "ind"], lconf) # specify counties that are used for the generation
gen_train.extract() # extract data from parsed files
gen_train.link(link_classifier) # first linkage to filter trivial non_matches
gen_train.get_reviewer().label_loop() # manual labeling
gen_train.get_reviewer().save() # save the progress
gen_train.get_reviewer().write_results() # when all pairs have been labeled, write the results for faster access
gen_train.to_classifier(date1, date2, lconf) # generate classifier from labeled data
gen_train.to_classifier(date1, date2, lconf) # generate classifier from labeled data
evaluation.evaluate(  # evaluate for different settings
    dconf,
    lconf,
    dedupe_classifier,
    link_classifier,
    10, # dedupe cluster c
    1, # dedupe cluster K
    "greedy" # link cluster approach
)
```

## Export
The input data, merged data and links can be exported at the end of the linkage.
The links are represented by the voter_id of the records.
Records might have multiple voter_ids separated by "|" if they have been deduplicated.

```python
from app.export import export
export(date1, date2, "~/output", with_merged=True) # export to folder ~/output
```

The review data can also be exported.

```python
gen_train.get_reviewer().export("~/output/")
```


## Parallel
Since the amount of data is huge, [ray](https://github.com/ray-project/ray) can be used to distribute the steps to a cluster.
Install docker on all machines, that should get a computation.
The project has to be on the master and the file that is used for the linkage has to be in the project root.
Use the `gen_cluster_conf.py` to generate a config:

```
pipenv run ./gen_cluster_conf.py ssh_username host1 host2 ... hostn
```

It generates a `cluster.yaml` file.  

Run the following to start the cluster:
```
pipenv run ray up cluster.yaml
```

Run the following to stop the cluster:
```
pipenv run ray down cluster.yaml
```

Run the following to stop the containers on the cluster once you are finished:
```
pipenv run ./stopcluster.py cluster.yaml
```

When there are problems, sometimes deleting the files in `/tmp` which contain "ray" or "cluster" solve the issues.

The cluster can be monitored by running:
```
ssh -L 8266:localhost:8266 -L 8265:localhost:8265 -L 8267:localhost:8267 static
```
(sometimes the dashboard gets started on different ports, default is 8265)  
The dashboard can be accessed by opening `localhost:8265` or `localhost:8266` or `localhost:8267` in the browser.

Change the `num_workers` in the linkage step:
```python
dedupe(date1, dedupe_classifier, dconf, num_workers=100)
link(date1, date2, link_classifier, lconf, num_workers=100)
```

and run the following on the master:
```
docker exec -it florida_link python /files/<your file>
```
