#!/bin/bash

POSTGERSQL_DIR=/var/lib/postgresql
POSTGRESQL_VERSION=$(ls $POSTGERSQL_DIR)
POSTGRES_MAIN=$POSTGERSQL_DIR/$POSTGRESQL_VERSION/main
OSMURL=http://download.geofabrik.de/north-america/us/florida-latest.osm.pbf
FILENAME=$(basename $OSMURL)
DATADIR=/data

if [ ! -L "$POSTGRES_MAIN" -o ! -d "$DATADIR/postgresdata" ]
then
    if [ -d "${POSTGRES_MAIN}.tmp" ]
    then
        stopServices
        rm "${POSTGRES_MAIN}" -r
        mv "${POSTGRES_MAIN}.tmp" "$POSTGRES_MAIN"
    fi
    mkdir -p "$DATADIR"
    rm -rf "$DATADIR/postgresdata"
    curl -Lo "$DATADIR/$FILENAME" $OSMURL
    sh /app/init.sh "$DATADIR/$FILENAME" postgresdata $(nproc)
    stopServices
    mv "$POSTGRES_MAIN" "${POSTGRES_MAIN}.tmp"
    ln -sf "$DATADIR/postgresdata" "${POSTGRES_MAIN}"
fi

exec /app/start.sh
