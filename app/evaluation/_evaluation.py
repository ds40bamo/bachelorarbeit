import json
import logging
from hashlib import md5, sha256
from shutil import copyfile

import numpy as np
import matplotlib.pyplot as plt
import jsonlines
import networkx as nx
import pandas as pd
from itertools import product
from app.compare import apply_metrics

from app.cluster import dedupe_cluster
from app.config import DedupeConfig, LinkConfig
from app.classify import DedupeClassifier, LinkClassifier
from app.classify._train import Block
from app.data import gen_path
from app.link import dedupe, link
from app.merge import dedupe_merge
from app.preprocessing import combine_classify
from app.preprocessing._parse._address._county import county_keys

from app.review import EvaluationReviewer
from sklearn.metrics import precision_score, recall_score
from app.cluster._cluster import link_approaches

available_counties = set(county_keys)


def _gen_md5_hash(_path):
    hasher = md5()
    with open(_path, "rb") as binary:
        hasher.update(binary.read())
        return hasher.digest().hex()


def _file_hash_is_valid(_path):
    hash_path = _path.parent / (_path.name + "_hash.json")

    try:
        with open(hash_path, "r") as file:
            validate_hash = json.load(file)["hash"]
            return _gen_md5_hash(_path) == validate_hash
    except FileNotFoundError:
        return False


def _gen_file_hash(_path):
    file_hash = _gen_md5_hash(_path)
    hash_path = _path.parent / (_path.name + "_hash.json")
    with open(hash_path, "w") as file:
        json.dump({"hash": file_hash}, file)


def _best_split(graph_with_edge, edgelist):
    if not edgelist:
        return None, None, -1
    curr_edge = edgelist[0]
    graph_without_edge = graph_with_edge.copy()
    graph_without_edge.remove_edge(*curr_edge)

    if nx.is_connected(graph_without_edge):
        graph1_without_edge, graph2_without_edge, score_without = _best_split(
            graph_without_edge, edgelist[1:]
        )
    else:
        graph1_without_edge, graph2_without_edge = nx.connected_components(
            graph_without_edge
        )
        graph1_without_edge = graph_without_edge.subgraph(graph1_without_edge)
        graph2_without_edge = graph_without_edge.subgraph(graph2_without_edge)
        score_without = nx.density(graph1_without_edge) + nx.density(
            graph2_without_edge
        )
    graph1_with_edge, graph2_with_edge, score_with = _best_split(
        graph_with_edge, edgelist[1:]
    )

    if score_without > score_with:
        return graph1_without_edge, graph2_without_edge, score_without
    else:
        return graph1_with_edge, graph2_with_edge, score_with


def _best_graph_density_split(graph):
    graph1, graph2, _ = _best_split(graph, list(graph.edges))
    return graph1, graph2


def _make_dense(links, density=0.5):
    graph = nx.from_pandas_edgelist(links, "left", "right")
    sparse = [graph.subgraph(component) for component in nx.connected_components(graph)]
    del graph
    dense = []
    while sparse:
        graph = sparse.pop()
        if nx.density(graph) >= density:
            dense.append(graph)
            continue
        graph1, graph2 = _best_graph_density_split(graph)
        if nx.density(graph1) < density and len(graph1) > 1:
            sparse.append(graph1)
        else:
            dense.append(graph1)
        if nx.density(graph2) < density and len(graph2) > 1:
            sparse.append(graph2)
        else:
            dense.append(graph2)
    return [list(cluster) for cluster in dense]


def _cluster_dedupe(date, counties):
    src_path = gen_path(date, filename="review_links.csv", counties=counties)
    dest_path = gen_path(date, filename="review_links.csv", counties=counties)
    links = pd.read_csv(src_path, dtype="uint32")
    clusters = _make_dense(links)
    with jsonlines.open(dest_path, "w") as file:
        file.write_all(clusters)


class Evaluation:
    def __init__(self, date1, date2, counties, link_config):
        counties = {county.lower() for county in counties}
        unknown_counties = counties.difference(available_counties)
        if unknown_counties:
            raise ValueError(f"{unknown_counties} not known")
        if not isinstance(link_config, LinkConfig):
            raise ValueError("link_config is not a instance of LinkConfig")
        self.link_config = link_config
        self.date1 = date1
        self.date2 = date2
        self.counties = counties
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

    def _combine(self):
        src_left = gen_path(self.date1, filename="parsed.csv", counties=self.counties)
        src_right = gen_path(self.date2, filename="parsed.csv", counties=self.counties)
        dst_left, dst_right = gen_path(
            self.date1,
            self.date2,
            suffix="_classify.csv",
            counties=self.counties,
            mkdir=True,
        )

        copyfile(src_left, dst_left)
        copyfile(src_right, dst_right)

    def extract(self):
        self.logger.info("extracting %s", self.date1)
        _extract(self.date1, self.counties)
        self.logger.info("extracting %s", self.date2)
        _extract(self.date2, self.counties)
        self.logger.info("combining %s %s", self.date1, self.date2)
        self._combine()

    def link(self, classifier, num_workers=1):
        link(
            self.date1,
            self.date2,
            classifier,
            self.link_config,
            counties=self.counties,
            num_workers=num_workers,
        )
        link_path = gen_path(
            self.date1, self.date2, filename="links.csv", counties=self.counties
        )
        links = pd.read_csv(link_path)
        links = links[links["left"] != links["right"]]
        links.to_csv(link_path, mode="w", header=True, index=False)

    def get_reviewer(self, recreate=False):
        if recreate:
            self.logger.info("creating reviewer")
            self.link_review = EvaluationReviewer.create(
                self.date1, self.date2, self.link_config, self.counties
            )
        try:
            link_review = self.link_review
        except AttributeError:
            try:
                self.link_review = EvaluationReviewer.load(self.date1, self.date2, self.counties)
                self.logger.info("reviewer was loaded")
            except FileNotFoundError:
                self.logger.info("creating reviewer")
                self.link_review = EvaluationReviewer.create(
                    self.date1, self.date2, self.link_config, self.counties
                )
            link_review = self.link_review
        return link_review

    def evaluate(
        self,
        dedupe_config,
        link_config,
        dedupe_classifier,
        link_classifier,
        dedupe_cluster_c,
        dedupe_cluster_k,
        cluster_approach,
        graphic=False,
        num_workers=1,
    ):
        hasher = sha256()

        hasher.update(dedupe_config.hash().encode("utf-8"))
        hasher.update(dedupe_classifier.hash().encode("utf-8"))
        dedupe_hash = hasher.hexdigest()

        out_path = gen_path(
            self.date1, filename="links.csv", counties=self.counties, _hash=dedupe_hash
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("deduping %s", self.date1)
            dedupe(
                self.date1,
                dedupe_classifier,
                dedupe_config,
                counties=self.counties,
                dst_hash=dedupe_hash,
                num_workers=num_workers,
                voter_ids=True
            )
            _gen_file_hash(out_path)

        out_path = gen_path(
            self.date2, filename="links.csv", counties=self.counties, _hash=dedupe_hash
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("deduping %s", self.date2)
            dedupe(
                self.date2,
                dedupe_classifier,
                dedupe_config,
                counties=self.counties,
                dst_hash=dedupe_hash,
                num_workers=num_workers,
                voter_ids=True
            )
            _gen_file_hash(out_path)

        hasher.update(f"_{dedupe_cluster_c}_{dedupe_cluster_k}_".encode("utf-8"))
        cluster_hash = hasher.hexdigest()

        out_path = gen_path(
            self.date1,
            filename="clusters.jsonl",
            counties=self.counties,
            _hash=cluster_hash,
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("deduping cluster %s", self.date1)
            dedupe_cluster(
                self.date1,
                dedupe_cluster_c,
                dedupe_cluster_k,
                counties=self.counties,
                src_hash=dedupe_hash,
                dst_hash=cluster_hash,
            )
            _gen_file_hash(out_path)

        out_path = gen_path(
            self.date2,
            filename="clusters.jsonl",
            counties=self.counties,
            _hash=cluster_hash,
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("deduping cluster %s", self.date2)
            dedupe_cluster(
                self.date2,
                dedupe_cluster_c,
                dedupe_cluster_k,
                counties=self.counties,
                src_hash=dedupe_hash,
                dst_hash=cluster_hash,
            )
            _gen_file_hash(out_path)

        merge_hash = hasher.hexdigest()

        out_path = gen_path(
            self.date1, filename="output.csv", counties=self.counties, _hash=merge_hash,
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("merging cluster %s", self.date1)
            dedupe_merge(
                self.date1,
                counties=self.counties,
                dedupe_hash=dedupe_hash,
                cluster_hash=cluster_hash,
                dst_hash=merge_hash,
            )
            _gen_file_hash(out_path)

        out_path = gen_path(
            self.date2, filename="output.csv", counties=self.counties, _hash=merge_hash
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("merging cluster %s", self.date2)
            dedupe_merge(
                self.date2,
                counties=self.counties,
                dedupe_hash=dedupe_hash,
                cluster_hash=cluster_hash,
                dst_hash=merge_hash,
            )
            _gen_file_hash(out_path)

        match1_path, match2_path = gen_path(
            self.date1,
            self.date2,
            suffix="_match.csv",
            counties=self.counties,
            _hash=merge_hash,
        )
        classify1_path, classify2_path = gen_path(
            self.date1,
            self.date2,
            suffix="_classify.csv",
            counties=self.counties,
            _hash=merge_hash,
        )
        if not (
            _file_hash_is_valid(match1_path)
            and _file_hash_is_valid(match2_path)
            and _file_hash_is_valid(classify1_path)
            and _file_hash_is_valid(classify2_path)
        ):
            self.logger.info("combining outputs %s %s", self.date1, self.date2)
            combine_classify(
                self.date1, self.date2, counties=self.counties, _hash=merge_hash, pre=False
            )
            _gen_file_hash(match1_path)
            _gen_file_hash(match2_path)
            _gen_file_hash(classify1_path)
            _gen_file_hash(classify2_path)

        hasher.update(link_config.hash().encode("utf-8"))
        hasher.update(link_classifier.hash().encode("utf-8"))
        link_hash = hasher.hexdigest()

        out_path = gen_path(
            self.date1,
            self.date2,
            filename="links.csv",
            counties=self.counties,
            _hash=link_hash,
        )
        if not _file_hash_is_valid(out_path):
            self.logger.info("linking %s %s", self.date1, self.date2)
            link(
                self.date1,
                self.date2,
                link_classifier,
                link_config,
                counties=self.counties,
                src_hash=merge_hash,
                dst_hash=link_hash,
                num_workers=num_workers,
            )
            _gen_file_hash(out_path)

        if graphic:
            self._graphic(link_hash, merge_hash)

        hasher.update(cluster_approach.encode("utf-8"))
        cluster_hash = hasher.hexdigest()
        save_path = gen_path(self.date1, self.date2, counties=self.counties, filename="evaluation.csv", _hash=cluster_hash)
        if not _file_hash_is_valid(save_path):
            self._gen_review_output(merge_hash, link_hash, cluster_hash, cluster_approach)

            _gen_file_hash(save_path)

        self._evaluate(cluster_hash)


    def _graphic(self, link_hash, merge_hash):
        links_path = gen_path(self.date1, self.date2, filename="links.csv", counties=self.counties, _hash=link_hash)
        gold_path = gen_path(self.date1, self.date2, counties=self.counties, filename="review_links.csv")
        gold_links = pd.read_csv(gold_path)
        classify_path_left, classify_path_right = gen_path(
            self.date1, self.date2, suffix="_classify.csv", counties=self.counties, _hash=merge_hash
        )
        voter_ids_left = pd.read_csv(classify_path_left, usecols=["voter_ids"], dtype="str").voter_ids
        voter_ids_right = pd.read_csv(classify_path_right , usecols=["voter_ids"], dtype="str").voter_ids
        output_links = pd.read_csv(links_path)

        gold_links = gold_links[gold_links["left"] != gold_links["right"]]
        output_links["left"] = voter_ids_left.loc[output_links["left"].values].values
        output_links["left"] = output_links["left"].str.split("|")
        output_links["right"] = voter_ids_right.loc[output_links["right"].values].values
        output_links["right"] = output_links["right"].str.split("|")
        output_links = output_links.explode("left")
        output_links = output_links.explode("right")
        output_links["left"] = output_links["left"].astype("int64")
        output_links["right"] = output_links["right"].astype("int64")

        gold_links["kind"] = "match"
        output_links = output_links.merge(gold_links, on=["left", "right"], how="outer")
        output_links.loc[output_links["kind"].isna(), "kind"] = "non_match"
        output_links = output_links.fillna(output_links.prob.min())
        matches = output_links[output_links["kind"] == "match"].prob
        non_matches = output_links[output_links["kind"] == "non_match"].prob

        fig, ax = plt.subplots()
        matches.plot.density(ind=np.linspace(0, 1, 101), ax=ax)
        non_matches.plot.density(ind=np.linspace(0, 1, 101), ax=ax)
        ax.legend(["Duplikate", "keine Duplikate"])

        save_path = gen_path(self.date1, self.date2, filename="distribution.png", counties=self.counties, _hash=link_hash)
        plt.savefig(save_path, bbox_inches="tight")
        plt.clf()


    def _gen_review_output(self, merge_hash, link_hash, cluster_hash, cluster_approach):
        save_path = gen_path(self.date1, self.date2, counties=self.counties, filename="evaluation.csv", _hash=cluster_hash)
        match_path_left, match_path_right = gen_path(
            self.date1, self.date2, suffix="_match.csv", counties=self.counties, _hash=merge_hash
        )
        classify_path_left, classify_path_right = gen_path(
            self.date1, self.date2, suffix="_classify.csv", counties=self.counties, _hash=merge_hash
        )
        links_path = gen_path(self.date1, self.date2, filename="links.csv", counties=self.counties, _hash=link_hash)
        voter_ids_left = pd.read_csv(match_path_left, usecols=["voter_ids"], dtype="str").voter_ids.str.split("|")
        voter_ids_right = pd.read_csv(match_path_right , usecols=["voter_ids"], dtype="str").voter_ids.str.split("|")
        links = []
        for left in (map(int, x) for x in voter_ids_left.values):
            while left:
                first, *left = left
                links.extend(((first, "left"), (other, "left")) for other in left)
        for right in (map(int, x) for x in voter_ids_right.values):
            while right:
                first, *right = right
                links.extend(((first, "right"), (other, "right")) for other in right)
        same_voter_ids = set(map(int, voter_ids_left.explode()))
        same_voter_ids.intersection_update(map(int, voter_ids_right.explode()))
        links.extend(((voter_id, "left"), (voter_id, "right")) for voter_id in same_voter_ids)
        graph = nx.from_edgelist(links)
        links = []
        for component in nx.connected_components(graph):
            left = []
            right = []
            for x, label in component:
                if label == "left":
                    left.append(x)
                elif label == "right":
                    right.append(x)
                else:
                    raise ValueError(f"unknown label {label}")
            links.extend(product(left, right))
        links = pd.DataFrame(links, columns=["left", "right"])

        voter_ids_left = pd.read_csv(classify_path_left, usecols=["voter_ids"], dtype="str").voter_ids
        voter_ids_right = pd.read_csv(classify_path_right , usecols=["voter_ids"], dtype="str").voter_ids
        output_links = pd.read_csv(links_path)

        output_links = output_links[output_links.prob > 0.5]
        found_links = link_approaches[cluster_approach](output_links)
        output_links = pd.DataFrame(zip(found_links["left"], found_links["right"]), columns=["left", "right"])
        output_links["left"] = voter_ids_left.loc[output_links["left"].values].values
        output_links["left"] = output_links["left"].str.split("|")
        output_links["right"] = voter_ids_right.loc[output_links["right"].values].values
        output_links["right"] = output_links["right"].str.split("|")
        output_links = output_links.explode("left")
        output_links = output_links.explode("right")

        links = links.append(output_links)
        links.to_csv(save_path, index=False)

    def _evaluate(self, _hash):
        gold_path = gen_path(self.date1, self.date2, counties=self.counties, filename="review_links.csv")
        gold = pd.read_csv(gold_path)
        gold["target"] = 1
        links_path = gen_path(
            self.date1,
            self.date2,
            filename="evaluation.csv",
            counties=self.counties,
            _hash=_hash,
        )
        links = pd.read_csv(links_path)
        links["predict"] = 1
        combined = gold.merge(links, how="outer", on=["left", "right"]).fillna(0)
        combined["target"] = combined["target"].astype("int8")
        combined["predict"] = combined["predict"].astype("int8")
        y_true = combined["target"]
        y_pred = combined["predict"]
        print("{:<7.3} {:<7.3}".format(precision_score(y_true, y_pred), recall_score(y_true, y_pred)))
        combined = combined[combined["left"] != combined["right"]]
        y_true = combined["target"]
        y_pred = combined["predict"]
        print("{:<7.3} {:<7.3}".format(precision_score(y_true, y_pred), recall_score(y_true, y_pred)))

    def to_classifier(self, date1, date2, _config, max_non_matches=None):
        if isinstance(_config, DedupeConfig):
            clf = DedupeClassifier(date1, date2)
            _config = _config.to_link_config()
        elif isinstance(_config, LinkConfig):
            clf = LinkClassifier(date1, date2)
        else:
            raise ValueError("_config has to be DedupeConfig or LinkConfig")
        path_left = gen_path(self.date1, filename="parsed.csv", counties=self.counties)
        path_right = gen_path(self.date2, filename="parsed.csv", counties=self.counties)
        data_left = pd.read_csv(path_left, keep_default_na=False, dtype="str")
        data_right = pd.read_csv(path_right, keep_default_na=False, dtype="str")
        data_left, data_right = _config._apply(data_left, data_right, voter_ids=True)
        gold_path = gen_path(self.date1, self.date2, counties=self.counties, filename="review_links.csv")
        gold = pd.read_csv(gold_path)
        candidates = pd.DataFrame(_config._key.block(data_left, data_right), columns=["left", "right"])
        rows_left = data_left.voter_ids.astype("int64").to_frame().reset_index().set_index("voter_ids").loc[gold.left.values]["index"].values
        rows_right = data_right.voter_ids.astype("int64").to_frame().reset_index().set_index("voter_ids").loc[gold.right.values]["index"].values
        candidates = candidates.append(pd.DataFrame({"left": rows_left, "right": rows_right, "target": 1}))
        candidates = candidates.drop_duplicates(subset=["left", "right"], keep="last")
        candidates = candidates.fillna(0)
        if max_non_matches is not None and sum(candidates["target"] == 0) > max_non_matches:
            candidates = candidates[candidates["target"] == 1].append(candidates[candidates["target"] == 0].sample(n=max_non_matches, replace=False))
        train = apply_metrics(candidates.set_index(["left", "right"]).index, data_left, data_right, _config)
        clf.fit(train, candidates.target)
        return clf

def _extract(date, counties, chunksize=3000000):
    translated_counties = [county_keys[county] for county in counties]
    src_path = gen_path(date, filename="parsed.csv")

    mode = "w"
    header = True
    chunk_iter = pd.read_csv(
        src_path, dtype="str", keep_default_na=False, chunksize=chunksize
    )
    dest_path = gen_path(date, filename="parsed.csv", counties=counties, mkdir=True)
    for chunk in chunk_iter:
        chunk[chunk["county"].isin(translated_counties)].rename(
            columns={"voter_id": "voter_ids"}
        ).to_csv(dest_path, mode=mode, header=header, index=False)
        mode = "a"
        header = False
        del chunk
