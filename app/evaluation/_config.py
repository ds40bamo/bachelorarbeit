from app.config import DedupeConfig, LinkConfig
from app.utils import fullname, first_name_rel_freq, middle_name_rel_freq, last_name_rel_freq

dedupe_config1 = DedupeConfig()
dedupe_config1.add("first_name", "first_name", "jaro_winkler_empty_no_info")
dedupe_config1.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
dedupe_config1.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config1.add("birth_date", "birth_date", "date")
dedupe_config1.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
dedupe_config1.add("gender", "gender", "exact_empty_no_info")
dedupe_config1.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
dedupe_config1.add("race", "race", "exact_empty_no_info")
dedupe_config1.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
dedupe_config1.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config1.add("birth_date", "birth_date", "age_diff")
dedupe_config1.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
dedupe_config1.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
dedupe_config1.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
dedupe_config1.add_complex_field("fullname", fullname)
dedupe_config1.add_complex_field("first_name_rel_freq", first_name_rel_freq)
dedupe_config1.add_complex_field("middle_name_rel_freq", middle_name_rel_freq)
dedupe_config1.add_complex_field("last_name_rel_freq", last_name_rel_freq)
dedupe_config1.add_list_field("middle_name", "|")
dedupe_config1.add_list_field("name_suffix", " ")
dedupe_config1.add_list_field("last_name", "|")

link_config1 = LinkConfig()
link_config1.add("first_name", "first_name", "jaro_winkler_empty_no_info")
link_config1.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
link_config1.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config1.add("birth_date", "birth_date", "date")
link_config1.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
link_config1.add("gender", "gender", "exact_empty_no_info")
link_config1.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
link_config1.add("race", "race", "exact_empty_no_info")
link_config1.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
link_config1.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config1.add("birth_date", "birth_date", "age_diff")
link_config1.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
link_config1.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
link_config1.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
link_config1.add_complex_field_left("fullname", fullname)
link_config1.add_complex_field_right("fullname", fullname)
link_config1.add_complex_field_left("first_name_rel_freq", first_name_rel_freq)
link_config1.add_complex_field_right("first_name_rel_freq", first_name_rel_freq)
link_config1.add_complex_field_left("middle_name_rel_freq", middle_name_rel_freq)
link_config1.add_complex_field_right("middle_name_rel_freq", middle_name_rel_freq)
link_config1.add_complex_field_left("last_name_rel_freq", last_name_rel_freq)
link_config1.add_complex_field_right("last_name_rel_freq", last_name_rel_freq)
link_config1.add_list_field_left("middle_name", "|")
link_config1.add_list_field_right("middle_name", "|")
link_config1.add_list_field_left("name_suffix", " ")
link_config1.add_list_field_right("name_suffix", " ")
link_config1.add_list_field_left("last_name", "|")
link_config1.add_list_field_right("last_name", "|")
link_config1.add_major_field("first_name")
link_config1.add_major_field("last_name")
link_config1.add_major_field("name_suffix")
link_config1.add_major_field("birth_date")
link_config1.add_major_field("gender")
link_config1.add_major_field("race")


dedupe_config2 = DedupeConfig()
dedupe_config2.add("first_name", "first_name", "damerau_levenshtein_empty_no_info")
dedupe_config2.add("middle_name", "middle_name", "symmetric_monge_elkan[damerau_levenshtein]")
dedupe_config2.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[damerau_levenshtein]")
dedupe_config2.add("birth_date", "birth_date", "date")
dedupe_config2.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
dedupe_config2.add("gender", "gender", "exact_empty_no_info")
dedupe_config2.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
dedupe_config2.add("race", "race", "exact_empty_no_info")
dedupe_config2.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
dedupe_config2.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[damerau_levenshtein]")
dedupe_config2.add("birth_date", "birth_date", "age_diff")
dedupe_config2.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
dedupe_config2.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
dedupe_config2.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
dedupe_config2.add_complex_field("fullname", fullname)
dedupe_config2.add_complex_field("first_name_rel_freq", first_name_rel_freq)
dedupe_config2.add_complex_field("middle_name_rel_freq", middle_name_rel_freq)
dedupe_config2.add_complex_field("last_name_rel_freq", last_name_rel_freq)
dedupe_config2.add_list_field("middle_name", "|")
dedupe_config2.add_list_field("name_suffix", " ")
dedupe_config2.add_list_field("last_name", "|")

link_config2 = LinkConfig()
link_config2.add("first_name", "first_name", "damerau_levenshtein_empty_no_info")
link_config2.add("middle_name", "middle_name", "symmetric_monge_elkan[damerau_levenshtein]")
link_config2.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[damerau_levenshtein]")
link_config2.add("birth_date", "birth_date", "date")
link_config2.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
link_config2.add("gender", "gender", "exact_empty_no_info")
link_config2.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
link_config2.add("race", "race", "exact_empty_no_info")
link_config2.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
link_config2.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[damerau_levenshtein]")
link_config2.add("birth_date", "birth_date", "age_diff")
link_config2.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
link_config2.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
link_config2.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
link_config2.add_complex_field_left("fullname", fullname)
link_config2.add_complex_field_right("fullname", fullname)
link_config2.add_complex_field_left("first_name_rel_freq", first_name_rel_freq)
link_config2.add_complex_field_right("first_name_rel_freq", first_name_rel_freq)
link_config2.add_complex_field_left("middle_name_rel_freq", middle_name_rel_freq)
link_config2.add_complex_field_right("middle_name_rel_freq", middle_name_rel_freq)
link_config2.add_complex_field_left("last_name_rel_freq", last_name_rel_freq)
link_config2.add_complex_field_right("last_name_rel_freq", last_name_rel_freq)
link_config2.add_list_field_left("middle_name", "|")
link_config2.add_list_field_right("middle_name", "|")
link_config2.add_list_field_left("name_suffix", " ")
link_config2.add_list_field_right("name_suffix", " ")
link_config2.add_list_field_left("last_name", "|")
link_config2.add_list_field_right("last_name", "|")
link_config2.add_major_field("first_name")
link_config2.add_major_field("last_name")
link_config2.add_major_field("name_suffix")
link_config2.add_major_field("birth_date")
link_config2.add_major_field("gender")
link_config2.add_major_field("race")

dedupe_config3 = DedupeConfig()
dedupe_config3.add("first_name", "first_name", "jaro_winkler_empty_no_info")
dedupe_config3.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
dedupe_config3.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config3.add("birth_date", "birth_date", "date")
dedupe_config3.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
dedupe_config3.add("gender", "gender", "exact_empty_no_info")
dedupe_config3.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
dedupe_config3.add("race", "race", "exact_empty_no_info")
dedupe_config3.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
dedupe_config3.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config3.add_complex_field("fullname", fullname)
dedupe_config3.add_list_field("middle_name", "|")
dedupe_config3.add_list_field("name_suffix", " ")
dedupe_config3.add_list_field("last_name", "|")

link_config3 = LinkConfig()
link_config3.add("first_name", "first_name", "jaro_winkler_empty_no_info")
link_config3.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
link_config3.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config3.add("birth_date", "birth_date", "date")
link_config3.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
link_config3.add("gender", "gender", "exact_empty_no_info")
link_config3.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
link_config3.add("race", "race", "exact_empty_no_info")
link_config3.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
link_config3.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config3.add_complex_field_left("fullname", fullname)
link_config3.add_complex_field_right("fullname", fullname)
link_config3.add_list_field_left("middle_name", "|")
link_config3.add_list_field_right("middle_name", "|")
link_config3.add_list_field_left("name_suffix", " ")
link_config3.add_list_field_right("name_suffix", " ")
link_config3.add_list_field_left("last_name", "|")
link_config3.add_list_field_right("last_name", "|")
link_config3.add_major_field("first_name")
link_config3.add_major_field("last_name")
link_config3.add_major_field("name_suffix")
link_config3.add_major_field("birth_date")
link_config3.add_major_field("gender")
link_config3.add_major_field("race")

dedupe_config4 = DedupeConfig()
dedupe_config4.add("first_name", "first_name", "jaro_winkler_empty_no_info")
dedupe_config4.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
dedupe_config4.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config4.add("birth_date", "birth_date", "date_alternative")
dedupe_config4.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
dedupe_config4.add("gender", "gender", "exact_empty_no_info")
dedupe_config4.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
dedupe_config4.add("race", "race", "exact_empty_no_info")
dedupe_config4.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
dedupe_config4.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
dedupe_config4.add("birth_date", "birth_date", "age_diff")
dedupe_config4.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
dedupe_config4.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
dedupe_config4.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
dedupe_config4.add_complex_field("fullname", fullname)
dedupe_config4.add_complex_field("first_name_rel_freq", first_name_rel_freq)
dedupe_config4.add_complex_field("middle_name_rel_freq", middle_name_rel_freq)
dedupe_config4.add_complex_field("last_name_rel_freq", last_name_rel_freq)
dedupe_config4.add_list_field("middle_name", "|")
dedupe_config4.add_list_field("name_suffix", " ")
dedupe_config4.add_list_field("last_name", "|")

link_config4 = LinkConfig()
link_config4.add("first_name", "first_name", "jaro_winkler_empty_no_info")
link_config4.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
link_config4.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config4.add("birth_date", "birth_date", "date_alternative")
link_config4.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
link_config4.add("gender", "gender", "exact_empty_no_info")
link_config4.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
link_config4.add("race", "race", "exact_empty_no_info")
link_config4.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
link_config4.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
link_config4.add("birth_date", "birth_date", "age_diff")
link_config4.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
link_config4.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
link_config4.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
link_config4.add_complex_field_left("fullname", fullname)
link_config4.add_complex_field_right("fullname", fullname)
link_config4.add_complex_field_left("first_name_rel_freq", first_name_rel_freq)
link_config4.add_complex_field_right("first_name_rel_freq", first_name_rel_freq)
link_config4.add_complex_field_left("middle_name_rel_freq", middle_name_rel_freq)
link_config4.add_complex_field_right("middle_name_rel_freq", middle_name_rel_freq)
link_config4.add_complex_field_left("last_name_rel_freq", last_name_rel_freq)
link_config4.add_complex_field_right("last_name_rel_freq", last_name_rel_freq)
link_config4.add_list_field_left("middle_name", "|")
link_config4.add_list_field_right("middle_name", "|")
link_config4.add_list_field_left("name_suffix", " ")
link_config4.add_list_field_right("name_suffix", " ")
link_config4.add_list_field_left("last_name", "|")
link_config4.add_list_field_right("last_name", "|")
link_config4.add_major_field("first_name")
link_config4.add_major_field("last_name")
link_config4.add_major_field("name_suffix")
link_config4.add_major_field("birth_date")
link_config4.add_major_field("gender")
link_config4.add_major_field("race")
