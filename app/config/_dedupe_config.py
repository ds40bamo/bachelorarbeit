import pandas as pd

from app.data import gen_path
from app.utils import (first_name_rel_freq, fullname, last_name_rel_freq,
                       middle_name_rel_freq)

from ._link_config import LinkConfig


def _read_classify_data(path, fields, chunksize, skiprows, voter_ids=False):
    if voter_ids:
        index_col = "voter_ids"
        extra = set(["voter_ids"])
    else:
        index_col = "voter_id"
        extra = set(["voter_id"])
    return pd.read_csv(
        path,
        dtype="str",
        usecols=set(fields) | extra,
        keep_default_na=False,
        index_col=index_col,
        chunksize=chunksize,
        skiprows=skiprows,
    )


class DedupeConfig:
    def __init__(self):
        self._link_config = LinkConfig()

    def add(self, col1, col2, metric):
        self._link_config.add(col1, col2, metric)
        self._link_config.add(col2, col1, metric)

    def set_key(self, key):
        self._link_config.set_key(key)

    def add_list_field(self, col, separator):
        self._link_config.add_list_field_left(col, separator)
        self._link_config.add_list_field_right(col, separator)

    def add_complex_field(self, col, specification):
        self._link_config.add_complex_field_left(col, specification)
        self._link_config.add_complex_field_right(col, specification)

    def _atom_fields(self):
        return self._link_config._atom_fields_left()

    def _used_fields(self):
        return self._link_config._used_fields_left()

    def _complex_fields(self):
        return self._link_config._complex_fields_left

    def _list_fields(self):
        return self._link_config._list_fields_left

    def _apply(self, data):
        for field, separator in self._list_fields().items():
            if field in data:
                data[field] = (
                    data[field]
                    .str.split(separator)
                    .map(lambda l: l if l != [""] else [])
                )

        for field, specification in self._complex_fields().items():
            function = specification["function"]
            data[field] = function(data)

        return data[self._used_fields()]

    def iterate_classify_chunks(
        self, date, chunksize=3000000, counties=None, voter_ids=False
    ):
        parsed_path = gen_path(date, filename="parsed.csv", counties=counties)
        skiprows = 1
        while True:
            chunk_iter = _read_classify_data(
                parsed_path,
                self._atom_fields(),
                chunksize,
                skiprows=range(1, skiprows),
                voter_ids=voter_ids,
            )
            chunk1 = next(chunk_iter)
            if len(chunk1) == 0:
                break
            chunk1 = self._apply(chunk1)
            yield chunk1, chunk1
            for chunk2 in map(self._apply, chunk_iter):
                yield chunk1, chunk2
            skiprows += chunksize

    def load_classify(self, date, counties=None):
        parsed_path = gen_path(date, filename="parsed.csv", counties=counties)
        data = _read_classify_data(parsed_path, self._atom_fields(), None, None)
        return self._apply(data)

    def to_link_config(self):
        return self._link_config

    def add_major_field(self, col):
        self._link_config.add_major_field(col)

    def hash(self):
        return self._link_config.hash()


DEFAULT_DEDUPE_CONFIG = DedupeConfig()
DEFAULT_DEDUPE_CONFIG.add("first_name", "first_name", "jaro_winkler_empty_no_info")
DEFAULT_DEDUPE_CONFIG.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
DEFAULT_DEDUPE_CONFIG.add(
    "last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]"
)
DEFAULT_DEDUPE_CONFIG.add("birth_date", "birth_date", "date")
DEFAULT_DEDUPE_CONFIG.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
DEFAULT_DEDUPE_CONFIG.add("gender", "gender", "exact_empty_no_info")
DEFAULT_DEDUPE_CONFIG.add(("latitude", "longitude"), ("latitude", "longitude"), "haversine")
DEFAULT_DEDUPE_CONFIG.add("race", "race", "exact_empty_no_info")
DEFAULT_DEDUPE_CONFIG.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
DEFAULT_DEDUPE_CONFIG.add(
    "fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]"
)
DEFAULT_DEDUPE_CONFIG.add("birth_date", "birth_date", "age_diff")
DEFAULT_DEDUPE_CONFIG.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
DEFAULT_DEDUPE_CONFIG.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
DEFAULT_DEDUPE_CONFIG.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
DEFAULT_DEDUPE_CONFIG.add_complex_field("fullname", fullname)
DEFAULT_DEDUPE_CONFIG.add_complex_field("first_name_rel_freq", first_name_rel_freq)
DEFAULT_DEDUPE_CONFIG.add_complex_field("middle_name_rel_freq", middle_name_rel_freq)
DEFAULT_DEDUPE_CONFIG.add_complex_field("last_name_rel_freq", last_name_rel_freq)
DEFAULT_DEDUPE_CONFIG.add_list_field("middle_name", "|")
DEFAULT_DEDUPE_CONFIG.add_list_field("name_suffix", " ")
DEFAULT_DEDUPE_CONFIG.add_list_field("last_name", "|")
