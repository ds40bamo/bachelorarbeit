import inspect
import logging
from collections import defaultdict

import pandas as pd
import numpy as np

from app.data import gen_path


def _read_header(path, usecols):
    return pd.read_csv(
        path,
        dtype="str",
        usecols=set(usecols) | {"voter_id"},
        keep_default_na=False,
        index_col="voter_id",
        nrows=0,
    )


def _data_iterator(path, usecols, chunksize=1000000):
    return pd.read_csv(
        path,
        dtype="str",
        usecols=set(usecols) | {"voter_id"},
        keep_default_na=False,
        index_col="voter_id",
        chunksize=chunksize,
    )


def _read_classify_data(path, fields, chunksize=None, voter_ids=False):
    if voter_ids:
        fields = set(fields)
        fields.add("voter_ids")
    return pd.read_csv(
        path,
        dtype="str",
        usecols=fields,
        keep_default_na=False,
        chunksize=chunksize
    )

def _read_train_data_from_voter_ids(path, voter_ids, usecols, chunksize=1000000):
    data = []
    for chunk in pd.read_csv(
        path,
        dtype="str",
        usecols=set(usecols) | {"voter_id"},
        keep_default_na=False,
        chunksize=chunksize
    ):
        data.append(chunk[chunk["voter_id"].isin(voter_ids)])
    return pd.concat(data)


def _read_train_data(
    path1,
    path2,
    left_fields,
    right_fields,
    major_fields,
    maxminor,
    chunksize=1000000,
    voter_id=False,
    registration_date=False
):
    left_cols = set(major_fields) | set(left_fields)
    right_cols = set(major_fields) | set(right_fields)
    if registration_date:
        left_cols.add("registration_date")
        right_cols.add("registration_date")

    header_left = _read_header(path1, usecols=left_cols)
    header_right = _read_header(path2, usecols=right_cols)

    major_data_left = header_left
    minor_data_left = header_left

    major_data_right = header_right
    minor_data_right = header_right

    iter1 = _data_iterator(path1, usecols=left_cols, chunksize=chunksize)
    iter2 = _data_iterator(path2, usecols=right_cols, chunksize=chunksize)

    for chunk1, chunk2 in zip(iter1, iter2):
        if not (chunk1.index == chunk2.index).all():
            raise ValueError("corresponding records are not in the same row in the CSV")

        major_selector = (chunk1[major_fields] != chunk2[major_fields]).any(axis=1)

        major_data_left = major_data_left.append(chunk1[major_selector])
        major_data_right = major_data_right.append(chunk2[major_selector])

        minor_count = len(minor_data_left)

        if maxminor is None or minor_count < maxminor:
            minor_selector = ~major_selector
            upper = maxminor - minor_count
            minor_data_left = minor_data_left.append(chunk1[minor_selector])
            minor_data_right = minor_data_right.append(chunk2[minor_selector])
            if len(minor_data_left) > maxminor:
                idx = np.random.choice(range(len(minor_data_left)), size=maxminor, replace=False)
                minor_data_left = minor_data_left.iloc[idx]
                minor_data_right = minor_data_right.iloc[idx]
    data1 = major_data_left.append(minor_data_left)
    data2 = major_data_right.append(minor_data_right)
    data1 = data1.reset_index(drop=not voter_id)
    data2 = data2.reset_index(drop=not voter_id)
    return data1, data2


class BaseLinkConfig:
    def __init__(self):
        self._metrics = defaultdict(lambda: defaultdict(set))
        self._list_fields_left = dict()
        self._list_fields_right = dict()
        self._complex_fields_left = dict()
        self._complex_fields_right = dict()
        self._single_feature_left = set()
        self._single_feature_right = set()
        self._major_fields = set()

    def add(self, col_left, col_right, metric):
        self._metrics[col_left][col_right].add(metric)

    def add_list_field_left(self, col, separator):
        self._list_fields_left[col] = separator

    def add_list_field_right(self, col, separator):
        self._list_fields_right[col] = separator

    def add_complex_field_left(self, col, field_definition):
        self._complex_fields_left[col] = {
            "dependencies": field_definition["dependencies"],
            "function": field_definition["function"],
        }

    def add_complex_field_right(self, col, field_definition):
        self._complex_fields_right[col] = {
            "dependencies": field_definition["dependencies"],
            "function": field_definition["function"],
        }

    def add_single_feature_left(self, col):
        self._single_feature_left.add(col)

    def add_single_feature_right(self, col):
        self._single_feature_right.add(col)

    def add_complex_field_right(self, col, field_definition):
        self._complex_fields_right[col] = {
            "dependencies": field_definition["dependencies"],
            "function": field_definition["function"],
        }

    def _used_fields_left(self):
        fields = set(self._single_feature_left)
        for key in self._metrics.keys():
            key = self._to_field_iter(key)
            for atom in key:
                fields.add(atom)
        return fields

    def _used_fields_right(self):
        fields = set(self._single_feature_right)
        for value in self._metrics.values():
            for key in value.keys():
                key = self._to_field_iter(key)
                for atom in key:
                    fields.add(atom)
        return fields

    def _atom_fields(self, table):
        if table == "left":
            complex_fields = self._complex_fields_left
            used_fields = self._used_fields_left()
            used_fields.update(self._single_feature_left)
        elif table == "right":
            complex_fields = self._complex_fields_right
            used_fields = self._used_fields_right()
            used_fields.update(self._single_feature_right)

        fields = set()
        for field in used_fields:
            if not field in complex_fields:
                fields.add(field)
            else:
                fields.update(complex_fields[field]["dependencies"])
        return fields

    def _atom_fields_left(self):
        return self._atom_fields("left")

    def _atom_fields_right(self):
        return self._atom_fields("right")

    @staticmethod
    def _to_field_iter(key):
        if isinstance(key, str):
            key = [key]
        elif not isinstance(key, tuple):
            raise ValueError(f"unknown datatype of key: {key}")
        return key

    def add_major_field(self, col):
        self._major_fields.add(col)

    def load_train_from_voter_ids(self, date1, date2, voter_ids):
        path1, path2 = gen_path(date1, date2, train=True, suffix="_train.csv")
        data_left = _read_train_data_from_voter_ids(path1, voter_ids["left"], self._atom_fields_left())
        data_right = _read_train_data_from_voter_ids(path2, voter_ids["right"], self._atom_fields_right())
        data_left, data_right = self._apply(data_left, data_right, voter_id=True)
        return data_left, data_right

    def load_train(self, date1, date2, maxminor=500000, voter_id=False, registration_date=False, skip_apply=False):
        train_path_left, train_path_right = gen_path(
            date1, date2, train=True, suffix="_train.csv"
        )

        logger = logging.getLogger(inspect.currentframe().f_code.co_name)
        logger.setLevel(logging.INFO)

        logger.info("loading data")
        major_fields = self._major_fields
        data_left, data_right = _read_train_data(
            train_path_left,
            train_path_right,
            self._atom_fields_left(),
            self._atom_fields_right(),
            major_fields,
            maxminor,
            voter_id=voter_id,
            registration_date=registration_date
        )
        if (data_left.index != data_right.index).any():
            raise ValueError("corresponding records are not in the same row in the CSV")

        logger.info("done")

        if skip_apply:
            return data_left, data_right

        data_left, data_right = self._apply(data_left, data_right, voter_id=voter_id, registration_date=registration_date)

        return data_left, data_right

    def iterate_classify_chunks(self, date1, date2, chunksize=3000000, counties=None, _hash=None):
        classify_path_left, classify_path_right = gen_path(
            date1, date2, suffix="_classify.csv", counties=counties, _hash=_hash
        )
        for chunk1 in _read_classify_data(classify_path_left, self._atom_fields_left(), chunksize=chunksize):
            for chunk2 in _read_classify_data(classify_path_right, self._atom_fields_right(), chunksize=chunksize):
                yield self._apply(chunk1.copy(), chunk2)


    def load_classify(self, date1, date2, counties=None, voter_ids=False):
        classify_path_left, classify_path_right = gen_path(
            date1, date2, suffix="_classify.csv", counties=counties
        )

        data_left = _read_classify_data(classify_path_left, self._atom_fields_left(), voter_ids=voter_ids)
        data_right = _read_classify_data(classify_path_right, self._atom_fields_right(), voter_ids=voter_ids)

        return self._apply(data_left, data_right, voter_ids=voter_ids)

    def _apply(self, data_left, data_right, voter_ids=False, voter_id=False, registration_date=False):
        processed_data = []
        for data, list_fields, complex_fields, used_fields in [
            (
                data_left,
                self._list_fields_left,
                self._complex_fields_left,
                self._used_fields_left(),
            ),
            (
                data_right,
                self._list_fields_right,
                self._complex_fields_right,
                self._used_fields_right(),
            ),
        ]:
            for field, separator in list_fields.items():
                if field in data:
                    data[field] = data[field].str.split(separator).map(lambda l: l if l != [""] else [])

            for field, specification in complex_fields.items():
                function = specification["function"]
                data[field] = function(data).values

            used_fields = set(used_fields)
            if voter_ids:
                used_fields.add("voter_ids")
            if voter_id:
                used_fields.add("voter_id")
            if registration_date:
                used_fields.add("registration_date")
            processed_data.append(data[used_fields])

        data_left, data_right = processed_data

        return data_left, data_right
