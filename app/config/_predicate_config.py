from collections import defaultdict
from itertools import chain

from ._base_link_config import BaseLinkConfig

class PredicateConfig(BaseLinkConfig):
    def __init__(self):
        super().__init__()

    @property
    def predicate_config(self):
        return self._metrics

    def raise_invalid(self, data1, data2):
        for col1, col2_method in self.predicate_config.items():
            if col1 not in data1:
                raise ValueError(f"{col1} is not in the first training set")
            for col2 in col2_method.keys():
                if col2 not in data2:
                    raise ValueError(f"{col2} is not in the second training set")

    def reverse(self):
        reverse_dict = defaultdict(lambda: defaultdict(set))
        for col1, col2_methods in self.predicate_config.items():
            for col2, methods in col2_methods.items():
                for method in methods:
                    reverse_dict[method][col1].add(col2)
        return reverse_dict


DEFAULT_PREDICATE_CONFIG = PredicateConfig()
DEFAULT_PREDICATE_CONFIG.add("county", "county", "exact")
DEFAULT_PREDICATE_CONFIG.add("first_name", "first_name", "exact")
DEFAULT_PREDICATE_CONFIG.add("first_name", "last_name", "exact")
DEFAULT_PREDICATE_CONFIG.add("middle_name", "middle_name", "exact")
DEFAULT_PREDICATE_CONFIG.add("last_name", "last_name", "exact")
DEFAULT_PREDICATE_CONFIG.add("last_name", "last_name", "soundex")
DEFAULT_PREDICATE_CONFIG.add("last_name", "last_name", "doublemetaphone")
DEFAULT_PREDICATE_CONFIG.add("last_name", "last_name", "nysiis")
DEFAULT_PREDICATE_CONFIG.add("last_name", "last_name", "3")
DEFAULT_PREDICATE_CONFIG.add("last_name", "first_name", "exact")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "exact")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "date_3")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "year")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "day_month")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "day_year")
DEFAULT_PREDICATE_CONFIG.add("birth_date", "birth_date", "month_year")
DEFAULT_PREDICATE_CONFIG.add("gender", "gender", "exact")
DEFAULT_PREDICATE_CONFIG.add("address", "address", "exact")
DEFAULT_PREDICATE_CONFIG.add("city", "city", "exact")
DEFAULT_PREDICATE_CONFIG.add("zipcode", "zipcode", "exact")
DEFAULT_PREDICATE_CONFIG.add("zipcode", "zipcode", "3")
DEFAULT_PREDICATE_CONFIG.add_major_field("first_name")
DEFAULT_PREDICATE_CONFIG.add_major_field("last_name")
DEFAULT_PREDICATE_CONFIG.add_major_field("name_suffix")
DEFAULT_PREDICATE_CONFIG.add_major_field("birth_date")
DEFAULT_PREDICATE_CONFIG.add_major_field("gender")
DEFAULT_PREDICATE_CONFIG.add_major_field("race")
DEFAULT_PREDICATE_CONFIG.add_list_field_left("middle_name", "|")
DEFAULT_PREDICATE_CONFIG.add_list_field_right("middle_name", "|")
DEFAULT_PREDICATE_CONFIG.add_list_field_left("name_suffix", " ")
DEFAULT_PREDICATE_CONFIG.add_list_field_right("name_suffix", " ")
DEFAULT_PREDICATE_CONFIG.add_list_field_left("last_name", "|")
DEFAULT_PREDICATE_CONFIG.add_list_field_right("last_name", "|")
