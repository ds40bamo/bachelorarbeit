from collections import defaultdict
from itertools import chain
from hashlib import sha256

from app.utils import fullname, gen_hash, first_name_rel_freq, middle_name_rel_freq, last_name_rel_freq

from ._base_link_config import BaseLinkConfig

class LinkConfig(BaseLinkConfig):
    def __init__(self):
        super().__init__()

    def _used_fields_left(self):
        fields = super()._used_fields_left()
        try:
            fields.update(self._key.used_fields_left())
        except AttributeError as ex:
            raise AttributeError(str(ex) + ", you need to call 'set_key' first")
        return fields

    def _used_fields_right(self):
        fields = super()._used_fields_left()
        try:
            fields.update(self._key.used_fields_left())
        except AttributeError as ex:
            raise AttributeError(str(ex) + ", you need to call 'set_key' first")
        return fields

    @property
    def metric_config(self):
        return self._metrics

    def set_key(self, key):
        self._key = key

    def hash(self):
        hasher = sha256()
        gen_hash(hasher, self._metrics)
        gen_hash(hasher, self._list_fields_left)
        gen_hash(hasher, self._list_fields_right)
        gen_hash(hasher, self._complex_fields_left)
        gen_hash(hasher, self._complex_fields_right)
        gen_hash(hasher, self._key.hash())
        return hasher.hexdigest()


DEFAULT_LINK_CONFIG = LinkConfig()
DEFAULT_LINK_CONFIG.add("first_name", "first_name", "jaro_winkler_empty_no_info")
DEFAULT_LINK_CONFIG.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
DEFAULT_LINK_CONFIG.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
DEFAULT_LINK_CONFIG.add("birth_date", "birth_date", "date")
DEFAULT_LINK_CONFIG.add("birth_date", "birth_date", "age_diff")
DEFAULT_LINK_CONFIG.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
DEFAULT_LINK_CONFIG.add("gender", "gender", "exact_empty_no_info")
DEFAULT_LINK_CONFIG.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
DEFAULT_LINK_CONFIG.add("race", "race", "exact_empty_no_info")
DEFAULT_LINK_CONFIG.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
DEFAULT_LINK_CONFIG.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
DEFAULT_LINK_CONFIG.add("first_name_rel_freq", "first_name_rel_freq", "freq_average")
DEFAULT_LINK_CONFIG.add("middle_name_rel_freq", "middle_name_rel_freq", "freq_average")
DEFAULT_LINK_CONFIG.add("last_name_rel_freq", "last_name_rel_freq", "freq_average")
DEFAULT_LINK_CONFIG.add_complex_field_left("fullname", fullname)
DEFAULT_LINK_CONFIG.add_complex_field_right("fullname", fullname)
DEFAULT_LINK_CONFIG.add_complex_field_left("first_name_rel_freq", first_name_rel_freq)
DEFAULT_LINK_CONFIG.add_complex_field_right("first_name_rel_freq", first_name_rel_freq)
DEFAULT_LINK_CONFIG.add_complex_field_left("middle_name_rel_freq", middle_name_rel_freq)
DEFAULT_LINK_CONFIG.add_complex_field_right("middle_name_rel_freq", middle_name_rel_freq)
DEFAULT_LINK_CONFIG.add_complex_field_left("last_name_rel_freq", last_name_rel_freq)
DEFAULT_LINK_CONFIG.add_complex_field_right("last_name_rel_freq", last_name_rel_freq)
DEFAULT_LINK_CONFIG.add_list_field_left("middle_name", "|")
DEFAULT_LINK_CONFIG.add_list_field_right("middle_name", "|")
DEFAULT_LINK_CONFIG.add_list_field_left("name_suffix", " ")
DEFAULT_LINK_CONFIG.add_list_field_right("name_suffix", " ")
DEFAULT_LINK_CONFIG.add_list_field_left("last_name", "|")
DEFAULT_LINK_CONFIG.add_list_field_right("last_name", "|")
DEFAULT_LINK_CONFIG.add_major_field("first_name")
DEFAULT_LINK_CONFIG.add_major_field("last_name")
DEFAULT_LINK_CONFIG.add_major_field("name_suffix")
DEFAULT_LINK_CONFIG.add_major_field("birth_date")
DEFAULT_LINK_CONFIG.add_major_field("gender")
DEFAULT_LINK_CONFIG.add_major_field("race")


DEFAULT_FILTER_CONFIG = LinkConfig()
DEFAULT_FILTER_CONFIG.add("first_name", "first_name", "jaro_winkler_empty_no_info")
DEFAULT_FILTER_CONFIG.add("middle_name", "middle_name", "symmetric_monge_elkan[jaro_winkler]")
DEFAULT_FILTER_CONFIG.add("last_name", "last_name", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
DEFAULT_FILTER_CONFIG.add("birth_date", "birth_date", "date")
DEFAULT_FILTER_CONFIG.add("registration_date", "registration_date", "date")
DEFAULT_FILTER_CONFIG.add("name_suffix", "name_suffix", "symmetric_monge_elkan[exact]")
DEFAULT_FILTER_CONFIG.add("gender", "gender", "exact_empty_no_info")
DEFAULT_FILTER_CONFIG.add(
    ("latitude", "longitude"), ("latitude", "longitude"), "haversine"
)
DEFAULT_FILTER_CONFIG.add("race", "race", "exact_empty_no_info")
DEFAULT_FILTER_CONFIG.add("party_affiliation", "party_affiliation", "exact_empty_no_info")
DEFAULT_FILTER_CONFIG.add("fullname", "fullname", "symmetric_monge_elkan_empty_no_info[jaro_winkler]")
DEFAULT_FILTER_CONFIG.add_complex_field_left("fullname", fullname)
DEFAULT_FILTER_CONFIG.add_complex_field_right("fullname", fullname)
DEFAULT_FILTER_CONFIG.add_list_field_left("middle_name", "|")
DEFAULT_FILTER_CONFIG.add_list_field_right("middle_name", "|")
DEFAULT_FILTER_CONFIG.add_list_field_left("name_suffix", " ")
DEFAULT_FILTER_CONFIG.add_list_field_right("name_suffix", " ")
DEFAULT_FILTER_CONFIG.add_list_field_left("last_name", "|")
DEFAULT_FILTER_CONFIG.add_list_field_right("last_name", "|")
DEFAULT_FILTER_CONFIG.add_major_field("first_name")
DEFAULT_FILTER_CONFIG.add_major_field("last_name")
DEFAULT_FILTER_CONFIG.add_major_field("name_suffix")
DEFAULT_FILTER_CONFIG.add_major_field("birth_date")
DEFAULT_FILTER_CONFIG.add_major_field("gender")
DEFAULT_FILTER_CONFIG.add_major_field("race")
