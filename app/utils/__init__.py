import inspect
from itertools import tee

from math import inf
import pandas as pd
import numpy as np
from dateutil import parser as dateparser

from app.constants import DTYPES


def _date_format(x):
    return x.strftime("%Y%m%d")


def parse_date(date):
    if isinstance(date, list):
        return list(map(_date_format, sorted(map(dateparser.parse, date))))
    return _date_format(dateparser.parse(date))


def read_csv(path, usecols=None, index_col="voter_id", chunksize=None):
    return pd.read_csv(
        path,
        dtype=DTYPES,
        usecols=usecols,
        true_values=["Y", "True"],
        false_values=["N", "False"],
        keep_default_na=False,
        na_values=["*"],
        index_col=index_col,
        chunksize=chunksize,
    )


def read_voter_ids(path, multiple=False):
    dtype = "str" if multiple else "uint32"
    col = "voter_ids" if multiple else "voter_id"
    return pd.read_csv(path, dtype=dtype, usecols=[col])[col]


def _fullname(data):
    return (data.first_name.map(lambda x: [x]) + data.middle_name + data.last_name).map(
        lambda l: [e for e in l if e != ""]
    )

fullname = {
    "dependencies": {"first_name", "middle_name", "last_name"},
    "function": _fullname,
}

def _first_name_rel_freq(data):
    return data.first_name.groupby(data.first_name).transform(len) / len(data.first_name)

first_name_rel_freq = {
    "dependencies": {"first_name"},
    "function": _first_name_rel_freq,
}

def _middle_name_rel_freq(data):
    return data.middle_name.groupby(data.middle_name.str[0].fillna("")).transform(len) / len(data.middle_name)

middle_name_rel_freq = {
    "dependencies": {"middle_name"},
    "function": _middle_name_rel_freq,
}

def _last_name_rel_freq(data):
    return data.last_name.groupby(data.middle_name.str[0].fillna("")).transform(len) / len(data.middle_name)

last_name_rel_freq = {
    "dependencies": {"last_name"},
    "function": _last_name_rel_freq,
}

def _age_at_registration(data):
    reg = data["registration_date"].astype("datetime64[ns]")
    days = pd.Series(map(lambda x: x.days, (reg.dt.to_pydatetime() - data["birth_date"].astype("datetime64[ns]").dt.to_pydatetime())))
    return (days // 365.25).replace({inf: np.iinfo(np.int32).max, -inf: np.iinfo(np.int32).min}).fillna(np.iinfo(np.int32).max).astype("int32")

age_at_registration = {
    "dependencies": {"registration_date", "birth_date"},
    "function": _age_at_registration,
}

def gen_hash(hasher, obj):
    if isinstance(obj, bytes):
        hasher.update(obj)
    elif isinstance(obj, str):
        gen_hash(hasher, bytes(obj, "utf-8"))
    elif isinstance(obj, int) or isinstance(obj, float):
        gen_hash(hasher, repr(obj))
    elif isinstance(obj, dict):
        for field in sorted(obj, key=repr):
            gen_hash(hasher, field)
            gen_hash(hasher, obj[field])
    elif isinstance(obj, set) or isinstance(obj, tuple) or isinstance(obj, list):
        for field in sorted(obj, key=repr):
            gen_hash(hasher, field)
    elif isinstance(obj, set) or isinstance(obj, tuple):
        for field in sorted(obj, key=repr):
            gen_hash(hasher, field)
    elif callable(obj):
        gen_hash(hasher, inspect.getsource(obj))
    else:
        raise ValueError(f"cant hash object of type {type(obj)}")


def triangle(first_iter):
    try:
        while True:
            chunk1 = next(first_iter)
            yield chunk1, chunk1
            first_iter, second_iter = tee(first_iter)
            for chunk2 in second_iter:
                yield chunk1, chunk2
    except StopIteration:
        pass
