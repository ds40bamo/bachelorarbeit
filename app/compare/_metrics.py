import jellyfish
import Levenshtein as Levenshtein_modul
import numpy as np
import pandas as pd
from math import inf

vec_len = np.vectorize(len)

def build_empty_no_info(func, kind, vectorized, multiple):
    if multiple:
        empty_value = []
    else:
        empty_value = ""
    if kind == "similarity":
        bad_value = 0.0
    elif kind == "distance":
        bad_value = 1.0
    else:
        raise ValueError("invalid metric_func_kind")
    if not vectorized:
        return lambda x, y: func(x, y) if (x != empty_value) & (y != empty_value) else bad_value

    vec_check_empty = np.vectorize(lambda x, y: (x == empty_value) |  (y == empty_value))
    def empty_no_info(x, y):
        scores = func(x, y)
        scores[vec_check_empty(x, y)] = bad_value
        return scores
    return empty_no_info


def build_monge_elkan(func, kind, vectorized, empty_no_info):
    if kind == "similarity":
        agg = max
        agg_default = 0.0
        empty_value_default = agg_default if empty_no_info else 1.0
    elif kind == "distance":
        agg = min
        agg_default = 1.0
        empty_value_default = agg_default if empty_no_info else 0.0
    else:
        raise ValueError("invalid metric_func_kind")

    def monge(x, y):
        if x:
            return sum((agg((func(xe, ye) for ye in y), default=agg_default) for xe in x)) / max(len(x), 1)
        elif y:
            return agg_default
        else:
            return empty_value_default
    if vectorized:
        return np.vectorize(monge)
    return monge


def build_symmetric_function(func):
    return lambda x, y: (func(x, y) + func(y, x)) / 2

class StringMetric():
    def __init__(self, kind, base, vec_base, normalized, vec_normalized):
        self.kind = kind
        self.base = base
        self.vec_base = vec_base
        self.normalized = normalized
        self.vec_normalized = vec_normalized

    def build(self, vectorized=False, normalized=False, empty_no_info=False, multiple=False, make_symmetric=False):
        if normalized:
            simple_func = self.normalized
            vec_func = self.vec_normalized
        else:
            simple_func = self.base
            vec_func = self.vec_base

        if multiple:
            func = build_monge_elkan(simple_func, self.kind, vectorized, empty_no_info)
        else:
            func = vec_func if vectorized else simple_func

        if make_symmetric:
            func = build_symmetric_function(func)

        if empty_no_info:
            func = build_empty_no_info(func, self.kind, vectorized, multiple)

        return func

class Exact(StringMetric):
    def __init__(self):
        kind = "similarity"
        base = lambda x, y: float(x == y)
        vec_base = np.vectorize(base)
        normalized = base
        vec_normalized = vec_base
        super().__init__(kind, base, vec_base, normalized, vec_normalized)

class Levenshtein(StringMetric):
    def __init__(self):
        kind = "distance"
        base = jellyfish.levenshtein_distance
        vec_base = np.vectorize(base)
        normalized = lambda x, y: base(x, y) / max(max(len(x), len(y)), 1)
        vec_normalized = lambda x, y: vec_base(x, y) / np.maximum(np.maximum(vec_len(x), vec_len(y)), 1)
        super().__init__(kind, base, vec_base, normalized, vec_normalized)


class DamerauLevenshtein(StringMetric):
    def __init__(self):
        kind = "distance"
        base = jellyfish.damerau_levenshtein_distance
        vec_base = np.vectorize(base)
        normalized = lambda x, y: base(x, y) / max(max(len(x), len(y)), 1)
        vec_normalized = lambda x, y: vec_base(x, y) / np.maximum(np.maximum(vec_len(x), vec_len(y)), 1)
        super().__init__(kind, base, vec_base, normalized, vec_normalized)

class JaroWinkler(StringMetric):
    def __init__(self):
        kind = "similarity"
        base = Levenshtein_modul.jaro_winkler
        vec_base = np.vectorize(base)
        normalized = base
        vec_normalized = vec_base
        super().__init__(kind, base, vec_base, normalized, vec_normalized)


def _haversine_distance(lat_lng1, lat_lng2):
    lat1 = np.radians(lat_lng1.iloc[:, 0])
    lng1 = np.radians(lat_lng1.iloc[:, 1])
    lat2 = np.radians(lat_lng2.iloc[:, 0])
    lng2 = np.radians(lat_lng2.iloc[:, 1])
    d = (
        np.sin((lat2 - lat1) * 0.5) ** 2
        + np.cos(lat1) * np.cos(lat2) * np.sin((lng2 - lng1) * 0.5) ** 2
    )
    return 2 * np.arcsin(np.sqrt(d)) / np.pi


def _date(x, y):
    x = pd.Series(x)
    y = pd.Series(y)
    x_time = x.dt.to_pydatetime()
    y_time = y.dt.to_pydatetime()
    score = 1.0 - np.abs(pd.Series(map(lambda t: t.days, x_time - y_time))) / 30
    score = score.fillna(0)
    score = np.maximum(score, 0)
    # swaped day and month
    score[
        (x.dt.day != y.dt.day)
        & (x.dt.day == y.dt.month)
        & (x.dt.month == y.dt.day)
        & (x.dt.year == y.dt.year)
        & (score < 0.5)
    ] = 0.5
    # only diffence in month
    score[
        (x.dt.day == y.dt.day)
        & (x.dt.year == y.dt.year)
        & (x.dt.month != y.dt.month)
        & (score < 0.75)
    ] = 0.75
    # ingnore default values
    score[(x == np.datetime64("1700-01-01")) | (y == np.datetime64("1700-01-01"))] = 0.0
    return score

def _date_alternative(x, y):
    x = pd.Series(x)
    y = pd.Series(y)
    x_time = x.dt.to_pydatetime()
    y_time = y.dt.to_pydatetime()
    score = 1.0 - np.abs(pd.Series(map(lambda t: t.days, x_time - y_time))) / 30
    score = score.fillna(0)
    score = np.maximum(score, 0)
    # swaped day and month
    score[
        (x.dt.day != y.dt.day)
        & (x.dt.day == y.dt.month)
        & (x.dt.month == y.dt.day)
        & (x.dt.year == y.dt.year)
        & (score < 0.5)
    ] = 0.5
    # only diffence in month
    score[
        (x.dt.day == y.dt.day)
        & (x.dt.year == y.dt.year)
        & (x.dt.month != y.dt.month)
        & (score < 0.75)
    ] = 0.75
    all_same_except_year = (x.dt.day == y.dt.day) & (x.dt.month == y.dt.month) & (x.dt.year != y.dt.year)
    score[all_same_except_year] = 1/((x[all_same_except_year].dt.year - y[all_same_except_year].dt.year).abs() + 1)
    # ingnore default values
    score[(x == np.datetime64("1700-01-01")) | (y == np.datetime64("1700-01-01"))] = 0.0
    return score

def _freq_average(x, y):
    return (x + y) / 2

def _age_difference(x, y):
    x = pd.Series(x)
    y = pd.Series(y)
    x_time = x.dt.to_pydatetime()
    y_time = y.dt.to_pydatetime()
    days = pd.Series(map(lambda x: x.days, (x_time - y_time))).abs()
    return (days // 365.25).replace({inf: np.iinfo(np.int32).max}).fillna(np.iinfo(np.int32).max).astype("int32")

exact = Exact()
levenshtein = Levenshtein()
damerau_levenshtein = DamerauLevenshtein()
jaro_winkler = JaroWinkler()

metrics = {
    "age_diff": _age_difference,
    "exact": exact.build(vectorized=True),
    "exact_empty_no_info": exact.build(vectorized=True, empty_no_info=True),
    "edit": levenshtein.build(vectorized=True, normalized=True),
    "edit_empty_no_info": levenshtein.build(vectorized=True, normalized=True, empty_no_info=True),
    "damerau_levenshtein": damerau_levenshtein.build(vectorized=True, normalized=True),
    "damerau_levenshtein_empty_no_info": damerau_levenshtein.build(vectorized=True, normalized=True, empty_no_info=True),
    "jaro_winkler": jaro_winkler.build(vectorized=True),
    "jaro_winkler_empty_no_info": jaro_winkler.build(vectorized=True, empty_no_info=True),
    "monge_elkan[exact]":  exact.build(vectorized=True, multiple=True),
    "monge_elkan_empty_no_info[exact]": exact.build(vectorized=True, empty_no_info=True, multiple=True),
    "monge_elkan[edit]": levenshtein.build(vectorized=True, normalized=True, multiple=True),
    "monge_elkan_empty_no_info[edit]": levenshtein.build(vectorized=True, normalized=True, empty_no_info=True, multiple=True),
    "monge_elkan[damerau_levenshtein]": damerau_levenshtein.build(vectorized=True, normalized=True, multiple=True),
    "monge_elkan_empty_no_info[damerau_levenshtein]": damerau_levenshtein.build(vectorized=True, normalized=True, empty_no_info=True, multiple=True),
    "monge_elkan[jaro_winkler]": jaro_winkler.build(vectorized=True, multiple=True),
    "monge_elkan_empty_no_info[jaro_winkler]": jaro_winkler.build(vectorized=True, multiple=True, empty_no_info=True),
    "symmetric_monge_elkan[exact]":  exact.build(vectorized=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan_empty_no_info[exact]": exact.build(vectorized=True, empty_no_info=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan[edit]": levenshtein.build(vectorized=True, normalized=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan_empty_no_info[edit]": levenshtein.build(vectorized=True, normalized=True, empty_no_info=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan[damerau_levenshtein]": damerau_levenshtein.build(vectorized=True, normalized=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan_empty_no_info[damerau_levenshtein]": damerau_levenshtein.build(vectorized=True, normalized=True, empty_no_info=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan[jaro_winkler]": jaro_winkler.build(vectorized=True, multiple=True, make_symmetric=True),
    "symmetric_monge_elkan_empty_no_info[jaro_winkler]": jaro_winkler.build(vectorized=True, empty_no_info=True, multiple=True, make_symmetric=True),
    "date": _date,
    "date_alternative": _date_alternative,
    "haversine": _haversine_distance,
    "freq_average": _freq_average,
}


def apply_metric(metric, *args):
    return metrics[metric](*args)


def generate_name(col1, col2, metric):
    name = []
    if isinstance(col1, list):
        name.append(":".join(col1))
    else:
        name.append(col1)
    if isinstance(col2, list):
        name.append(":".join(col2))
    else:
        name.append(col2)
    name.append(metric)
    return "-".join(name)


def convert_to_metric_config(data1, data2, metric_config):
    data1 = data1.copy()
    data2 = data2.copy()
    for col1, col2_metric in metric_config.items():
        for col2, metrics in col2_metric.items():
            for metric in metrics:
                if "date" in metric:
                    data1[col1] = data1[col1].astype("datetime64")
                    data2[col2] = data2[col2].astype("datetime64")
                if metric == "haversine":
                    data1[col1[0]] = data1[col1[0]].astype("float")
                    data1[col1[1]] = data1[col1[1]].astype("float")
                    data2[col2[0]] = data2[col2[0]].astype("float")
                    data2[col2[1]] = data2[col2[1]].astype("float")
    return data1, data2


def extract_data(multiindex, data1, data2):
    index1 = multiindex.get_level_values(0)
    index2 = multiindex.get_level_values(1)
    data1_extract = data1.reindex(index1).reset_index(drop=True)
    data2_extract = data2.reindex(index2).reset_index(drop=True)
    return data1_extract, data2_extract


def apply_metrics(multiindex, data1, data2, config):
    metric_config = config.metric_config
    data1, data2 = convert_to_metric_config(data1, data2, metric_config)
    data1_extract, data2_extract = extract_data(multiindex, data1, data2)
    name_map = []
    series = []

    for col1, col2_metric in metric_config.items():
        if isinstance(col1, tuple):
            col1 = list(col1)
        for col2, metrics in col2_metric.items():
            name_map.append([col1, col2])
            if isinstance(col2, tuple):
                col2 = list(col2)
            for metric in metrics:
                compare_col1 = data1_extract[col1]
                compare_col2 = data2_extract[col2]
                computed = apply_metric(metric, compare_col1, compare_col2)
                computed = pd.Series(computed, name=generate_name(col1, col2, metric))
                series.append(computed.reset_index(drop=True))

    for col in config._single_feature_left:
        series.append(pd.Series(data1[col].loc[multiindex.get_level_values(0)], name=col + "_left").reset_index(drop=True))

    for col in config._single_feature_right:
        series.append(pd.Series(data2[col].loc[multiindex.get_level_values(1)], name=col + "_right").reset_index(drop=True))
    return pd.concat(series, axis=1).set_index(multiindex, drop=True)
