import logging
import unittest

import pandas as pd

from app.config import PredicateConfig
from app.index._learn._learn import CandidateConjunctionGenerator
from app.index._learn._predicates import Predicates


class TestCandidateConjunctionGenerator(unittest.TestCase):
    def _check_predicates(self, predicates, test_values):
        for predicate in predicates:
            test_value = test_values[predicate.to_tuple()]
            self.assertTrue((predicate.matches == test_value["matches"]).all())
            self.assertEqual(predicate.matches_count, test_value["matches_count"])
            self.assertEqual(
                predicate.non_matches_count, test_value["non_matches_count"]
            )
            self.assertAlmostEqual(
                predicate.reduction_ratio(), test_value["reduction_ratio"], places=3
            )
            self.assertAlmostEqual(predicate.score(), test_value["score"], places=3)

    def test_candidates(self):
        data1 = pd.DataFrame(
            {
                "name": ["a", "c", "c", "", "d", "", "b", "a"],
                "date": [
                    "01/01/2001",
                    "02/01/2002",
                    "",
                    "10/10/2000",
                    "",
                    "02/04/2006",
                    "01/02/2010",
                    "01/01/2000",
                ],
                "list": ["1 2", "", "1", "1", "5 4", "", "1", "1 1"],
            }
        )
        data2 = pd.DataFrame(
            {
                "name": ["a", "b", "c", "", "a", "", "b", "d"],
                "date": [
                    "01/01/2000",
                    "05/11/2002",
                    "",
                    "10/10/2000",
                    "",
                    "02/04/2006",
                    "01/01/2010",
                    "01/01/2000",
                ],
                "list": ["2", "5", "5", "1 4", "", "", "3", "3 4"],
            }
        )
        data1["list"] = data1["list"].str.split(" ")
        data2["list"] = data2["list"].str.split(" ")

        predicate_config = PredicateConfig()
        predicate_config.add("name", "name", "exact")
        predicate_config.add("name", "name", "doublemetaphone")
        predicate_config.add("date", "date", "exact")
        predicate_config.add("date", "date", "date_3")
        predicate_config.add("date", "date", "year")
        predicate_config.add("list", "list", "exact")

        test_values = {
            (("date", "date", "date_3"),): {
                "matches": [3, 5, 6, 7],
                "matches_count": 4,
                "non_matches_count": 1,
                "reduction_ratio": 0.921875,
                "score": 2.0,
            },
            (("date", "date", "date_3"), ("date", "date", "year")): {
                "matches": [3, 5, 6, 7],
                "matches_count": 4,
                "non_matches_count": 1,
                "reduction_ratio": 0.921875,
                "score": 2.0,
            },
            (
                ("date", "date", "date_3"),
                ("date", "date", "exact"),
                ("date", "date", "year"),
            ): {
                "matches": [3, 5, 7],
                "matches_count": 3,
                "non_matches_count": 1,
                "reduction_ratio": 0.9375,
                "score": 1.5,
            },
            (("date", "date", "exact"),): {
                "matches": [3, 5, 7],
                "matches_count": 3,
                "non_matches_count": 1,
                "reduction_ratio": 0.9375,
                "score": 1.5,
            },
            (("date", "date", "date_3"), ("date", "date", "exact")): {
                "matches": [3, 5, 7],
                "matches_count": 3,
                "non_matches_count": 1,
                "reduction_ratio": 0.9375,
                "score": 1.5,
            },
            (("date", "date", "year"),): {
                "matches": [1, 3, 5, 6, 7],
                "matches_count": 5,
                "non_matches_count": 4,
                "reduction_ratio": 0.859375,
                "score": 1.0,
            },
            (("list", "list", "exact"),): {
                "matches": [0, 3],
                "matches_count": 2,
                "non_matches_count": 8,
                "reduction_ratio": 0.84375,
                "score": 0.2222222222222222,
            },
            (("date", "date", "date_3"), ("list", "list", "exact")): {
                "matches": [3],
                "matches_count": 1,
                "non_matches_count": 0,
                "reduction_ratio": 0.984375,
                "score": 1.0,
            },
            (
                ("date", "date", "date_3"),
                ("date", "date", "exact"),
                ("list", "list", "exact"),
            ): {
                "matches": [3],
                "matches_count": 1,
                "non_matches_count": 0,
                "reduction_ratio": 0.984375,
                "score": 1.0,
            },
            (("name", "name", "doublemetaphone"),): {
                "matches": [0, 2, 6],
                "matches_count": 3,
                "non_matches_count": 6,
                "reduction_ratio": 0.859375,
                "score": 0.42857142857142855,
            },
            (("date", "date", "date_3"), ("name", "name", "doublemetaphone")): {
                "matches": [6],
                "matches_count": 1,
                "non_matches_count": 1,
                "reduction_ratio": 0.96875,
                "score": 0.5,
            },
            (
                ("date", "date", "date_3"),
                ("date", "date", "year"),
                ("name", "name", "doublemetaphone"),
            ): {
                "matches": [6],
                "matches_count": 1,
                "non_matches_count": 1,
                "reduction_ratio": 0.96875,
                "score": 0.5,
            },
            (("name", "name", "exact"),): {
                "matches": [0, 2, 6],
                "matches_count": 3,
                "non_matches_count": 6,
                "reduction_ratio": 0.859375,
                "score": 0.42857142857142855,
            },
            (("date", "date", "date_3"), ("name", "name", "exact")): {
                "matches": [6],
                "matches_count": 1,
                "non_matches_count": 1,
                "reduction_ratio": 0.96875,
                "score": 0.5,
            },
            (
                ("date", "date", "date_3"),
                ("date", "date", "year"),
                ("name", "name", "exact"),
            ): {
                "matches": [6],
                "matches_count": 1,
                "non_matches_count": 1,
                "reduction_ratio": 0.96875,
                "score": 0.5,
            },
        }
        logging.disable(level=logging.CRITICAL)

        predicates = Predicates.from_data(data1, data2, predicate_config)
        self._check_predicates(predicates, test_values)

        candidates = CandidateConjunctionGenerator(predicates, 3, 0.5)
        self._check_predicates(candidates, test_values)
        self._check_predicates(predicates, test_values)
