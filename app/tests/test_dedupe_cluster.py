import unittest

import pandas as pd

from app.cluster._cluster import CsSnCluster


class TestDedupeCluster(unittest.TestCase):
    def test_cluster(self):
        pairs = [
            (1, 2, 0.05),
            (1, 3, 0.15),
            (1, 4, 0.175),
            (1, 5, 0.15),
            (1, 6, 0.2),
            (2, 3, 0.1),
            (2, 4, 0.15),
            (2, 5, 0.1),
            (2, 6, 0.15),
            (3, 4, 0.05),
            (3, 5, 0.15),
            (3, 6, 0.1),
            (3, 7, 0.2),
            (4, 5, 0.2),
            (4, 6, 0.15),
            (5, 6, 0.1),
            (5, 7, 0.21),
            (6, 7, 0.21),
            (6, 8, 0.21),
            (6, 12, 0.4),
            (7, 8, 0.05),
            (7, 9, 0.05),
            (7, 10, 0.14),
            (7, 11, 0.25),
            (7, 12, 0.25),
            (8, 9, 0.025),
            (8, 10, 0.05),
            (8, 11, 0.1),
            (9, 12, 0.2),
            (9, 10, 0.035),
            (9, 11, 0.125),
            (10, 11, 0.15),
            (11, 12, 0.1),
        ]
        distances = pd.DataFrame(pairs, columns=["left", "right", "distance"])
        solutions_1_2 = []
        solutions_1_4 = []
        solutions_1_6 = []
        solutions_2_2 = [{1, 2}, {3, 4}]
        solutions_2_4 = [{1, 2}, {3, 4}]
        solutions_2_6 = [{1, 2}, {3, 4}]
        solutions_3_2 = [{1, 2}, {3, 4}, {8, 9}]
        solutions_3_4 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_3_6 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_4_2 = [{1, 2}, {3, 4}, {8, 9}]
        solutions_4_4 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_4_6 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_5_2 = [{1, 2}, {3, 4}, {8, 9}]
        solutions_5_4 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_5_6 = [{1, 2, 3, 4, 5, 6}, {7, 8, 9, 10}]
        solutions_6_2 = [{1, 2}, {3, 4}, {8, 9}]
        solutions_6_4 = [{1, 2}, {3, 4}, {7, 8, 9, 10}]
        solutions_6_6 = [{1, 2, 3, 4, 5, 6}, {7, 8, 9, 10}]

        for clusters, solution_clusters in [
            (list(CsSnCluster(distances, 1, 2)), solutions_1_2),
            (list(CsSnCluster(distances, 1, 4)), solutions_1_4),
            (list(CsSnCluster(distances, 1, 6)), solutions_1_6),
            (list(CsSnCluster(distances, 2, 2)), solutions_2_2),
            (list(CsSnCluster(distances, 2, 4)), solutions_2_4),
            (list(CsSnCluster(distances, 2, 6)), solutions_2_6),
            (list(CsSnCluster(distances, 3, 2)), solutions_3_2),
            (list(CsSnCluster(distances, 3, 4)), solutions_3_4),
            (list(CsSnCluster(distances, 3, 6)), solutions_3_6),
            (list(CsSnCluster(distances, 4, 2)), solutions_4_2),
            (list(CsSnCluster(distances, 4, 4)), solutions_4_4),
            (list(CsSnCluster(distances, 4, 6)), solutions_4_6),
            (list(CsSnCluster(distances, 5, 2)), solutions_5_2),
            (list(CsSnCluster(distances, 5, 4)), solutions_5_4),
            (list(CsSnCluster(distances, 5, 6)), solutions_5_6),
            (list(CsSnCluster(distances, 6, 2)), solutions_6_2),
            (list(CsSnCluster(distances, 6, 4)), solutions_6_4),
            (list(CsSnCluster(distances, 6, 6)), solutions_6_6),
        ]:
            left_same_as_right = all(
                cluster in solution_clusters for cluster in clusters
            )
            right_same_as_left = all(
                cluster in clusters for cluster in solution_clusters
            )
            self.assertTrue(left_same_as_right and right_same_as_left)
