import unittest

from app.data import gen_path

class TestGenPath(unittest.TestCase):
    def test_gen_path(self):
        date1, date2 = "20200101", "20200102"
        link_path = gen_path(date1, date2, filename="links.csv")
        out_path = gen_path(date1, filename="links.csv")
        train1_path, train2_path = gen_path(date1, date2, train=True, suffix="_train.csv")
        model_path = gen_path(date1, date2, train=True, filename="model.pkl")
        extract_path = gen_path("20200101", raw=True)
        hash_path = gen_path("20200101", raw=True, filename="hash.json")
        self.assertTrue(str(link_path).endswith("combine/20200101_20200102/links.csv"))
        self.assertTrue(str(out_path).endswith("data/20200101/links.csv"))
        self.assertTrue(str(train1_path).endswith("train/20200101_20200102/20200101_train.csv"))
        self.assertTrue(str(train2_path).endswith("train/20200101_20200102/20200102_train.csv"))
        self.assertTrue(str(model_path).endswith("train/20200101_20200102/model.pkl"))
        self.assertTrue(str(extract_path).endswith("data/20200101/raw"))
        self.assertTrue(str(hash_path).endswith("data/20200101/raw/hash.json"))
