import unittest
import pandas as pd

from app.compare import apply_metric

class TestMetrics(unittest.TestCase):
    def assertAlmostEqual_List(self, l1, l2, places=7):
        for x, y in zip(l1, l2):
            self.assertAlmostEqual(x, y, places=places)

    exact_examples = [
        ("a", "b", 0.0),
        ("b", "a", 0.0),
        ("a", "a", 1.0),
        ("hello", "world", 0.0),
        ("world", "hello", 0.0),
        ("world", "world", 1.0),
        ("", "", 1.0)
    ]

    exact_empty_no_info_examples = [
        ("a", "b", 0.0),
        ("b", "a", 0.0),
        ("a", "a", 1.0),
        ("hello", "world", 0.0),
        ("world", "hello", 0.0),
        ("world", "world", 1.0),
        ("", "", 0.0)
    ]

    string_examples = [
        ("peter", "pedro", 0.6, 0.6, 0.78666667),
        ("christen", "christian", 0.22222222, 0.22222222, 0.9537037),
        ("mxa", "max", 0.66666667, 0.33333333, 0.9),
        ("julio", "de julio", 0.375, 0.375, 0.875),
        ("", "de julio", 1.0, 1.0, 0.0),
        ("de julio", "", 1.0, 1.0, 0.0),
        ("", "", 0.0, 0.0, 1.0)
    ]

    string_empty_no_info_examples = [
        ("peter", "pedro", 0.6, 0.6, 0.78666667),
        ("christen", "christian", 0.22222222, 0.22222222, 0.9537037),
        ("mxa", "max", 0.66666667, 0.33333333, 0.9),
        ("julio", "de julio", 0.375, 0.375, 0.875),
        ("", "de julio", 1.0, 1.0, 0.0),
        ("de julio", "", 1.0, 1.0, 0.0),
        ("", "", 1.0, 1.0, 0.0)
    ]

    monge_examples = [
        (["hello", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "bye"], ["world", "hello"], 0.5, 0.5, 0.5, 0.7555555555555555),
        (["christian", "peter"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["peter", "christian"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["mxa", "de julio"], ["julio", "max"], 0.0, 0.52083333, 0.35416667, 0.8875),
        ([], ["julio", "max"], 0.0, 1.0, 1.0, 0.0),
        (["julio", "max"], [], 0.0, 1.0, 1.0, 0.0),
        ([], [], 1.0, 0.0, 0.0, 1.0)
    ]

    monge_empty_no_info_examples = [
        (["hello", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "bye"], ["world", "hello"], 0.5, 0.5, 0.5, 0.7555555555555555),
        (["christian", "peter"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["peter", "christian"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["mxa", "de julio"], ["julio", "max"], 0.0, 0.52083333, 0.35416667, 0.8875),
        ([], ["julio", "max"], 0.0, 1.0, 1.0, 0.0),
        (["julio", "max"], [], 0.0, 1.0, 1.0, 0.0),
        ([], [], 0.0, 1.0, 1.0, 0.0)
    ]


    symmetric_monge_examples = [
        (["hello", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "world"], ["world", "hello"], 0.75, 0.2, 0.2, 0.85833333),
        (["world", "bye"], ["world", "hello"], 0.5, 0.45, 0.45, 0.7555555555555555),
        (["christian", "peter"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["peter", "christian"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["mxa", "de julio"], ["julio", "max"], 0.0, 0.52083333, 0.35416667, 0.8875),
        ([], ["julio", "max"], 0.0, 1.0, 1.0, 0.0),
        (["julio", "max"], [], 0.0, 1.0, 1.0, 0.0),
        ([], [], 1.0, 0.0, 0.0, 1.0)
    ]

    symmetric_monge_empty_no_info_examples = [
        (["hello", "world"], ["world", "hello"], 1.0, 0.0, 0.0, 1.0),
        (["world", "world"], ["world", "hello"], 0.75, 0.2, 0.2, 0.85833333),
        (["world", "bye"], ["world", "hello"], 0.5, 0.45, 0.45, 0.7555555555555555),
        (["christian", "peter"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["peter", "christian"], ["christen", "pedro"], 0.0, 0.41111111, 0.41111111, 0.87018519),
        (["mxa", "de julio"], ["julio", "max"], 0.0, 0.52083333, 0.35416667, 0.8875),
        ([], ["julio", "max"], 0.0, 1.0, 1.0, 0.0),
        (["julio", "max"], [], 0.0, 1.0, 1.0, 0.0),
        ([], [], 0.0, 1.0, 1.0, 0.0)
    ]

    date_examples = [
        ("01/01/1990", "01/01/1990", 1.0),
        ("01/01/1990", "12/31/1989", 0.9666666666666667),
        ("12/31/1989", "01/01/1990", 0.9666666666666667),
        ("05/01/1990", "01/01/1990", 0.75),
        ("01/01/1990", "01/10/1990", 0.7),
        ("10/01/1990", "10/01/1990", 1.0),
        ("01/01/1990", "01/09/1990", 0.7333333333333334),
        ("01/01/1990", "01/03/1990", 0.9333333333333333),
        ("01/01/1990", "03/01/1990", 0.75)
    ]

    distance_examples = [
        ([28.018605, -81.72269], [27.3194221, -80.30971], 0.007963656643754417),
        ([27.3194221, -80.30971], [28.018605, -81.72269], 0.007963656643754417),
        ([30.934719, -87.508789], [30.5986, -87.069182], 0.002809041931013215),
        ([29.0266, -81.3349], [30.934719, -87.508789], 0.0315385022798907),
        ([30.934719, -87.508789], [30.934719, -87.508789], 0.0)
    ]

    def test_exact(self):
        exact1, exact2, exact = zip(*self.exact_examples)
        self.assertAlmostEqual_List(exact, apply_metric("exact", exact1, exact2))

    def test_exact_no_info(self):
        exact1, exact2, exact = zip(*self.exact_empty_no_info_examples)
        self.assertAlmostEqual_List(exact, apply_metric("exact_empty_no_info", exact1, exact2))

    def test_edit_distance(self):
        string1, string2, edit_distance, _, _ = zip(*self.string_examples)
        self.assertAlmostEqual_List(edit_distance, apply_metric("edit", string1, string2))

    def test_damerau_levenshtein(self):
        string1, string2, _, damerau_levenshtein, _ = zip(*self.string_examples)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("damerau_levenshtein", string1, string2))

    def test_jaro_winkler(self):
        string1, string2, _, _, jaro_winkler = zip(*self.string_examples)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("jaro_winkler", string1, string2))

    def test_edit_distance_empty_no_info(self):
        string1, string2, edit_distance, _, _ = zip(*self.string_empty_no_info_examples)
        self.assertAlmostEqual_List(edit_distance, apply_metric("edit_empty_no_info", string1, string2))

    def test_damerau_levenshtein_empty_no_info(self):
        string1, string2, _, damerau_levenshtein, _ = zip(*self.string_empty_no_info_examples)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("damerau_levenshtein_empty_no_info", string1, string2))

    def test_jaro_winkler_empty_no_info(self):
        string1, string2, _, _, jaro_winkler = zip(*self.string_empty_no_info_examples)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("jaro_winkler_empty_no_info", string1, string2))

    def test_me_exact(self):
        list1, list2, exact, _, _, _ = zip(*self.monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(exact, apply_metric("monge_elkan[exact]", list1, list2))

    def test_me_edit_distance(self):
        list1, list2, _, edit_distance, _, _ = zip(*self.monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(edit_distance, apply_metric("monge_elkan[edit]", list1, list2))

    def test_me_damerau_levenshtein(self):
        list1, list2, _, _, damerau_levenshtein, _ = zip(*self.monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("monge_elkan[damerau_levenshtein]", list1, list2))

    def test_me_jaro_winkler(self):
        list1, list2, _, _, _, jaro_winkler = zip(*self.monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("monge_elkan[jaro_winkler]", list1, list2))

    def test_me_exact_empty_no_info(self):
        list1, list2, exact, _, _, _ = zip(*self.monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(exact, apply_metric("monge_elkan_empty_no_info[exact]", list1, list2))

    def test_me_edit_distance_empty_no_info(self):
        list1, list2, _, edit_distance, _, _ = zip(*self.monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(edit_distance, apply_metric("monge_elkan_empty_no_info[edit]", list1, list2))

    def test_me_damerau_levenshtein_empty_no_info(self):
        list1, list2, _, _, damerau_levenshtein, _ = zip(*self.monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("monge_elkan_empty_no_info[damerau_levenshtein]", list1, list2))

    def test_me_jaro_winkler_empty_no_info(self):
        list1, list2, _, _, _, jaro_winkler = zip(*self.monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("monge_elkan_empty_no_info[jaro_winkler]", list1, list2))


    def test_symmetric_me_exact(self):
        list1, list2, exact, _, _, _ = zip(*self.symmetric_monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(exact, apply_metric("symmetric_monge_elkan[exact]", list1, list2))

    def test_symmetric_me_edit_distance(self):
        list1, list2, _, edit_distance, _, _ = zip(*self.symmetric_monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(edit_distance, apply_metric("symmetric_monge_elkan[edit]", list1, list2))

    def test_symmetric_me_damerau_levenshtein(self):
        list1, list2, _, _, damerau_levenshtein, _ = zip(*self.symmetric_monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("symmetric_monge_elkan[damerau_levenshtein]", list1, list2))

    def test_symmetric_me_jaro_winkler(self):
        list1, list2, _, _, _, jaro_winkler = zip(*self.symmetric_monge_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("symmetric_monge_elkan[jaro_winkler]", list1, list2))

    def test_symmetric_me_exact_empty_no_info(self):
        list1, list2, exact, _, _, _ = zip(*self.symmetric_monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(exact, apply_metric("symmetric_monge_elkan_empty_no_info[exact]", list1, list2))

    def test_symmetric_me_edit_distance_empty_no_info(self):
        list1, list2, _, edit_distance, _, _ = zip(*self.symmetric_monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(edit_distance, apply_metric("symmetric_monge_elkan_empty_no_info[edit]", list1, list2))

    def test_symmetric_me_damerau_levenshtein_empty_no_info(self):
        list1, list2, _, _, damerau_levenshtein, _ = zip(*self.symmetric_monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(damerau_levenshtein, apply_metric("symmetric_monge_elkan_empty_no_info[damerau_levenshtein]", list1, list2))

    def test_symmetric_me_jaro_winkler_empty_no_info(self):
        list1, list2, _, _, _, jaro_winkler = zip(*self.symmetric_monge_empty_no_info_examples)
        list1 = pd.Series(list1)
        list2 = pd.Series(list2)
        self.assertAlmostEqual_List(jaro_winkler, apply_metric("symmetric_monge_elkan_empty_no_info[jaro_winkler]", list1, list2))

    def test_date(self):
        date1, date2, date_distance = zip(*self.date_examples)
        date1 = pd.Series(date1, dtype="datetime64[ns]")
        date2 = pd.Series(date2, dtype="datetime64[ns]")
        self.assertAlmostEqual_List(date_distance, apply_metric("date", date1, date2))

    def test_haversine(self):
        lat_lng1, lat_lng2, haversine_distance = zip(*self.distance_examples)
        lat_lng1 = pd.DataFrame(lat_lng1)
        lat_lng2 = pd.DataFrame(lat_lng2)
        self.assertAlmostEqual_List(haversine_distance, apply_metric("haversine", lat_lng1, lat_lng2))
