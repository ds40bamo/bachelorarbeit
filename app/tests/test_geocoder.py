import unittest

import numpy as np
import pandas as pd

from app.preprocessing._parse._address._geocoder import Geocoder


class TestGeocoder(unittest.TestCase):
    def test_empty(self):
        geocoder = Geocoder()
        test_frame = pd.DataFrame(
            {"county": [], "address": [], "city": [], "zipcode": [], "address_addition": []}
        )
        test_frame.index.name = "voter_id"
        result = geocoder.geocode(test_frame)
        self.assertEqual(result.shape, (0, 7))

    def test_clean(self):
        geocoder = Geocoder()
        rows = [
            ["", "32720 forest ridge rd", "deland", "32720", ""],
            ["", "2709 s dellwood dr", "eustis", "32726", ""],
            ["", "14755 mockingbird ln w", "clearwater", "33760", ""],
            ["", "731 lindenwood cir e", "ormond beach", "32174", ""],
            ["", "2732 village pine ter", "orlando", "32833", ""],
        ]
        county, address, city, zipcode, address_addition = zip(*rows)
        test_frame = pd.DataFrame(
            {
                "county": county,
                "address": address,
                "city": city,
                "zipcode": zipcode,
                "address_addition": address_addition,
            }
        )
        test_frame.index.name = "voter_id"
        result = geocoder.geocode(test_frame)
        self.assertEqual(result.shape, (5, 7))
        self.assertAlmostEqual(result.iloc[0].longitude, -81.407909, places=3)
        self.assertAlmostEqual(result.iloc[0].latitude, 28.958445, places=3)
        self.assertAlmostEqual(result.iloc[1].longitude, -81.652661, places=3)
        self.assertAlmostEqual(result.iloc[1].latitude, 28.84839, places=3)
        self.assertAlmostEqual(result.iloc[2].longitude, -82.72677, places=3)
        self.assertAlmostEqual(result.iloc[2].latitude, 27.906819, places=3)
        self.assertAlmostEqual(result.iloc[3].longitude, -81.069809, places=3)
        self.assertAlmostEqual(result.iloc[3].latitude, 29.304007, places=3)
        self.assertAlmostEqual(result.iloc[4].longitude, -81.0852876, places=3)
        self.assertAlmostEqual(result.iloc[4].latitude, 28.5074803, places=3)

    def test_dirty(self):
        geocoder = Geocoder()
        rows = [
            ["", "130 avenue c se", "winter haven", "88330", ""],
            ["", "163 silver oak dr", "orlando", "32800", ""],
            ["", "7491 pine forest rd", "", "32568", ""],
            ["", "7491 pine forest rd", "", "34269", ""],
            ["", "", "", "88330", "32720"],
            ["", "", "", "", "2720 327200"],
            ["", "", "", "", "88330"],
            ["", "", "", "", "32720"],
            ["", "", "", "", "apt 272"],
            ["", "", "", "", ""],
        ]
        county, address, city, zipcode, address_addition = zip(*rows)
        test_frame = pd.DataFrame(
            {
                "county": county,
                "address": address,
                "city": city,
                "zipcode": zipcode,
                "address_addition": address_addition,
            }
        )
        test_frame.index.name = "voter_id"
        result = geocoder.geocode(test_frame)
        self.assertEqual(result.shape, (10, 7))
        self.assertAlmostEqual(result.iloc[0].longitude, -81.72269, places=3)
        self.assertAlmostEqual(result.iloc[0].latitude, 28.018605, places=3)
        self.assertAlmostEqual(result.iloc[1].longitude, -80.30971, places=3)
        self.assertAlmostEqual(result.iloc[1].latitude, 27.3194221, places=3)
        self.assertAlmostEqual(result.iloc[2].longitude, -87.508789, places=3)
        self.assertAlmostEqual(result.iloc[2].latitude, 30.934719, places=3)
        self.assertAlmostEqual(result.iloc[3].longitude, -87.069182, places=3)
        self.assertAlmostEqual(result.iloc[3].latitude, 30.5986, places=3)
        self.assertAlmostEqual(result.iloc[4].longitude, -81.3349, places=3)
        self.assertAlmostEqual(result.iloc[4].latitude, 29.0266, places=3)
        self.assertAlmostEqual(result.iloc[5].longitude, -81.4639835, places=3)
        self.assertAlmostEqual(result.iloc[5].latitude, 27.7567667, places=3)
        self.assertAlmostEqual(result.iloc[6].longitude, -81.4639835, places=3)
        self.assertAlmostEqual(result.iloc[6].latitude, 27.7567667, places=3)
        self.assertAlmostEqual(result.iloc[7].longitude, -81.3349, places=3)
        self.assertAlmostEqual(result.iloc[7].latitude, 29.0266, places=3)
        self.assertAlmostEqual(result.iloc[8].longitude, -81.4639835, places=3)
        self.assertAlmostEqual(result.iloc[8].latitude, 27.7567667, places=3)
        self.assertAlmostEqual(result.iloc[9].longitude, -81.4639835, places=3)
        self.assertAlmostEqual(result.iloc[9].latitude, 27.7567667, places=3)
