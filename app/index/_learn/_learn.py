import logging
from itertools import chain

from ._candidates import BlockingKey, KeyCandidates
from ._predicates import (ConjunctionPredicate, ConjunctionPredicateCache,
                          Predicates)


class CandidateConjunctionGenerator(object):
    def __init__(self, predicates, k, min_reduction_ratio):
        if not isinstance(predicates, Predicates):
            raise ValueError("predicates needs to be instance of Predicates")
        self.predicates = predicates
        self.k = k
        self.min_reduction_ratio = min_reduction_ratio
        self.total_candidates = self._estimate_candidates()
        self.cache = ConjunctionPredicateCache()
        self.logger = logging.getLogger("conjunction generator")
        self.logger.setLevel(logging.INFO)

    def __iter__(self):
        yield from self._fold_predicates()

    def _estimate_candidates(self):
        number_predicates = len(self.predicates)
        number_first = map(lambda x: x.reduction_ratio(), self.predicates)
        number_first = filter(lambda x: x > self.min_reduction_ratio, number_first)
        number_first = len(list(number_first))
        number = 0
        for i in range(1, self.k):
            number += number_predicates * (number_predicates - i)
        return number

    def _fold_predicates(self):
        self.current_number = 0
        for candidate in self.predicates:
            yield candidate
            yield from self._fold_predicate(candidate)

    def _log(self, pre_predicat, append_predicat):
        self.current_number += 1
        key = tuple(sorted(set(pre_predicat.to_tuple() + append_predicat.to_tuple())))
        self.logger.info("%d~%d %s", self.current_number, self.total_candidates, key)

    def _fold_predicate(self, cur_predicate):
        if len(cur_predicate) >= self.k:
            return
        best_candidate = None
        best_score = None
        for candidate in self.predicates:
            if candidate in cur_predicate:
                continue
            if self.min_reduction_ratio is not None:
                if cur_predicate.reduction_ratio() < self.min_reduction_ratio:
                    if candidate.reduction_ratio() < self.min_reduction_ratio:
                        continue
            self._log(cur_predicate, candidate)
            key_or_merged, score = self.cache.get(cur_predicate, candidate)
            if score is None:
                key_or_merged = cur_predicate.merge(candidate)
                score = key_or_merged.score()
                self.cache.add(key_or_merged)
            if best_score is None or score > best_score:
                best_candidate = key_or_merged
                best_score = score
        if best_candidate is not None:
            if not isinstance(best_candidate, ConjunctionPredicate):
                best_candidate = self.predicates.from_keys(best_candidate)
            yield best_candidate
            yield from self._fold_predicate(best_candidate)


def approxRBSetCover(matches, candidate_conjunctions, min_reduction_ratio):
    candidates = KeyCandidates(
        candidate_conjunctions, min_reduction_ratio=min_reduction_ratio
    )
    num_all_matches = len(matches)
    max_precission = candidates.possible_match_count / num_all_matches
    found_keys = {}
    blocking_key = BlockingKey(num_all_matches)
    best_candidate = candidates.best()
    while best_candidate.score != 0:
        candidates.remove_matches(best_candidate.matches)
        blocking_key.add(best_candidate)
        found_keys[blocking_key.precission()] = blocking_key.to_tuple()
        best_candidate = candidates.best()
    return found_keys


def approxDNF(predicates, match_ids, k, min_reduction_ratio):
    if k < 1:
        raise ValueError("k needs to be bigger than 1")
    if min_reduction_ratio > 1.0 or min_reduction_ratio < 0.0:
        raise ValueError("min_reduction_ratio not in range [0.0, 1.0]")
    candidate_conjunctions = CandidateConjunctionGenerator(
        predicates, k=k, min_reduction_ratio=min_reduction_ratio
    )
    return approxRBSetCover(
        match_ids, candidate_conjunctions, min_reduction_ratio=min_reduction_ratio
    )


def learn_blocking_key(
    date1,
    date2,
    predicate_config,
    k,
    min_reduction_ratio,
    maxminor,
):
    predicates, match_ids = Predicates.load(
        date1,
        date2,
        predicate_config,
        maxminor,
    )
    return approxDNF(predicates, match_ids, k, min_reduction_ratio)
