import logging
from collections import defaultdict
from functools import reduce

import pandas as pd


from .._block import BlockGenerator
from ._pairs import BlockedPairs



class ConjunctionPredicateCache(object):
    def __init__(self):
        self.logger = logging.getLogger("cache")
        self.logger.setLevel(logging.INFO)
        self.cache = dict()

    def add(self, conjunction):
        self.cache[conjunction.to_tuple()] = conjunction.score()

    def get(self, pre_predicate, append_predicate):
        key = tuple(sorted(set(pre_predicate.to_tuple() + append_predicate.to_tuple())))
        hit = self.cache.get(key)
        if hit is not None:
            self.logger.info("cache hit %s", key)
        return key, hit


class Predicate(object):
    def __init__(
        self,
        name_left,
        name_right,
        method,
        key_left,
        key_right,
        all_keys_left,
        all_keys_right,
    ):
        self.name_left = str(name_left)
        self.name_right = str(name_right)
        self.method = str(method)
        self.pairs = BlockedPairs(
            keys=(key_left, key_right), all_keys=(all_keys_left, all_keys_right)
        )

    def to_conjunction(self):
        return ConjunctionPredicate(
            [(self.name_left, self.name_right, self.method)], self.pairs
        )

    def __repr__(self):
        return (
            self.__class__.__name__
            + f"({self.name_left} - {self.name_right} - {self.method})"
        )


class ConjunctionPredicate(object):
    def __init__(self, conjunction, pairs):
        self.conjunction = set(conjunction)
        self.pairs = pairs

    def merge(self, other):
        if self is other:
            return self
        other = other.to_conjunction()
        pairs = self.pairs.merge(other.pairs)
        return ConjunctionPredicate(self.conjunction | other.conjunction, pairs)

    def reduction_ratio(self):
        return self.pairs.reduction_ratio()

    def score(self):
        return self.pairs.matches_count / (self.pairs.non_matches_count + 1)

    def to_pairs(self):
        return self.pairs.compile()

    def to_conjunction(self):
        return self

    def to_tuple(self):
        return tuple(sorted(self.conjunction))

    @property
    def matches(self):
        return self.pairs.matches

    @property
    def non_matches_count(self):
        return self.pairs.non_matches_count

    @property
    def matches_count(self):
        return self.pairs.matches_count

    def __len__(self):
        return len(self.conjunction)

    def __repr__(self):
        return (
            self.__class__.__name__
            + "("
            + " and ".join(
                map(lambda x: f"{{{x[0]} - {x[1]} - {x[2]}}}", self.conjunction)
            )
            + ")"
        )

    def __contains__(self, conjunction):
        return all(map(lambda p: p in self.to_tuple(), conjunction.to_tuple()))


class Predicates(object):
    def __init__(self, predicates):
        predicates = map(lambda p: p.to_conjunction(), predicates)
        self.predicates = {p.to_tuple(): p for p in predicates}

    def from_keys(self, candidate_keys):
        candidates = map(lambda key: self.predicates[(key,)], candidate_keys)
        candidates = set(candidates)
        candidates = sorted(candidates, key=lambda x: x.reduction_ratio(), reverse=True)
        return reduce(lambda x, y: x.merge(y), candidates)

    def pairs(self):
        all_pairs = set()
        for predicate in self.predicates.values():
            all_pairs.update(predicate.to_pairs())
        return all_pairs

    @staticmethod
    def load(
        date1,
        date2,
        predicate_config,
        maxminor=500000,
    ):
        data1, data2 = predicate_config.load_train(date1, date2, maxminor)
        match_ids = data1.index
        predicates = Predicates.from_data(data1, data2, predicate_config)
        for col in data1:
            del data1[col]
        del data1
        for col in data2:
            del data2[col]
        del data2
        return predicates, match_ids

    def __iter__(self):
        sorted_predicates = sorted(self.predicates.items(), key=lambda x: x[0])
        yield from map(lambda x: x[1], sorted_predicates)

    def __len__(self):
        return len(self.predicates.values())

    def __repr__(self):
        return (
            self.__class__.__name__
            + "("
            + ", ".join(map(repr, self.predicates.values()))
            + ")"
        )

    @staticmethod
    def from_data(data1, data2, predicate_config):
        predicate_config.raise_invalid(data1, data2)
        predicates = []
        for method, col1_col2 in predicate_config.reverse().items():
            for col1, key1, col2, key2 in BlockGenerator(
                method, data1, data2, col1_col2
            ):
                predicate = Predicate(
                    col1, col2, method, key1, key2, data1.index, data2.index
                )
                predicates.append(predicate)
        return Predicates(predicates)
