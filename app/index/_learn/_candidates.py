from itertools import chain


class BlockingKey(object):
    def __init__(self, num_all_matches):
        self.blocking_key = set()
        self.found_matches = set()
        self.num_all_matches = num_all_matches

    def add(self, key):
        self.blocking_key.add(key.name)
        self.found_matches.update(key.matches)

    def precission(self):
        return len(self.found_matches) / self.num_all_matches

    def to_tuple(self):
        return set(self.blocking_key)

    def __iter__(self):
        return iter(self.blocking_key)

    def __repr__(self):
        return self.__class__.__name__ + "(" + ", ".join(map(repr, iter(self))) + ")"


class Candidate(object):
    def __init__(self, name, matches, non_matches_count):
        self.name = name
        self.matches = set(matches)
        self.non_matches_count = non_matches_count
        self.score = self._score()
        self.is_complex = self._is_complex(name)

    @staticmethod
    def _is_complex(name):
        return any(map(lambda x: x[0] != x[1], name))

    def remove_matches(self, matches):
        self.matches.difference_update(matches)
        self.score = self._score()

    def _score(self):
        return len(self.matches) / (self.non_matches_count + 1)

    def copy(self):
        return Candidate(self.name, self.matches, self.non_matches_count)

    def __lt__(self, other):
        if self.score < other.score:
            return True
        elif self.score == other.score:
            return self.is_complex and not other.is_complex
        return False

    def __repr__(self):
        return (
            self.__class__.__name__
            + "("
            + ", ".join(
                map(
                    repr,
                    [self.name, len(self.matches), self.non_matches_count, self.score],
                )
            )
            + ")"
        )


class KeyCandidates(object):
    def __init__(self, candidate_conjunctions, min_reduction_ratio):
        candidates = filter(
            lambda x: x.reduction_ratio() < min_reduction_ratio, candidate_conjunctions
        )
        candidates = map(
            lambda x: Candidate(x.to_tuple(), x.matches, x.non_matches_count),
            candidate_conjunctions,
        )
        candidates = {cand.name : cand for cand in candidates}
        self.candidates = sorted(candidates.values(), reverse=True)
        possible_match_count = map(lambda c: c.matches, self.candidates)
        possible_match_count = chain.from_iterable(possible_match_count)
        possible_match_count = set(possible_match_count)
        self.possible_match_count = len(possible_match_count)

    def remove_matches(self, matches):
        for candidate in self.candidates:
            candidate.remove_matches(matches)
        self.candidates = sorted(self.candidates, reverse=True)

    def best(self):
        return self.candidates[0].copy()
