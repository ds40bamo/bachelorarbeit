from itertools import chain
import numpy as np
import sortednp as snp
import pandas as pd


class Pairs(object):
    def __init__(self, blocks=None, pairs=None):
        if blocks is not None:
            self._init_with_blocks(blocks)
        elif pairs is not None:
            self._init_with_pairs(pairs)
        else:
            raise ValueError("specify blocks or pairs")

    def _init_with_blocks(self, blocks):
        self.ids_left, self.ids_right = self._generate_pairs(blocks)
        self.matches = self._generate_matches(blocks)

    def _init_with_pairs(self, pairs):
        ids_left = []
        ids_right = []
        cache = {}
        try:
            pairs1, pairs2 = pairs
            pair_iter1 = PairIterator(pairs1)
            pair_iter2 = PairIterator(pairs2)
            while True:
                number1 = pair_iter1.number
                number2 = pair_iter2.number
                if number1 == number2:
                    ids_right1 = pair_iter1.right_ids
                    ids_right2 = pair_iter2.right_ids
                    cache_key = (id(ids_right1), id(ids_right2))
                    try:
                        ids_right_new = cache[cache_key]
                    except KeyError:
                        ids_right_new = snp.intersect(ids_right1, ids_right2)
                        cache[cache_key] = ids_right_new
                    if len(ids_right_new) != 0:
                        ids_left.append(number1)
                        ids_right.append(ids_right_new)
                    pair_iter1.next()
                    pair_iter2.next()
                elif number2 > number1:
                    pair_iter1.find(number2)
                else:
                    pair_iter2.find(number1)
        except StopIteration:
            self.ids_left = np.array(ids_left, dtype="uint32")
            self.ids_right = ids_right
            self.matches = snp.intersect(pairs1.matches, pairs2.matches)

    def __iter__(self):
        for id_left, id_right_list in zip(self.ids_left, self.ids_right):
            for id_right in id_right_list:
                yield id_left, id_right

    def merge(self, other):
        return Pairs(pairs=(self, other))

    @property
    def non_matches_count(self):
        return len(self) - self.matches_count

    @property
    def matches_count(self):
        return len(self.matches)

    def __len__(self):
        return sum(map(len, self.ids_right))

    @staticmethod
    def _generate_pairs(blocks):
        pairs = {}
        for block1, block2 in zip(blocks[1], blocks[2]):
            block2_list = np.fromiter(block2, dtype="uint32")
            block2_list.sort()
            cache = {}
            for number in block1:
                if number in pairs:
                    old_values = pairs[number]
                    cache_key = id(old_values)
                    try:
                        updated = cache[cache_key]
                    except KeyError:
                        updated = np.fromiter(set(old_values) | block2, dtype="uint32")
                        updated.sort()
                        cache[cache_key] = updated
                    pairs[number] = updated
                else:
                    pairs[number] = block2_list
        pairs = sorted(pairs.items())
        if len(pairs) != 0:
            ids_left, ids_right = zip(*pairs)
        else:
            ids_left, ids_right = [], []
        ids_left = np.array(ids_left, dtype="uint32")
        return ids_left, ids_right

    @staticmethod
    def _generate_matches(blocks):
        matches = blocks[1].values & blocks[2].values
        matches = np.fromiter(set(chain.from_iterable(matches)), dtype="uint32")
        matches.sort()
        return matches


class PairIterator(object):
    def __init__(self, pairs):
        self.ids_left = pairs.ids_left
        self.ids_right = pairs.ids_right
        try:
            self.number = self.ids_left[0]
            self.index = 0
        except IndexError:
            raise StopIteration()

    @property
    def right_ids(self):
        return self.ids_right[self.index]

    def next(self):
        try:
            self.index += 1
            self.number = self.ids_left[self.index]
        except IndexError:
            raise StopIteration()

    def find(self, number):
        try:
            while self.number < number:
                self.index += 1
                self.number = self.ids_left[self.index]
        except IndexError:
            raise StopIteration()


class BlockedPairs(object):
    def __init__(self, keys=None, blocked_pairs=None, all_keys=None):
        if keys is not None:
            self._init_with_keys(keys, all_keys)
        elif blocked_pairs is not None:
            self._init_with_blocked_pairs(blocked_pairs)
        else:
            raise ValueError("specify keys or blocked_pairs")

    def _init_with_keys(self, keys, all_keys):
        key1, key2 = keys
        all_keys1, all_keys2 = all_keys
        self.all_keys1 = all_keys1.unique()
        self.all_keys2 = all_keys2.unique()
        key1 = self._convert_key(key1)
        key2 = self._convert_key(key2)
        blocks = self._generate_blocks(key1, key2)
        self.pairs = Pairs(blocks=blocks)

    def _init_with_blocked_pairs(self, blocked_pairs):
        blocked_pairs1, blocked_pairs2 = blocked_pairs
        self.all_keys1 = blocked_pairs1.all_keys1.union(blocked_pairs2.all_keys1)
        self.all_keys2 = blocked_pairs1.all_keys2.union(blocked_pairs2.all_keys2)
        self.pairs = Pairs(pairs=(blocked_pairs1.pairs, blocked_pairs2.pairs))

    def compile(self):
        return iter(self.pairs)

    @staticmethod
    def _convert_key(key):
        if isinstance(key, list):
            key = list(map(pd.Series, key))
            key = pd.concat(key)
        else:
            key = pd.Series(key)
        return key

    def reduction_ratio(self):
        return 1 - len(self.pairs) / self._cartasian_product()

    def _cartasian_product(self):
        return len(self.all_keys1) * len(self.all_keys2)

    def merge(self, other):
        return BlockedPairs(blocked_pairs=(self, other))

    @property
    def matches(self):
        return self.pairs.matches

    @property
    def non_matches_count(self):
        return self.pairs.non_matches_count

    @property
    def matches_count(self):
        return self.pairs.matches_count

    @staticmethod
    def _generate_blocks(key1, key2):
        key1.name = 1
        key2.name = 2
        blocks1 = key1.groupby(key1).agg(lambda x: set(x.index))
        blocks2 = key2.groupby(key2).agg(lambda x: set(x.index))
        blocks = pd.concat([blocks1, blocks2], join="inner", axis=1)
        blocks = blocks.reset_index(drop=True)
        return blocks
