import json
from collections import defaultdict
from pathlib import Path

from app.config import PredicateConfig
import config
from app.config import DEFAULT_PREDICATE_CONFIG
from app.data import gen_path

from ._learn import Predicates, learn_blocking_key
from hashlib import sha256

ROOT = Path(config.ROOT)


def _load_blocking_keys(date1, date2):
    blocking_key_path = gen_path(date1, date2, train=True, filename="blocking_keys.json")

    with open(blocking_key_path, "r") as file:
        return json.load(file)


def _write_blocking_keys(date1, date2, keys):
    blocking_key_path = gen_path(date1, date2, train=True, filename="blocking_keys.json")

    with open(blocking_key_path, "w") as file:
        json.dump(keys, file)


class Key:
    def __init__(self, disjunction, precission):
        self.disjunction = self._clean_disjunction(disjunction)
        self.precission = precission

    def block(self, data1, data2):
        atoms = {atom for conjunction in self.disjunction for atom in conjunction}
        predicate_config = PredicateConfig()
        for col1, col2, method in atoms:
            predicate_config.add(col1, col2, method)
        predicates = Predicates.from_data(data1, data2, predicate_config)
        conjunction = Predicates(map(predicates.from_keys, self.disjunction))
        return conjunction.pairs()

    def to_json(self):
        return list(
            map(lambda conjunction: list(map(list, conjunction)), self.disjunction)
        )

    @staticmethod
    def _clean_disjunction(disjunction):
        cleaned_disjunction = []
        for conjunction in disjunction:
            cleaned_disjunction.append(Key._clean_conjunction(conjunction))
        duplicate_free_disjunction = set(cleaned_disjunction)
        return Key._eliminate_redundant_keys(duplicate_free_disjunction)

    @staticmethod
    def _clean_conjunction(conjunction):
        cleaned_conjunction = []
        for atom in conjunction:
            cleaned_conjunction.append(tuple(atom))
        return tuple(sorted(cleaned_conjunction))

    @staticmethod
    def _eliminate_redundant_keys(disjunction):
        redundancy_free_disjunction = set()
        for conjunction in disjunction:
            for other in disjunction:
                if other is not conjunction:
                    if Key._in_conjunction(other, conjunction):
                        break
            else:
                redundancy_free_disjunction.add(conjunction)
        return redundancy_free_disjunction

    @staticmethod
    def _in_conjunction(other, conjunction):
        other = set(other)
        conjunction = set(conjunction)
        return other.intersection(conjunction) == other

    def used_fields_right(self):
        return set(atom[0] for conjunction in self.disjunction for atom in conjunction)

    def used_fields_left(self):
        return set(atom[1] for conjunction in self.disjunction for atom in conjunction)

    def hash(self):
        hasher = sha256()
        hasher.update(bytes(repr(sorted(map(sorted, self.disjunction))), "utf-8"))
        return hasher.hexdigest()

class Keys:
    def __init__(self, keys, date1, date2):
        self.date1 = date1
        self.date2 = date2
        self.keys = {}
        for precission, key in keys.items():
            precission = float(precission)
            self.keys[precission] = Key(key, precission)

    def get(self, min_precission):
        last_key = None
        for precission, key in sorted(self.keys.items(), key=lambda x: x[0]):
            if precission >= min_precission:
                return key
            last_key = key
        return last_key

    @staticmethod
    def learn(
        date1,
        date2,
        predicate_config=DEFAULT_PREDICATE_CONFIG,
        maxminor=500000,
        k=3,
        min_reduction_ratio=0.98,
    ):
        return Keys(
            learn_blocking_key(
                date1, date2, predicate_config, k, min_reduction_ratio, maxminor,
            ),
            date1,
            date2,
        )

    @staticmethod
    def load(date1, date2):
        return Keys(_load_blocking_keys(date1, date2), date1, date2)

    def to_json(self):
        return {precission: key.to_json() for precission, key in self.keys.items()}

    def save(self):
        _write_blocking_keys(self.date1, self.date2, self.to_json())

    def blocksizes(self, data1, data2):
        sizes = {}
        for precission, value in self.keys.items():
            predicates = value.block(data1, data2)
            predicates = Predicates(map(predicates.from_keys, value.disjunction))
            sizes[precission] = len(predicates.pairs())
        return sizes
