from ._block import apply_method
import logging


class BlockGenerator(object):
    def __init__(self, method, data1, data2, col1_col2):
        self.data1 = data1
        self.data2 = data2
        self.method = method
        self.col1_col2 = col1_col2.copy()
        self.cache = {}

    def __iter__(self):
        while len(self.col1_col2) > 0:
            col1, cols2 = self.col1_col2.popitem()
            for col2 in cols2:
                key1 = apply_method(self.method, self.data1[col1].explode())
                key2 = self.cache.get(col2)
                if key2 is None:
                    key2 = apply_method(self.method, self.data2[col2].explode())
                    self.cache[col2] = key2
                if not self._col_is_needed(col2):
                    del self.cache[col2]
                yield col1, key1, col2, key2

    def _col_is_needed(self, col2):
        for cols2 in self.col1_col2.values():
            if col2 in cols2:
                return True
        return False
