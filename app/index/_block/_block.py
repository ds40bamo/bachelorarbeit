import jellyfish
import numpy as np
import pandas as pd
from doublemetaphone import doublemetaphone as _doublemetaphone


def astype(_type):
    def decorator(func):
        def wrapper(*args, **kwargs):
            arg1 = _clean(args[0])
            arg1 = arg1.astype(_type)
            arg1 = _clean(arg1)
            return func(arg1, *args[1:], **kwargs)

        return wrapper

    return decorator


def _clean(data):
    try:
        data = data[data != ""]
    except TypeError:
        pass

    try:
        data = data[~np.isnat(data)]
    except TypeError:
        pass

    data = data[~(data.isna())]

    return data


def apply_method(method, series):
    try:
        result = first(series, int(method))
    except ValueError:
        result = methods[method](series)

    if isinstance(result, pd.Series):
        return _clean(result)
    result = map(_clean, result)
    result = pd.concat(result)
    return result.astype("string")


@astype("str")
def first(series, n):
    return series.str[:int(n)].str.strip()


@astype("str")
def exact(series):
    return series


@astype("str")
def soundex(series):
    return series.map(jellyfish.soundex)


@astype("str")
def nysiis(series):
    return series.map(jellyfish.nysiis)


@astype("str")
def doublemetaphone(series):
    result = series.map(_doublemetaphone)
    return [result.str[0], result.str[1]]


@astype("datetime64")
def data_3(series):
    offset = pd.DateOffset(1)
    return [
        (series - offset),
        series,
        (series + offset),
    ]


@astype("datetime64")
def month(series):
    return series.dt.month.astype("str")


@astype("datetime64")
def year(series):
    return series.dt.year.astype("str")


@astype("datetime64")
def day_month(series):
    return series.dt.day.astype("str").str.zfill(2) + series.dt.month.astype(
        "str"
    ).str.zfill(2)


@astype("datetime64")
def day_year(series):
    return series.dt.day.astype("str").str.zfill(2) + series.dt.year.astype("str")


@astype("datetime64")
def month_year(series):
    return series.dt.month.astype("str").str.zfill(2) + series.dt.year.astype("str")


methods = {
    "exact": exact,
    "soundex": soundex,
    "nysiis": nysiis,
    "doublemetaphone": doublemetaphone,
    "date_3": data_3,
    "month": month,
    "year": year,
    "day_month": day_month,
    "day_year": day_year,
    "month_year": month_year,
}
