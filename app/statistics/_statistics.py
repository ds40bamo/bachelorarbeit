from app.data import gen_path
import pandas as pd
import numpy as np
from math import ceil
import matplotlib.pyplot as plt

def data_statistics(date, list_fields={"middle_name": "|", "last_name": "|", "name_suffix": " "}, num_cols_at_once=10):
    _path = gen_path(date, filename="parsed.csv")
    columns = pd.read_csv(_path, nrows=0).columns
    result = pd.DataFrame()
    for subcols in np.array_split(columns, ceil(len(columns) / num_cols_at_once)):
        data = pd.read_csv(_path, dtype="str", keep_default_na=False, na_values=["", " ", "  ", "   "], usecols=subcols)
        for column in data.columns:
            col_stat = {}
            if column in list_fields:
                counts = data[column].str.split(list_fields[column]).explode().value_counts()
            else:
                counts = data[column].value_counts()
            has_value = len(counts) != 0
            mid = ceil((len(counts)-1)/2)
            col_stat["median"] = counts.index[mid] if has_value else np.nan
            col_stat["median_count"] = counts.iloc[mid] if has_value else np.nan
            col_stat["mode"] = counts.index[0] if has_value else np.nan
            col_stat["mode_count"] = counts.iloc[0] if has_value else np.nan
            col_stat["freq_std"] = counts.std() if has_value else np.nan
            col_stat["num_values"] = len(counts)
            col_stat["num_missing"] = data[column].isna().sum()
            result = result.append(pd.Series(col_stat, name=column))
        del data
    return result

def date_frequency(date, date_cols=["birth_date", "registration_date"]):
    _path = gen_path(date, filename="parsed.csv")
    data = pd.read_csv(_path, parse_dates=date_cols, usecols=date_cols, keep_default_na=False, na_values=[""])

    for col in date_cols:
        date_col = data[col].dropna()
        day_month = pd.DataFrame({"day": date_col.dt.day, "month": date_col.dt.month})
        counts = day_month.groupby(["month", "day"]).size()
        counts.name = "count"
        counts.plot(use_index=True)
        file_name = f"{date}_{col}_month_day.png"
        plt.savefig(file_name, bbox_inches="tight")
        print(f"generated {file_name}")
        plt.clf()

        counts = data[col][data[col].dt.year > 1900].value_counts()
        counts = counts.sort_index()
        smooth = counts.rolling(365).mean()
        smooth.plot(use_index=True)
        file_name = f"{date}_{col}_year.png"
        plt.savefig(file_name, bbox_inches="tight")
        print(f"generated {file_name}")
        plt.clf()
