import hashlib
import json
import logging
import re
import shutil
from pathlib import Path
from threading import Thread
from urllib.parse import urljoin
from zipfile import ZipFile

import requests

import config
from app.utils import parse_date
from bs4 import BeautifulSoup
from ._utils import gen_path

DATA_URL = config.DATA_URL


def download(date):
    parsed_date = parse_date(date)
    logger = logging.getLogger(parsed_date)
    logger.setLevel(logging.INFO)
    data_path = gen_path(date)
    extract_path = gen_path(date, raw=True)

    logger.info("checking hash")
    if not hash_is_valid(date):
        logger.info("hash is not valid")
        shutil.rmtree(extract_path, ignore_errors=True)
        file_path = get_existing_zip_file(data_path)
        if file_path is not None:
            logger.info("found existing zip file: %s", file_path)
            try:
                logger.info("trying extract zip")
                extract_zip(file_path, extract_path, logger=logger)
                logger.info("extracting success")
            except Exception:
                logger.info("zipfile broken, downloading data")
                shutil.rmtree(extract_path, ignore_errors=True)
                get_data(date)
        else:
            logger.info("fetching data remotly")
            get_data(date)

        try:
            logger.info("generating hash")
            write_hash(date)
        except Exception as ex:
            logger.info("problem generating hash: %s", ex)
        logger.info("done")
    else:
        logger.info("hash is valid, nothing to do")


def get_existing_zip_file(data_path):
    files = list(Path(data_path).glob("*.zip"))
    if len(files) > 0:
        return files[0]
    return None


def hash_is_valid(date):
    hash_path = gen_path(date, raw=True, filename="hash.json")

    try:
        with open(hash_path, "r") as file:
            validate_hash = json.load(file)["hash"]
            return gen_hash(date) == validate_hash
    except Exception:
        return False


def write_hash(date):
    hash_path = gen_path(date, raw=True, filename="hash.json")
    with open(hash_path, "w") as file:
        json.dump({"hash": gen_hash(date)}, file)


def gen_hash(date):
    extract_path = gen_path(date, raw=True)
    files = filter(lambda path: path.is_file(), extract_path.glob("*.txt"))
    md5 = hashlib.md5()
    for file in sorted(files):
        with open(file, "rb") as binary:
            md5.update(binary.read())
    return md5.digest().hex()


FOLLOW_BUZZ_WORDS = ["extract", "detail"]
FOLLOW_STOP_WORDS = ["history"]
DATE_RE = re.compile(".*[0-9]{8}.*")


def is_candidate_follow(link):
    link = link.lower()
    return (
        link.endswith("/")
        and not link.startswith("/download")
        and (any([word in link for word in FOLLOW_BUZZ_WORDS]) or DATE_RE.match(link))
        and not any([word in link for word in FOLLOW_STOP_WORDS])
    )


ZIP_BUZZ_WORDS = ["extract", "vr", "detail", "registration"]
ZIP_STOP_WORDS = ["history", "vh", "_h"]


def is_candidate_zip(link):
    link = link.lower()
    return (
        link.endswith(".zip")
        and (any([word in link for word in FOLLOW_BUZZ_WORDS]) or DATE_RE.match(link))
        and not any([word in link for word in ZIP_STOP_WORDS])
    )


def is_candidate_txt(link):
    link = link.lower()
    return link.endswith(".txt") and "_h" not in link


def is_candidate_link(tag):
    clean_tag = " ".join(tag.text.lower().split())
    return clean_tag != "parent directory"


def find_data(base_url):
    try:
        req = requests.get(base_url)
        req.raise_for_status()
    except Exception:
        return None

    soup = BeautifulSoup(req.text, features="lxml")

    link_tags = soup.find_all("a", href=True)

    links = [a.get("href") for a in link_tags if is_candidate_link(a)]
    zip_links = [link for link in links if is_candidate_zip(link)]
    txt_links = [link for link in links if is_candidate_txt(link)]
    follow_links = [link for link in links if is_candidate_follow(link)]

    if len(zip_links) > 0:
        return [urljoin(base_url + "/", link) for link in zip_links]
    elif len(txt_links) > 10:
        return [urljoin(base_url + "/", link) for link in txt_links]
    elif len(follow_links) > 0:
        for link in follow_links:
            urls = find_data(urljoin(base_url + "/", link))
            if urls is not None:
                return urls
    return None


def get_data(date):
    parsed_date = parse_date(date)

    data_path = gen_path(date)
    extract_path = gen_path(date, raw=True)
    zip_save_path = gen_path(date, filename=f"{parsed_date}.zip")
    url = urljoin(DATA_URL + "/", parsed_date)

    logger = logging.getLogger(parsed_date)
    logger.setLevel(logging.INFO)

    logger.info("looking up data files...")
    urls = find_data(url)
    if urls is not None:
        first_url = urls[0]
        if first_url.endswith(".zip"):
            logger.info("downloading zip %s", first_url)
            download_file(first_url, zip_save_path, logger=logger)
            logger.info("extracting zip...")
            extract_zip(zip_save_path, extract_path, logger=logger)
        else:
            logger.info("downloading txt files")
            download_txts(urls, date, logger=logger)
    else:
        logger.warning("no data found")
        raise Exception("no data found")


def download_txts(urls, date, logger=None):
    file_count = len(urls)
    for i, url in enumerate(urls, start=1):
        filename = url.split("/")[-1]
        if logger is not None:
            logger.info("file %s of %s: %s", i, file_count, filename)
        save_path = gen_path(date, raw=True, filename=filename)
        download_file(url, save_path)


def extract_zip(zip_path, extract_path, logger=None):
    extract_path = Path(extract_path)
    extract_path.mkdir(exist_ok=True, parents=True)
    with ZipFile(zip_path, "r") as zip_file:
        for zip_info in zip_file.infolist():
            filename = zip_info.filename
            if filename[-1] == "/" or not filename.endswith(".txt"):
                continue
            if logger:
                logger.info("extracting %s", filename)
            zip_info.filename = str(Path(filename).name)
            zip_file.extract(zip_info, extract_path)


def download_file(url, save_path, logger=None):
    chunk_size = 20 * 1024 ** 2
    Path(save_path).parent.mkdir(exist_ok=True, parents=True)

    req = requests.get(url, stream=True)
    req.raise_for_status()

    last_read_data = None
    read_data = 0
    with open(save_path, "wb") as file:
        for chunk in req.iter_content(chunk_size=chunk_size):
            last_read_data = read_data
            read_data += len(chunk)
            file.write(chunk)
            last_block = last_read_data // chunk_size
            curr_block = read_data // chunk_size
            if curr_block != last_block and logger is not None:
                logger.info("%s MB", read_data // 1024 ** 2)
