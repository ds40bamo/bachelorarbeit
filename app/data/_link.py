from itertools import chain, product

import jsonlines
import pandas as pd

from ._utils import gen_path


def _diagonal_excluding_product(iterable):
    iterable = list(iterable)
    for i, element1 in enumerate(iterable):
        for j, element2 in enumerate(iterable):
            if i != j:
                yield (element1, element2)


def load_dedupe_clusters(date, counties=None, _hash=None):
    dedupe_path = gen_path(date, filename="clusters.jsonl", counties=counties, _hash=_hash)
    with jsonlines.open(dedupe_path, "r") as file:
        return pd.DataFrame(
            chain.from_iterable(
                _diagonal_excluding_product(cluster) for cluster in file
            ),
            columns=["left", "right"],
            dtype="uint32",
        )


def load_dedupe_links(date, counties=None, _hash=None):
    dedupe_path = gen_path(date, filename="links.csv", counties=counties, _hash=_hash)
    distances = pd.read_csv(
        dedupe_path,
        usecols=["left", "right", "prob"],
        dtype={"left": "uint32", "right": "uint32", "status": "int8", "prob": float},
    )
    distances["distance"] = 1.0 - distances["prob"]
    del distances["prob"]
    return distances


def load_link_links(date1, date2):
    link_path = gen_path(date1, date2, filename="clusters.jsonl")
    with jsonlines.open(link_path, "r") as file:
        return pd.DataFrame(file, columns=["left", "right"], dtype="uint32")


def load_review_links(date1, date2, counties):
    links_path = gen_path(date1, date2, filename="links.csv", counties=counties)
    links = pd.read_csv(
        links_path,
        dtype={"left": "uint32", "right": "uint32", "status": "int8", "prob": "float"},
        keep_default_na=False,
    )
    return links.set_index(["left", "right"])


def load_review_link_data(date1, date2, config, counties, voter_ids=False):
    links = load_review_links(date1, date2, counties)
    index1 = links.index.get_level_values(0)
    index2 = links.index.get_level_values(1)

    if date2 is None:
        data = config.load_classify(date1, counties=counties)
        data1, data2 = data, data
    else:
        data1, data2 = config.load_classify(date1, date2, counties=counties, voter_ids=voter_ids)

    return links, data1, data2
