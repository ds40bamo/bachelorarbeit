from pathlib import Path

import config
from app.utils import parse_date
from functools import reduce

ROOT = Path(config.ROOT)
import re
county_re = re.compile("[^a-zA-Z]")

def _insert_hash(name, _hash):
    first_component, *rest = name.split(".", 1)
    first_component += "_" + _hash
    name = [first_component, *rest]
    return ".".join(name)

def gen_path(date1, date2=None, train=False, raw=False, filename=None, suffix=None, counties=None, mkdir=False, _hash=None):
    _path = ROOT

    if isinstance(date1, str):
        date1 = [date1]

    if isinstance(date2, str):
        date2 = [date2]

    if date2 is None:
        pre_subpath = "data"
    else:
        if train:
            pre_subpath = "train"
        else:
            pre_subpath = "combine"

    if _hash is not None:
        if filename is not None:
            filename = _insert_hash(filename, _hash)
        if suffix is not None:
            suffix = _insert_hash(suffix, _hash)

    if counties is not None:
        counties = sorted([county_re.sub("", county.upper()) for county in counties])
        _path /= "evaluation"
        _path /= "_".join(counties)

    if pre_subpath is not None:
        _path /= pre_subpath

    parsed_date1 = parse_date(date1)
    if date2 is not None:
        parsed_date2 = parse_date(date2)

        if set(parsed_date1) & set(parsed_date2):
            raise ValueError("some dates are same")

        combined_dates = list(map(parse_date, parsed_date1 + parsed_date2))

        name = "_".join(combined_dates)
        _path /= name
    else:
        _path /= "_".join(parsed_date1)

    if raw:
        _path /= "raw"

    if mkdir:
        _path.mkdir(exist_ok=True, parents=True)

    if filename is not None:
        return _path / filename

    if suffix is not None:
        path1 = _path / ("_".join(parsed_date1) + suffix)
        path2 = _path / ("_".join(parsed_date2) + suffix)
        return path1, path2
    return _path
