from app.compare import apply_metrics
import pandas as pd

def classify(data1, data2, key, classifier, config, blocks_are_same=None):
    if blocks_are_same is not None and blocks_are_same:
        index = data1.index
        pairs = key.block(data1.reset_index(drop=True), data2.reset_index(drop=True))
        pairs = ((idx_left, idx_right) for idx_left, idx_right in pairs if idx_left > idx_right)
        remaped_indexes = list(map(lambda x: index[list(x)], zip(*pairs)))
        pairs = set(zip(*remaped_indexes))
    else:
        pairs = key.block(data1, data2)
    pairs = pd.MultiIndex.from_tuples(pairs)
    pairs = pairs.set_names(["left", "right"])
    encode = apply_metrics(pairs, data1, data2, config)
    classified = classifier.predict(encode)
    classified = classified.set_index(pairs)
    selector = (classified["status"] == 1) | (classified["prob"] == 1.0)
    matches = classified[selector]
    classified = classified[~selector]
    uncertain = classified[classified["prob"] > 0.0]
    assert len(matches.index.intersection(uncertain.index)) == 0
    return matches, uncertain
