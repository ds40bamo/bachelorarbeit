from app.data import gen_path
import pandas as pd

def no_dedupe(date, chunksize=3000000):
    src_path = gen_path(date, filename="parsed.csv")

    mode = "w"
    header = True
    chunk_iter = pd.read_csv(
        src_path, dtype="str", keep_default_na=False, chunksize=chunksize
    )
    dest_path = gen_path(date, filename="output.csv", mkdir=True)
    for chunk in chunk_iter:
        chunk.rename(columns={"voter_id": "voter_ids"}).to_csv(dest_path, mode=mode, header=header, index=False)
        mode = "a"
        header = False
        del chunk
