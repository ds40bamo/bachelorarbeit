import logging
from itertools import product

import ray

from app.utils import triangle

from ._classify import classify


@ray.remote
def dedupe_remote_classify(data1, data2, key, classifier, metric_config, blocks_are_same):
    return classify(data1, data2, key, classifier, metric_config, blocks_are_same)

@ray.remote
def link_remote_classify(data1, data2, key, classifier, metric_config):
    return classify(data1, data2, key, classifier, metric_config)

def dedupe_classify_builder(block1, block2, key, classifier, metric_config):
    return dedupe_remote_classify.remote(
        block1, block2, key, classifier, metric_config, block1 is block2
    )

def link_classify_builder(block1, block2, key, classifier, metric_config):
    return link_remote_classify.remote(
        block1, block2, key, classifier, metric_config
    )

def dedupe_block_builder(data1_split, data2_split):
    are_same_splits = data1_split is data2_split

    data1_split = [ray.put(split) for split in data1_split]

    if are_same_splits:
        blocks = list(triangle(iter(data1_split)))
    else:
        data2_split = [ray.put(split) for split in data2_split]
        blocks = list(product(data1_split, data2_split))

    return blocks

def link_block_builder(data1_split, data2_split):
    data1_split = [ray.put(split) for split in data1_split]
    data2_split = [ray.put(split) for split in data2_split]
    blocks = list(product(data1_split, data2_split))

    return blocks

def distributed(data1_split, data2_split, out_path, key, classifier, metric_config, num_workers, append, name, block_builder, classify_builder):
    ray.init("auto", ignore_reinit_error=True)

    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    blocks = block_builder(data1_split, data2_split)

    num_parts = len(blocks)
    logger.info("number of parts to link: %d", num_parts)

    key = ray.put(key)
    classifier = ray.put(classifier)
    metric_config = ray.put(metric_config)

    mode = "a" if append else "w"
    header = False if append else True

    current_block = 1

    running = []
    finnished = []

    for block1, block2 in blocks:
        running.append(
            classify_builder(block1, block2, key, classifier, metric_config)
        )
        if len(running) > num_workers:
            finnished, running = ray.wait(running)
            for matches, uncertain in ray.get(finnished):
                logger.info("linked: %d/%d", current_block, num_parts)
                current_block += 1
                matches.append(uncertain).to_csv(out_path, mode=mode, header=header)
                mode = "a"
                header = False

    while len(running) > 0:
        finnished, running = ray.wait(running)
        for matches, uncertain in ray.get(finnished):
            logger.info("linked: %d/%d", current_block, num_parts)
            current_block += 1
            matches.append(uncertain).to_csv(out_path, mode=mode, header=header)
            mode = "a"
            header = False

def dedupe(
    data1_split,
    data2_split,
    out_path,
    key,
    classifier,
    metric_config,
    num_workers,
    append=False,
):
    distributed(
        data1_split,
        data2_split,
        out_path,
        key,
        classifier,
        metric_config,
        num_workers,
        append,
        "dedupe",
        dedupe_block_builder,
        dedupe_classify_builder
    )

def link(
    data1_split,
    data2_split,
    out_path,
    key,
    classifier,
    metric_config,
    num_workers,
    append=False,
):
    distributed(
        data1_split,
        data2_split,
        out_path,
        key,
        classifier,
        metric_config,
        num_workers,
        append,
        "link",
        link_block_builder,
        link_classify_builder
    )
