import numpy as np

from app.data import gen_path

from ._distributed import link as distributed_link
from ._distributed import dedupe as distributed_dedupe
from ._local import link as local_link
from ._local import dedupe as local_dedupe

def split_data(data, blocksize=10000):
    num_blocks = len(data) // blocksize
    return np.array_split(data, num_blocks)


def dedupe(date, classifier, dedupe_config, chunksize=3000000, blocksize=100000, num_workers=1, counties=None, dst_hash=None, voter_ids=False):
    link_config = dedupe_config.to_link_config()
    key = link_config._key
    out_path = gen_path(date, filename="links.csv", counties=counties, _hash=dst_hash)
    append = False
    for data1, data2 in dedupe_config.iterate_classify_chunks(date, chunksize, counties=counties, voter_ids=voter_ids):
        data1_split = split_data(data1, blocksize=blocksize)
        if data1 is data2:
            data2_split = data1_split
        else:
            data2_split = split_data(data2, blocksize=blocksize)
        if num_workers > 1:
            distributed_dedupe(
                data1_split,
                data2_split,
                out_path,
                key,
                classifier,
                link_config,
                num_workers,
                append=append
            )
        else:
            local_dedupe(data1_split, data2_split, out_path, key, classifier, link_config, append=append)
        append=True
        del data1
        del data2
        del data1_split
        del data2_split


def link(date1, date2, classifier, link_config, chunksize=1000000, blocksize=100000, num_workers=1, counties=None, src_hash=None, dst_hash=None):
    key = link_config._key
    out_path = gen_path(date1, date2, filename="links.csv", counties=counties, _hash=dst_hash)
    append = False
    for data1, data2 in link_config.iterate_classify_chunks(date1, date2, chunksize=chunksize, counties=counties, _hash=src_hash):
        data1_split = split_data(data1, blocksize=blocksize)
        data2_split = split_data(data2, blocksize=blocksize)
        if num_workers > 1:
            distributed_link(
                data1_split,
                data2_split,
                out_path,
                key,
                classifier,
                link_config,
                num_workers,
                append=append,
            )
        else:
            local_link(data1_split, data2_split, out_path, key, classifier, link_config, append=append)
        append = True
        del data1
        del data2
        del data1_split
        del data2_split
