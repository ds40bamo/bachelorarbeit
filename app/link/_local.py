import inspect
import logging
from itertools import product, starmap

from app.utils import triangle

from ._classify import classify


def dedupe_classify_builder(block1, block2, key, classifier, config):
    return classify(block1, block2, key, classifier, config, block2 is block1)


def link_classify_builder(block1, block2, key, classifier, config):
    return classify(block1, block2, key, classifier, config)


def dedupe_block_builder(data1_split, data2_split):
    if data1_split is data2_split:
        blocks = list(triangle(iter(data1_split)))
    else:
        blocks = list(product(data1_split, data2_split))
    return blocks


def link_block_builder(data1_split, data2_split):
    blocks = list(product(data1_split, data2_split))
    return blocks


def local(
    data1_split,
    data2_split,
    out_path,
    key,
    classifier,
    config,
    append,
    name,
    block_builder,
    classify_builder,
):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    blocks = block_builder(data1_split, data2_split)

    num_parts = len(blocks)
    logger.info("number of parts to link: %d", num_parts)
    predictions = starmap(
        lambda block1, block2: classify_builder(
            block1, block2, key, classifier, config
        ),
        blocks,
    )

    mode = "a" if append else "w"
    header = False if append else True
    for i, results in enumerate(predictions, start=1):
        matches, uncertain = results
        logger.info("linked: %d/%d", i, num_parts)
        matches.append(uncertain).to_csv(out_path, mode=mode, header=header)
        mode = "a"
        header = False

def dedupe(
    data1_split,
    data2_split,
    out_path,
    key,
    classifier,
    config,
    append=False,
):
    local(
        data1_split,
        data2_split,
        out_path,
        key,
        classifier,
        config,
        append,
        "dedupe",
        dedupe_block_builder,
        dedupe_classify_builder
    )

def link(
    data1_split,
    data2_split,
    out_path,
    key,
    classifier,
    config,
    append=False,
):
    local(
        data1_split,
        data2_split,
        out_path,
        key,
        classifier,
        config,
        append,
        "link",
        link_block_builder,
        link_classify_builder
    )

