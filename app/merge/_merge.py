import inspect
import logging
from itertools import chain

import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

from app.data import (gen_path, load_dedupe_clusters, load_dedupe_links,
                      load_link_links)


def _get_clusters(distances, p):
    """
    remove edges weeker than p and return strongly connected components with sizes bigger than 1
    """
    distances = distances[distances["distance"] > p]
    distances = distances.append(
        distances.rename(columns={"left": "right", "right": "left"}), ignore_index=True
    )
    instances = set(distances.left)
    groups = distances.groupby("left")["right"]

    clusters = []
    try:
        while True:
            instance = instances.pop()
            cluster = {instance}
            queue = {instance}
            while queue:
                curr_node = queue.pop()
                right = groups.get_group(curr_node)
                for node in right:
                    if node not in cluster:
                        cluster.add(node)
                        queue.add(node)
            instances.difference_update(cluster)
            clusters.append(cluster)
    except KeyError:
        pass

    return clusters


def _data_iterator(path):
    chunks = pd.read_csv(path, dtype="str", keep_default_na=False, chunksize=1000000,)
    for chunk in chunks:
        chunk = chunk.rename(columns={"voter_ids": "voter_id"})
        chunk["voter_id"] = chunk["voter_id"].astype("uint32")
        yield chunk


def _take_most_frequent_or_newest_valid(column):
    counts = column.value_counts()
    if (counts != 1).sum() == 0:
        return column.iloc[0]
    return counts.index[0]


def _merge_first_name(first_names):
    plausible_name = _take_most_frequent_or_newest_valid(first_names)
    # name is inital
    if len(plausible_name) == 1:
        for name in first_names:
            if plausible_name != name and name.startswith(plausible_name):
                return name

    return plausible_name


def _merge_name_best_candidate(name, other_names):
    best_candiate = name
    new_other_names = set()
    for other_name in other_names:
        if other_name.startswith(best_candiate):
            best_candiate = other_name
        elif not best_candiate.startswith(other_name):
            new_other_names.add(other_name)
    return best_candiate, new_other_names


def _merge_name_lists(name_lists):
    new_name = []
    longest_name_idx = name_lists.str.len().idxmax()
    longest_name = name_lists.loc[longest_name_idx]
    other_names = name_lists[name_lists.index != longest_name_idx]
    other_names = chain.from_iterable(other_names)
    for name in longest_name:
        best_candidate, other_names = _merge_name_best_candidate(name, other_names)
        new_name.append(best_candidate)

    while other_names:
        name = other_names.pop()
        best_candidate, other_names = _merge_name_best_candidate(name, other_names)
        new_name.append(best_candidate)

    return new_name


def _merge_last_name(last_names):
    all_last_names = set(last_names.iloc[0])
    pos = 1
    for curr_last_names in last_names[1:]:
        if any(
            name.startswith(curr_last_name) or curr_last_name.startswith(name)
            for curr_last_name in curr_last_names
            for name in all_last_names
        ):
            all_last_names.update(curr_last_names)
        else:
            break
        pos += 1
    return _merge_name_lists(last_names[:pos])


def _take_most_frequent_value(cluster_instances, columns):
    """takes most frequent value per column, but np.nan if there are multiple most frequent
    """
    transformed = []
    for column in columns:
        names = ["_group", column]
        col_frame = cluster_instances[names]
        not_na = ~col_frame[column].isna()
        counts = pd.MultiIndex.from_frame(col_frame[not_na][names]).value_counts()
        _group_content, column_content = zip(*counts.index)
        frame = pd.DataFrame(
            {"_group": _group_content, column: column_content, "_count": counts.values}
        )
        frame.sort_values(["_group", "_count"], ascending=False, inplace=True)
        previous_group_is_not_same = frame["_group"].shift(1) != frame["_group"]
        next_group_is_not_same = frame["_group"].shift(-1) != frame["_group"]
        next_count_is_not_same = frame["_count"].shift(-1) != frame["_count"]
        selector = previous_group_is_not_same & (
            next_count_is_not_same | next_group_is_not_same
        )
        transformed.append(frame[selector][names].set_index("_group"))
    return pd.concat(transformed, axis=1).reindex(cluster_instances["_group"].unique())


def _newest_records_and_valid_records(cluster_instances):
    previous_is_not_same = (
        cluster_instances["_group"].shift(1) != cluster_instances["_group"]
    )
    next_is_not_same = (
        cluster_instances["_group"].shift(-1) != cluster_instances["_group"]
    )
    newest_records = cluster_instances[previous_is_not_same].set_index("_group")
    selector = cluster_instances.isna().apply(lambda x: x & next_is_not_same)
    cluster_instances[selector] = ""
    newest_valid_entries = (
        cluster_instances.bfill()[previous_is_not_same]
        .set_index("_group")
        .replace("", np.nan)
    )
    cluster_instances[selector] = cluster_instances[selector].replace("", np.nan)
    return newest_records, newest_valid_entries


def _newest_valid_address_record(cluster_instances):
    newest_valid_address_record = cluster_instances[
        ~cluster_instances["address"].isna()
    ]
    previous_group_is_not_same = (
        newest_valid_address_record["_group"].shift(1)
        != newest_valid_address_record["_group"]
    )
    newest_valid_address_record = newest_valid_address_record[
        previous_group_is_not_same
    ]
    return newest_valid_address_record


def _merge(cluster_instances, group, multiple_voter_ids=False):
    if len(cluster_instances) == 0:
        return cluster_instances
    cluster_instances["registration_date"] = cluster_instances[
        "registration_date"
    ].astype("datetime64")
    if multiple_voter_ids:
        cluster_instances["voter_ids"] = cluster_instances["voter_ids"].str.split("|")
    else:
        cluster_instances["voter_id"] = cluster_instances["voter_id"].astype(str)
    cluster_instances = cluster_instances.replace("", np.nan)
    cluster_instances["middle_name"] = cluster_instances["middle_name"].str.split("|")
    cluster_instances["last_name"] = cluster_instances["last_name"].str.split("|")
    cluster_instances.loc[cluster_instances["race"].isin(("6", "9")), "race"] = ""
    cluster_instances["excemption"] = cluster_instances["excemption"].replace({"True": True, "False": False})
    cluster_instances["_group"] = group
    sort_cols = ["_group", "registration_date"]
    if "_newer" in cluster_instances:
        sort_cols.append("_newer")
    cluster_instances.sort_values(sort_cols, ascending=False, inplace=True)
    if "_newer" in cluster_instances:
        del cluster_instances["_newer"]

    cluster_instances = cluster_instances.astype("object")
    newest_records, newest_valid_entries = _newest_records_and_valid_records(
        cluster_instances
    )
    most_frequent_or_newest_valid_entries = _take_most_frequent_value(
        cluster_instances, ["gender", "race", "birth_date"]
    )
    most_frequent_or_newest_valid_entries[
        most_frequent_or_newest_valid_entries.isna()
    ] = newest_valid_entries

    merged_newest_valid_fields = newest_valid_entries[
        [
            "party_affiliation",
            "status",
            "phone_number",
            "phone_extension",
            "email",
        ]
    ]
    del newest_valid_entries

    merged_frequent_fields = most_frequent_or_newest_valid_entries[
        ["gender", "race", "birth_date"]
    ]
    del most_frequent_or_newest_valid_entries

    newest_valid_address_record = _newest_valid_address_record(cluster_instances)
    records_with_same_newest_address = newest_valid_address_record[
        ["_group", "address"]
    ].merge(cluster_instances, on=["_group", "address"])
    records_with_same_newest_address.sort_values(
        ["_group", "registration_date"], ascending=False, inplace=True
    )
    _, newest_valid_address_record = _newest_records_and_valid_records(
        records_with_same_newest_address
    )
    newest_valid_address_record = newest_valid_address_record.reindex_like(
        newest_records
    )

    selector = newest_valid_address_record["address"].isna()
    newest_valid_address_record[selector] = newest_records

    selector = newest_valid_address_record["county"] != newest_records["county"]
    newest_valid_address_record[selector] = newest_records[selector]

    merged_newest_fields = newest_records[["registration_date"]]
    del newest_records

    merged_address_fields = newest_valid_address_record[
        [
            "county",
            "address",
            "address_addition",
            "city",
            "zipcode",
            "mail_address1",
            "mail_address2",
            "mail_address3",
            "mail_city",
            "mail_state",
            "mail_zipcode",
            "mail_country",
            "precinct",
            "precinct_group",
            "precinct_split",
            "precinct_suffix",
            "congressional_district",
            "house_district",
            "senate_district",
            "county_commission_district",
            "school_board_district",
            "phone_area_code",
            "zip4",
            "mail_zip4",
            "longitude",
            "latitude",
        ]
    ]
    del newest_valid_address_record

    groups = cluster_instances.groupby("_group")
    if multiple_voter_ids:
        new_voter_ids = (
            groups["voter_ids"].agg(lambda x: set(chain.from_iterable(x))).str.join("|")
        )
    else:
        new_voter_ids = groups["voter_id"].agg(list).str.join("|")
    new_excemption = groups["excemption"].all()
    groups = cluster_instances[~cluster_instances["first_name"].isna()].groupby(
        "_group"
    )
    new_first_name = groups["first_name"].agg(_merge_first_name)
    groups = cluster_instances[~cluster_instances["name_suffix"].isna()].groupby(
        "_group"
    )
    new_name_suffix = groups["name_suffix"].agg(
        lambda x: " ".join(set(chain.from_iterable(suffix.split() for suffix in x)))
    )
    groups = cluster_instances[~cluster_instances["middle_name"].isna()].groupby(
        "_group"
    )
    new_middle_name = groups["middle_name"].agg(_merge_name_lists).str.join("|")
    groups = cluster_instances[~cluster_instances["last_name"].isna()].groupby("_group")
    new_last_name = groups["last_name"].agg(_merge_last_name).str.join("|")

    new_records = pd.concat(
        [
            new_voter_ids,
            new_name_suffix,
            new_excemption,
            merged_address_fields,
            merged_frequent_fields,
            merged_newest_fields,
            merged_newest_valid_fields,
            new_first_name,
            new_middle_name,
            new_last_name,
        ],
        axis=1,
    )
    del cluster_instances["_group"]
    new_records["registration_date"] = (
        new_records["registration_date"].astype("datetime64").dt.strftime("%m/%d/%Y")
    )
    new_records = new_records[cluster_instances.columns].fillna("")
    if multiple_voter_ids:
        new_records = new_records.rename({"voter_id": "voter_ids"})
    return new_records


def dedupe_merge(date, p=0.5, counties=None, dedupe_hash=None, cluster_hash=None, dst_hash=None):
    """
    remove links weeker than p and take strongly connected components as duplicates

    Parameters
    ----------
    p : float
        minimum strength of a edge
    """
    logger = logging.getLogger(inspect.currentframe().f_code.co_name)
    logger.setLevel(logging.INFO)
    logger.info("loading data")
    distances = load_dedupe_links(date, counties=counties, _hash=dedupe_hash)
    cluster_pairs = load_dedupe_clusters(date, counties=counties, _hash=cluster_hash)
    distances = cluster_pairs.merge(distances, on=["left", "right"])
    clusters = _get_clusters(distances, p)
    load_path = gen_path(date, filename="parsed.csv", counties=counties)
    save_path = gen_path(date, filename="output.csv", counties=counties, _hash=dst_hash)
    instances = set(chain.from_iterable(clusters))
    cluster_chunks = []
    mode = "w"
    header = True
    logger.info("extracting cluster instances")
    for chunk in _data_iterator(load_path):
        selector = chunk["voter_id"].isin(instances)
        cluster_chunks.append(chunk[selector])
        chunk[~selector].rename(columns={"voter_id": "voter_ids"}).to_csv(
            save_path, mode=mode, header=header, index=False
        )
        mode = "a"
        header = False
    cluster_instances = pd.concat(cluster_chunks)
    # split dataframe into dataframes of records that are in same cluster
    cat = pd.DataFrame(
        ((node, i) for i, cluster in enumerate(clusters) for node in cluster),
        columns=["voter_id", "category"],
    ).set_index("voter_id")["category"]
    assert len(cat) == len(cluster_instances)
    group_selector = cat.reindex(cluster_instances["voter_id"]).values
    assert len(cat) == len(group_selector)
    cluster_instances["_group"] = group_selector
    # if more than 2 records have the same registration_date and birth_date it is unlikely a match
    # normally duplicates have a different registration_date (it are probably twins)
    bigger_one = (
        cluster_instances[["registration_date", "birth_date", "_group"]].value_counts()
        > 1
    )
    bigger_one_groups = set(bigger_one[bigger_one].reset_index()["_group"])
    selector = cluster_instances["_group"].isin(bigger_one_groups)
    cluster_instances.drop(columns="_group")[selector].to_csv(
        save_path, mode="a", header=False, index=False
    )
    cluster_instances = cluster_instances[~selector]
    group_col = cluster_instances["_group"].values
    del cluster_instances["_group"]
    logger.info("merging cluster records")
    merged_instances = _merge(cluster_instances, group_col)
    merged_instances.to_csv(save_path, mode="a", header=False, index=False)


def _read_next(data_iter, wait_for_merge_entries, offset, node_component_map, newer):
    try:
        entries = next(data_iter)
        entries["_group"] = node_component_map[entries.index + offset]
        entries["_newer"] = newer
        had_next = True
        if not wait_for_merge_entries is None:
            wait_for_merge_entries = wait_for_merge_entries.append(entries)
        else:
            wait_for_merge_entries = entries
    except StopIteration:
        had_next = False
    return wait_for_merge_entries, had_next


def _get_component_map_component_count(date1, date2):
    load_path_left, load_path_right = gen_path(date1, date2, suffix="_match.csv")
    ids_left = (
        pd.read_csv(load_path_left, usecols=["voter_ids"], dtype="str")["voter_ids"]
        .str.split("|")
        .explode()
        .astype("uint32")
    )
    ids_right = (
        pd.read_csv(load_path_right, usecols=["voter_ids"], dtype="str")["voter_ids"]
        .str.split("|")
        .explode()
        .astype("uint32")
    )
    ids_merged = (
        ids_left.to_frame()
        .reset_index()
        .merge(ids_right.to_frame().reset_index(), on="voter_ids")
    )
    nodes_left = ids_merged.index_x.values
    nodes_right = ids_merged.index_y.values
    min_right_node = nodes_left.max() + 1
    nodes_right += min_right_node
    row_col_num = nodes_right.max() + 1
    graph = csr_matrix(
        (np.ones(len(ids_merged)), (nodes_left, nodes_right)),
        shape=(row_col_num, row_col_num),
    )
    node_component_map = connected_components(graph, directed=False)[1]
    line_component_left = node_component_map[ids_left.index.unique()]
    line_component_right = node_component_map[ids_right.index.unique() + min_right_node]
    component_count = pd.Series(
        np.concatenate((line_component_left, line_component_right))
    ).value_counts()
    return node_component_map, component_count, min_right_node


def merge_match_files(date1, date2, chunksize=500000):
    load_path_left, load_path_right = gen_path(date1, date2, suffix="_match.csv")
    save_path = gen_path(date1, date2, filename="merged_match.csv")
    (
        node_component_map,
        component_count,
        right_offset,
    ) = _get_component_map_component_count(date1, date2)
    data_left_iter = pd.read_csv(
        load_path_left, dtype="str", keep_default_na=False, chunksize=chunksize
    )
    data_right_iter = pd.read_csv(
        load_path_right, dtype="str", keep_default_na=False, chunksize=chunksize
    )
    wait_for_merge_entries = None
    mode = "w"
    header = True
    while True:
        wait_for_merge_entries, left_had_next = _read_next(
            data_left_iter, wait_for_merge_entries, 0, node_component_map, newer=False
        )
        wait_for_merge_entries, right_had_next = _read_next(
            data_right_iter,
            wait_for_merge_entries,
            right_offset,
            node_component_map,
            newer=True,
        )
        if not left_had_next and not right_had_next:
            if len(wait_for_merge_entries) > 0:
                print("merge done, but there are unmerged entries !!!")
            break
        counts = wait_for_merge_entries["_group"].value_counts()
        all_instanes_in_memory = component_count.loc[counts.index] == counts
        cluster_ids = all_instanes_in_memory[all_instanes_in_memory].index
        selector = wait_for_merge_entries["_group"].isin(cluster_ids)
        mergable = wait_for_merge_entries[selector]
        wait_for_merge_entries = wait_for_merge_entries[~selector]
        group = mergable["_group"].values
        del mergable["_group"]
        merged_instances = _merge(mergable, group, multiple_voter_ids=True)
        del mergable
        merged_instances.to_csv(save_path, mode=mode, header=header, index=False)
        del merged_instances
        mode = "a"
        header = False


def read_link_data(date1, date2):
    load_path_left, load_path_right = gen_path(date1, date2, suffix="_classify.csv")
    save_path = gen_path(date1, date2, filename="merged_link.csv")
    clusters = load_link_links(date1, date2)
    link_lines_left, link_lines_right = (
        clusters["left"].values,
        clusters["right"].values,
    )
    link_lines_left = set(link_lines_left)
    link_lines_right = set(link_lines_right)
    data_left = pd.read_csv(load_path_left, dtype="str", keep_default_na=False).rename(
        {"voter_ids": "voter_id"}
    )
    selector = data_left.index.isin(link_lines_left)
    has_match_left = data_left[selector]
    data_left[~selector].to_csv(save_path, mode="w", header=True, index=False)
    data_right = pd.read_csv(
        load_path_right, dtype="str", keep_default_na=False
    ).rename({"voter_ids": "voter_id"})
    selector = data_right.index.isin(link_lines_right)
    has_match_right = data_right[selector]
    data_right[~selector].to_csv(save_path, mode="a", header=False, index=False)

    return has_match_left.loc[clusters.left], has_match_right.loc[clusters.right]


def merge_link_results(date1, date2):
    logger = logging.getLogger(inspect.currentframe().f_code.co_name)
    logger.setLevel(logging.INFO)
    save_path = gen_path(date1, date2, filename="merged_link.csv")
    logger.info("loading data")
    cluster_instances = pd.concat(read_link_data(date1, date2))

    length_of_one_database = len(cluster_instances) // 2
    newest = np.concatenate(
        (
            np.zeros(length_of_one_database, dtype=bool),
            np.ones(length_of_one_database, dtype=bool),
        )
    )
    cluster_instances["_newer"] = newest
    category = np.arange(length_of_one_database)
    logger.info("merging cluster records")
    merged_instances = _merge(
        cluster_instances, np.concatenate((category, category)), multiple_voter_ids=True
    )
    merged_instances.to_csv(save_path, mode="a", header=False, index=False)


def combine_merge(date1, date2):
    merged_match_path = gen_path(date1, date2, filename="merged_match.csv")
    merged_link_path = gen_path(date1, date2, filename="merged_link.csv")
    out_path = gen_path(date1, date2, filename="output.csv")

    header = True
    mode = "w"
    for chunk in pd.read_csv(merged_match_path, dtype="str", chunksize=1000000):
        chunk.to_csv(out_path, index=False, header=header, mode=mode)
        header = False
        mode = "a"

    for chunk in pd.read_csv(merged_link_path, dtype="str", chunksize=1000000):
        chunk.to_csv(out_path, index=False, header=header, mode=mode)
        header = False
        mode = "a"
