import shutil
from app.data import gen_path
from app.merge._merge import _get_component_map_component_count
from pathlib import Path
import pandas as pd
import jsonlines

def export(date1, date2, output_path, with_merged=False):
    """
    copy the final output of the linkage to a specified location

    Parameters
    ----------
    path_
        path where the data is extracted

    with_merge
        also copy the merged data
    """
    classify1_path, classify2_path = gen_path(date1, date2, suffix="_classify.csv")
    match1_path, match2_path = gen_path(date1, date2, suffix="_match.csv")
    cluster_path = gen_path(date1, date2, filename="clusters.jsonl")
    merge_path = gen_path(date1, date2, filename="output.csv")
    output_path = Path(output_path).expanduser() / f"{date1}_{date2}"
    links_path = output_path / "links.csv"

    component_map, _, offset = _get_component_map_component_count(date1, date2)
    match_ids_left = pd.read_csv(match1_path, usecols=["voter_ids"], dtype="str").rename(columns={"voter_ids": date1})
    match_ids_right = pd.read_csv(match2_path, usecols=["voter_ids"], dtype="str").rename(columns={"voter_ids": date2})
    match_ids_left["_group"] = component_map[match_ids_left.index]
    match_ids_right["_group"] = component_map[match_ids_right.index + offset]
    merged = match_ids_left.merge(match_ids_right, on="_group")

    with jsonlines.open(cluster_path, "r") as file:
        links = pd.DataFrame(file, columns=["left", "right"])

    ids_left = pd.read_csv(classify1_path, usecols=["voter_ids"], dtype="str")["voter_ids"]
    ids_right = pd.read_csv(classify2_path, usecols=["voter_ids"], dtype="str")["voter_ids"]
    links_ids_left = ids_left.loc[links["left"]]
    links_ids_right = ids_right.loc[links["right"]]

    merged = merged.append(pd.DataFrame({date1: links_ids_left, date2: links_ids_right}))

    output_path.mkdir(exist_ok=True, parents=True)
    merged.to_csv(links_path, header=True, index=False, mode="w")

    date1_path = output_path / f"{date1}.csv"
    date2_path = output_path / f"{date2}.csv"

    shutil.copyfile(match1_path, date1_path)
    shutil.copyfile(match2_path, date2_path)

    for chunk in pd.read_csv(classify1_path, chunksize=1000000, dtype="str"):
        chunk.to_csv(date1_path, header=False, index=False, mode="a")

    for chunk in pd.read_csv(classify2_path, chunksize=1000000, dtype="str"):
        chunk.to_csv(date2_path, header=False, index=False, mode="a")

    if with_merged:
        merge_output_path = output_path / "merged.csv"
        shutil.copyfile(merge_path, merge_output_path)
