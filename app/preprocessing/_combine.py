import logging

import pandas as pd
import numpy as np
from shutil import copyfile

from app.compare import apply_metrics
from app.utils import read_voter_ids
from app.data import gen_path


matching_fields = [
    "first_name",
    "middle_name",
    "last_name",
    "county",
    "name_suffix",
    "address",
    "city",
    "zipcode",
    "gender",
    "race",
    "birth_date",
    "registration_date",
    "zip4",
    "party_affiliation"
]

def separate(src_path, match_index, match_path, classify_path=None, chunksize=2000000):
    logger = logging.getLogger("separate")
    logger.setLevel(logging.INFO)

    # iterate over chunks and separate into records that have
    # a match and records that have no match (for later classify)
    csv_iter = pd.read_csv(src_path, dtype="str", keep_default_na=False, chunksize=chunksize)
    mode = "w"
    header = True
    for i, chunk in enumerate(csv_iter):
        logger.info("chunk %s", i)

        chunk_match_index = chunk.index & match_index
        match_data = chunk.reindex(chunk_match_index)
        match_data.to_csv(match_path, mode=mode, header=header, index=False)

        if classify_path is not None:
            classify_index = chunk.index.difference(chunk_match_index)
            classify_data = chunk.reindex(classify_index)
            classify_data.to_csv(classify_path, mode=mode, header=header, index=False)
        mode = "a"
        header = False


def combine_train(date1, date2, bufsize=2000000):
    logger = logging.getLogger("combine")
    logger.setLevel(logging.INFO)

    src1_path = gen_path(date1, filename="parsed.csv")
    src2_path = gen_path(date2, filename="parsed.csv")

    # read ids and find out which ids are same
    logger.info("reading ids 1")
    voter_ids1 = read_voter_ids(src1_path)
    logger.info("reading ids 2")
    voter_ids2 = read_voter_ids(src2_path)
    link_ids = pd.Index(voter_ids1.unique()) & pd.Index(voter_ids2.unique())
    has_match1 = voter_ids1.isin(link_ids)
    has_match2 = voter_ids2.isin(link_ids)
    match_index1 = voter_ids1[has_match1].index
    match_index2 = voter_ids2[has_match2].index

    match1_path, match2_path = gen_path(date1, date2, train=True, suffix="_match.csv", mkdir=True)

    separate(src1_path, match_index1, match1_path)
    separate(src2_path, match_index2, match2_path)
    extract_train(date1, date2, bufsize=bufsize)

def combine_classify(date1, date2, counties=None, _hash=None, pre=True):
    logger = logging.getLogger("combine")
    logger.setLevel(logging.INFO)

    src1_path = gen_path(date1, filename="output.csv", counties=counties, _hash=_hash)
    src2_path = gen_path(date2, filename="output.csv", counties=counties, _hash=_hash)

    # read ids and find out which ids are same
    logger.info("reading ids 1")
    voter_ids1 = read_voter_ids(src1_path, multiple=True).str.split("|").explode().astype("uint32")
    logger.info("reading ids 2")
    voter_ids2 = read_voter_ids(src2_path, multiple=True).str.split("|").explode().astype("uint32")
    link_ids = pd.Index(voter_ids1.unique()) & pd.Index(voter_ids2.unique())
    has_match1 = voter_ids1.isin(link_ids)
    has_match2 = voter_ids2.isin(link_ids)
    match_index1 = voter_ids1[has_match1].index.unique()
    match_index2 = voter_ids2[has_match2].index.unique()

    if pre:
        match_suffix = "_match_pre.csv"
        classify_suffix = "_classify_pre.csv"
    else:
        match_suffix = "_match.csv"
        classify_suffix = "_classify.csv"
    match1_path, match2_path = gen_path(date1, date2, suffix=match_suffix, counties=counties, _hash=_hash, mkdir=True)
    classify1_path, classify2_path = gen_path(date1, date2, suffix=classify_suffix, counties=counties, _hash=_hash, mkdir=True)

    separate(src1_path, match_index1, match1_path, classify1_path)
    separate(src2_path, match_index2, match2_path, classify2_path)


def extract_train(date1, date2, bufsize=2000000):
    logger = logging.getLogger("extract")
    logger.setLevel(logging.INFO)

    match1_path, match2_path = gen_path(date1, date2, train=True, suffix="_match.csv")
    train1_path, train2_path = gen_path(date1, date2, train=True, suffix="_train.csv")

    CSVTrainExtract(match1_path, match2_path, train1_path, train2_path, bufsize=bufsize).merge()


class RunningBuffer:
    def __init__(self, csv_running_path, csv_walking_path, bufsize):
        self.chunk_iterator = pd.read_csv(
            csv_running_path, dtype=str, keep_default_na=False, chunksize=int(0.1 * bufsize), index_col="voter_id"
        )
        self.buf = pd.read_csv(csv_running_path, dtype=str, index_col="voter_id", nrows=0)
        self.ids = pd.Index(read_voter_ids(csv_walking_path))
        self.csv_path = csv_running_path
        self.bufsize = bufsize
        self.all_data_in_window = False
        self.might_have_data = True
        self.runs = 0
        self.last_change_run = 0
        self.move()

    def drop(self):
        self.buf = self.buf.tail(int(0.9 * self.bufsize))

    def fill(self):
        logger = logging.getLogger("extract")
        logger.setLevel(logging.INFO)
        logger.info("filling run buffer")
        while self.buf.shape[0] < self.bufsize:
            try:
                next_chunk = next(self.chunk_iterator)
                same_index = self.buf.index & next_chunk.index
                if same_index.size > 0:
                    add_index = next_chunk.index.difference(same_index)
                    self.buf = self.buf.append(next_chunk.reindex(add_index))
                    self.all_data_in_window = True
                    return
                new_index = self.ids & next_chunk.index
                self.buf = self.buf.append(next_chunk.reindex(new_index))
            except StopIteration:
                self.chunk_iterator = pd.read_csv(
                    self.csv_path, dtype=str, keep_default_na=False, chunksize=(0.1 * self.bufsize), index_col="voter_id"
                )

    def move(self):
        if not self.all_data_in_window:
            if self.buf.size > 0.9 * self.bufsize:
                self.drop()
            self.fill()

    def update(self, keep_ids):
        if keep_ids.size == 0:
            self.last_change_run = self.runs
        keep_index = self.buf.index & keep_ids
        drop_ids = self.buf.index.difference(keep_ids)
        self.ids = self.ids.difference(drop_ids)
        self.buf = self.buf.reindex(keep_index)
        self.move()


class WalkingBuffer:
    def __init__(self, csv_path, bufsize):
        self.chunk_iterator = pd.read_csv(
            csv_path, dtype=str, keep_default_na=False, chunksize=int(0.1 * bufsize), index_col="voter_id"
        )
        self.buf = pd.read_csv(csv_path, dtype=str, index_col="voter_id", nrows=0)
        self.bufsize = bufsize
        self.has_next = True
        self.fill()

    def has_data(self):
        return self.has_next or self.buf.size != 0

    def fill(self):
        logger = logging.getLogger("extract")
        logger.setLevel(logging.INFO)
        logger.info("filling walk buffer")
        if self.has_next:
            size = self.buf.shape[0]
            buf_parts = [self.buf]
            try:
                while size < self.bufsize:
                    next_part = next(self.chunk_iterator)
                    size += next_part.shape[0]
                    buf_parts.append(next_part)
            except StopIteration:
                self.has_next = False
            self.buf = pd.concat(buf_parts)

    def update(self, keep_ids):
        keep_index = self.buf.index & keep_ids
        self.buf = self.buf.reindex(keep_index)
        self.fill()


class CSVTrainExtract:
    def __init__(self, csv1_path, csv2_path, train1_path, train2_path, bufsize=1000000):
        logger = logging.getLogger("extract")
        logger.setLevel(logging.INFO)
        logger.info("bufsize is %d", bufsize)
        self.walking_buf = WalkingBuffer(csv1_path, bufsize)
        self.running_buf = RunningBuffer(csv2_path, csv1_path, bufsize)
        self.train1_path = train1_path
        self.train2_path = train2_path
        header = pd.read_csv(csv1_path, dtype=str, index_col="voter_id", nrows=0)
        header.to_csv(train1_path, mode="w", header=True)
        header.to_csv(train2_path, mode="w", header=True)

    def next_merge(self):
        logger = logging.getLogger("extract")
        logger.setLevel(logging.INFO)
        logger.info("number left for merging: %s", self.running_buf.ids.size)
        walking_index = self.walking_buf.buf.index
        running_index = self.running_buf.buf.index
        matching_ids = walking_index & running_index

        walk_matching = self.walking_buf.buf.reindex(matching_ids)
        run_matching = self.running_buf.buf.reindex(matching_ids)

        walking_keep_ids = walking_index.difference(matching_ids)
        running_keep_ids = running_index.difference(matching_ids)
        self.walking_buf.update(walking_keep_ids)
        self.running_buf.update(running_keep_ids)

        walk_excemption = walk_matching.excemption == "False"
        run_excemption = run_matching.excemption == "False"
        both_not_excemption = walk_excemption & run_excemption
        walk_matching = walk_matching[both_not_excemption]
        run_matching = run_matching[both_not_excemption]

        walk_matching_relevant = walk_matching[matching_fields]
        run_matching_relevant = run_matching[matching_fields]
        not_same = ~(walk_matching_relevant.eq(run_matching_relevant))
        not_same = not_same.any(axis=1)
        walk_matching[not_same].to_csv(self.train1_path, mode="a", header=False)
        run_matching[not_same].to_csv(self.train2_path, mode="a", header=False)

    def merge(self):
        run_buf = self.running_buf
        last_ids = None
        while (
            self.walking_buf.has_data()
            and run_buf.runs - 1 <= run_buf.last_change_run
            and not (run_buf.all_data_in_window and run_buf.ids.size == last_ids.size)
        ):
            last_ids = run_buf.ids
            self.next_merge()


def _check_lines(date1, date2):
    match1_path, match2_path = gen_path(date1, date2, suffix="_match_pre.csv")
    voter_ids_left = pd.read_csv(match1_path, usecols=["voter_ids"], dtype="str")["voter_ids"].str.split("|").explode()
    voter_ids_right = pd.read_csv(match2_path, usecols=["voter_ids"], dtype="str")["voter_ids"].str.split("|").explode()
    voter_ids_left.index.name = "left"
    voter_ids_right.index.name = "right"
    index = voter_ids_left.reset_index().merge(voter_ids_right.reset_index(), on="voter_ids").set_index(["left", "right"]).index
    return index

def _read_next(data_iter, wait_entries, add_num_changed=False):
    try:
        entries = next(data_iter)
        entries["voter_ids"] = entries["voter_ids"].str.split("|")
        if add_num_changed:
            entries["_num_changed"] = 0
        had_next = True
        if not wait_entries is None:
            wait_entries = wait_entries.append(entries)
        else:
            wait_entries = entries
    except StopIteration:
        had_next = False
    return wait_entries, had_next

def _reduce_voter_ids(left_voter_ids, right_voter_ids):
    left_ids = set(left_voter_ids)
    left_ids.difference_update(right_voter_ids)
    num_changed = len(set(right_voter_ids).intersection(right_voter_ids))
    if left_ids:
        new_ids = list(left_ids)
    else:
        new_ids = [np.random.randint(low=1_000_000_000, high=np.iinfo(np.uint32).max, dtype="uint32").astype("str")]
    return new_ids, num_changed

def clean_matches(date1, date2, match_filter_classifier, filter_different=10000):
    """
    filter matches where the voter_id is most likely not valid for linking
    """
    classifier = match_filter_classifier["clf"]
    config = match_filter_classifier["config"]
    match_pre1_path, match_pre2_path = gen_path(date1, date2, suffix="_match_pre.csv")
    classify_pre1_path, classify_pre2_path = gen_path(date1, date2, suffix="_classify_pre.csv")

    match1_path, match2_path = gen_path(date1, date2, suffix="_match.csv")
    classify1_path, classify2_path = gen_path(date1, date2, suffix="_classify.csv")

    backup_pairs = _check_lines(date1, date2)

    from app.compare import apply_metrics
    copyfile(classify_pre1_path, classify1_path)
    copyfile(classify_pre2_path, classify2_path)

    pairs = backup_pairs
    data_left_iter = pd.read_csv(match_pre1_path, keep_default_na=False, dtype="str", chunksize=1000000)
    data_right_iter = pd.read_csv(match_pre2_path, keep_default_na=False, dtype="str", chunksize=1000000)

    left_wait = None
    right_wait = None

    mode = "w"
    header = True
    while True:
        left_wait, left_had_next = _read_next(
            data_left_iter,
            left_wait
        )
        right_wait, right_had_next = _read_next(
            data_right_iter,
            right_wait,
            add_num_changed=True
        )
        if not left_had_next and not right_had_next:
            break
        pair_selector = pairs.get_level_values(0).isin(left_wait.index) & pairs.get_level_values(1).isin(right_wait.index)
        curr_pairs = pairs[pair_selector]
        pairs = pairs[~pair_selector]
        curr_left =  left_wait.loc[curr_pairs.get_level_values(0).unique()]
        curr_right = right_wait.loc[curr_pairs.get_level_values(1).unique()]
        applied_left, applied_right = config._apply(curr_left, curr_right)
        common_cols = list(set(applied_left.columns) & set(applied_right.columns))
        aligned_left = applied_left.loc[curr_pairs.get_level_values(0)]
        aligned_right = applied_right.loc[curr_pairs.get_level_values(1)]
        num_same = (aligned_left[common_cols].reset_index(drop=True) == aligned_right[common_cols].reset_index(drop=True)).sum(axis=1)
        num_same.name = "num_same"
        num_same = num_same.to_frame()
        num_same["left"] = curr_pairs.get_level_values("left").values
        num_same["right"] = curr_pairs.get_level_values("right").values
        num_same.sort_values("num_same", inplace=True, ascending=True)
        check_pairs = num_same.set_index(["left", "right"]).index[:filter_different]
        left_candidates = applied_left.loc[check_pairs.get_level_values(0).unique()]
        right_candidates = applied_right.loc[check_pairs.get_level_values(1).unique()]
        encoded = apply_metrics(check_pairs, left_candidates, right_candidates, config).values
        prediction = classifier.predict(encoded)
        non_matches = check_pairs[prediction == 0]
        for left_index, right_index in non_matches:
            if right_wait.loc[right_index, "excemption"] == "False":
                left_wait.loc[left_index, "voter_ids"], num_changed = _reduce_voter_ids(left_wait.loc[left_index, "voter_ids"], right_wait.loc[right_index, "voter_ids"])
                right_wait.loc[right_index, "_num_changed"] += num_changed

        left_keep_selector = left_wait.index.isin(pairs.get_level_values(0))
        right_keep_selector = right_wait.index.isin(pairs.get_level_values(1))

        left_write = left_wait[~left_keep_selector]
        right_write = right_wait[~right_keep_selector]

        left_wait = left_wait[left_keep_selector]
        right_wait = right_wait[right_keep_selector]

        left_has_random = left_write["voter_ids"].map(lambda l: all(int(e) >= 1_000_000_000 for e in l))
        right_all_removed = right_write["_num_changed"] >= right_write["voter_ids"].str.len()
        del right_write["_num_changed"]

        left_write["voter_ids"] = left_write["voter_ids"].str.join("|")
        right_write["voter_ids"] = right_write["voter_ids"].str.join("|")

        left_write[left_has_random].to_csv(classify1_path, mode="a", header=False, index=False)
        right_write[right_all_removed].to_csv(classify2_path, mode="a", header=False, index=False)

        left_write[~left_has_random].to_csv(match1_path, mode=mode, header=header, index=False)
        right_write[~right_all_removed].to_csv(match2_path, mode=mode, header=header, index=False)

        mode = "a"
        header = False
