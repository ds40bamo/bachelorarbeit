def process_gender(data):
    data["gender"] = data.gender.fillna("")
    data["gender"] = data.gender.replace("U", "")
    data["gender"] = data.gender.astype("category")
    return data
