def process_excemption(data):
    selector = data.excemption.isna() & (
        data.birth_date.isna() | data.registration_date.isna()
    )
    data["excemption"][selector] = "Y"
    data["excemption"] = data["excemption"] == "Y"
    return data
