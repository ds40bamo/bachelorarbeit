import csv
import logging
import sys
from pathlib import Path

import pandas as pd

from app.constants import CSVHEADER
from app.data import gen_path

from ._address import (Geocoder, process_address, process_address_addition,
                       process_city, process_city_and_zipcode, process_county,
                       process_zipcode)
from ._date import process_date
from ._excemption import process_excemption
from ._gender import process_gender
from ._name import (NameLocationCorrector, NameRearanger, NameSplitter,
                    TrigramExpander, extract_name_suffix, process_name,
                    process_name_suffix)
from ._race import process_race


def read_data(path, usecols=None, index_col=None):
    return pd.read_table(
        path,
        names=CSVHEADER,
        dtype=str,
        na_values=["*", "* "],
        usecols=usecols,
        index_col=index_col,
        keep_default_na=False,
        quoting=csv.QUOTE_NONE,
        engine="python",
    )


class ParsePipeline:
    def __init__(
        self,
        date_or_files,
        has_header=False,
        domain="localhost:7070",
        scheme="http",
    ):
        self.geocoder = Geocoder(
            domain=domain,
            scheme=scheme,
        )

        self.skip_header = 1 if has_header else 0
        if isinstance(date_or_files, str):
            extract_path = gen_path(date_or_files, raw=True)
            files = filter(lambda path: path.is_file(), extract_path.glob("*.txt"))
        elif hasattr(date_or_files, "__iter__"):
            files = date_or_files
        else:
            raise ValueError("specify date or files")

        files = sorted(files)
        for file in files:
            if not Path(file).is_file():
                raise ValueError(f"{file} is not a file")

        if not files:
            raise FileNotFoundError("no files found")

        self.logger = logging.getLogger("process_name")
        self.logger.setLevel(logging.INFO)
        self.files = files
        self._generate_statistics(files)

    def __iter__(self):
        return map(self._read_full_file, self.files)

    def __getitem__(self, index):
        return map(self._read_full_file, self.files[index])

    def __len__(self):
        return len(self.files)

    def process(self, data):
        data = self._tokenize_name_cols(data)

        self.logger.info("processing name")
        data = self.expander.apply(data)
        data = self.splitter.apply(data)
        data = self.rearanger.apply(data)
        data = self.location_corrector.apply(data)

        data = self._join_name_cols(data)

        self.logger.info("geocoding addresses")
        data = self.geocoder.geocode(data)

        return data

    def _generate_statistics(self, files):
        self.expander = TrigramExpander()
        self.splitter = NameSplitter()
        self.rearanger = NameRearanger()
        self.location_corrector = NameLocationCorrector()

        for file in files:
            data = self._read_stat_file(file)
            data = self._tokenize_name_cols(data)
            self._update(data)

        self._commit()

    def _update(self, data):
        self.expander.update(data)
        self.splitter.update(data)
        self.rearanger.update(data)
        self.location_corrector.update(data)

    def _commit(self):
        self.splitter.commit()
        self.rearanger.commit()
        self.location_corrector.commit()

    @staticmethod
    def _join_name_cols(data):
        for column in ["first_name", "middle_name", "last_name"]:
            data[column] = data[column].str.join("|")
        return data

    @classmethod
    def _tokenize_name_cols(cls, data):
        for column in ["first_name", "middle_name", "last_name"]:
            data[column] = data[column].map(cls._tokenize)
        return data

    @staticmethod
    def _tokenize(document):
        return list(map(sys.intern, filter(lambda x: x != "", document.split(" "))))

    def _read_full_file(self, file):
        self.logger.info("reading and processing full file %s", file)

        data = read_data(file, index_col="voter_id")

        data = data.fillna("")

        data["address"] = process_address(data.address)
        data["mail_address1"] = process_address(data.mail_address1)
        data["mail_address2"] = process_address(data.mail_address2)
        data["mail_address3"] = process_address(data.mail_address3)
        data["mail_country"] = process_address(data.mail_country)
        data = process_city_and_zipcode(data, "city", "zipcode")
        data["city"] = process_city(data.mail_city)
        zipcode, zip4 = process_zipcode(data.mail_zipcode)
        data["mail_zipcode"] = zipcode
        data["mail_zip4"] = zip4
        data["county"] = process_county(data.county)
        data["address_addition"] = process_address_addition(data.address_addition)

        data["birth_date"] = process_date(data.birth_date)
        data["registration_date"] = process_date(data.registration_date)
        data = process_excemption(data)
        data = process_race(data)

        data["first_name"] = process_name(data.first_name)
        data["middle_name"] = process_name(data.middle_name)
        data["last_name"] = process_name(data.last_name)
        data = process_gender(data)
        data = extract_name_suffix(data)
        data = process_name_suffix(data)

        del data["state"]

        return data

    def _read_stat_file(self, file):
        self.logger.info("generating statistics from file %s", file)

        data = read_data(
            file, usecols=["first_name", "middle_name", "last_name", "gender"]
        )

        data["first_name"] = process_name(data.first_name)
        data["middle_name"] = process_name(data.middle_name)
        data["last_name"] = process_name(data.last_name)
        data = process_gender(data)

        return data


def parse(date, domain="localhost:7070", scheme="http"):
    dest_path = gen_path(date, filename="parsed.csv")

    mode = "w"
    header = True
    pipeline = ParsePipeline(date, domain=domain, scheme=scheme)
    for data in pipeline:
        data = pipeline.process(data)
        data.to_csv(dest_path, mode=mode, header=header)
        mode = "a"
        header = False
