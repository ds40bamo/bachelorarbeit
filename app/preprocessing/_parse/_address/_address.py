def process_address(data):
    data = data.fillna("")
    data = data.str.lower()
    data = data.str.replace("[^a-zA-Z0-9]", " ")
    data = data.str.replace(r"\s+", " ")
    data = data.str.strip()
    data[data.str.contains(r"\bno\b.*\bresidence\b|\bno\b.*\bcity\b")] = ""
    return data


def process_address_addition(data):
    data = data.fillna("")
    data = data.str.lower()
    data = data.str.replace(r"\s+", " ")
    data = data.str.strip()
    return data


def process_country(data):
    data = data.fillna("")
    data = data.str.lower()
    data = data.str.replace(r"\s+", " ")
    data = data.str.strip()
    return data
