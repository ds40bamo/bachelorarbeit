import numpy as np
import pandas as pd
import pgeocode
import uszipcode

ABBREVIATIONS = {
    r"mt": "mount",
    r"st": "saint",
    r"ft": "fort",
    r"bch": "beach",
    r"hbr": "harbour",
    r"pt": "port",
    r"spr?gs": "springs",
    r"lk": "lake",
    r"est": "estates",
    r"plm": "palm",
    r"afb": "air force base",
    r"grn": "green",
    r"cv": "cove",
    r"n": "north",
    r"hgts": "heights",
    r"mm": "",
    r"-?unknown": "",
}


def _expand_city_abbreviations(data, city_col):
    """expand common abbreviation in city like mt -> mount
    """
    expression = r"\b(?:" + "|".join(ABBREVIATIONS.keys()) + r")\b"
    city = data[city_col][data[city_col].str.contains(expression)]
    for abbr, expansion in ABBREVIATIONS.items():
        city = city.str.replace(r"\b" + abbr + r"\b", expansion)
    data.city.update(city)
    data[city_col] = data[city_col].str.replace(r"[.0-9]", "")
    data[city_col] = data[city_col].str.split("/").str[0]
    data[city_col] = data[city_col].str.replace(r"\s+", " ")
    data[city_col] = data[city_col].str.strip()
    return data


DIFFER_SET1 = {"1", "7", "4", "9"}
DIFFER_SET2 = {"3", "8", "9", "0"}
DIFFER_SET3 = {"6", "5", "8"}
DIFFER_SET4 = {"6", "0"}
DIFFER_SET5 = {"6", "4"}
DIFFER_SET6 = {"3", "2", "9"}
DIFFER_SET7 = {"1", "2", "7"}


def _correct_zipcode(row, zipcode_col):
    zipcode = row[zipcode_col]
    candidates = row["zipcode_candidates"]
    if len(candidates) == 1:
        return candidates[0]

    # only differs in optical similar digits
    ocr_zipcodes = []
    for candidate in candidates:
        number_differed = 0
        for left, right in zip(list(zipcode), list(candidate)):
            if left != right:
                if (
                    (left in DIFFER_SET1 and right in DIFFER_SET1)
                    or (left in DIFFER_SET2 and right in DIFFER_SET2)
                    or (left in DIFFER_SET3 and right in DIFFER_SET3)
                    or (left in DIFFER_SET4 and right in DIFFER_SET4)
                    or (left in DIFFER_SET5 and right in DIFFER_SET5)
                    or (left in DIFFER_SET6 and right in DIFFER_SET6)
                    or (left in DIFFER_SET7 and right in DIFFER_SET7)
                ):
                    number_differed += 1
                else:
                    break
        else:
            ocr_zipcodes.append((candidate, number_differed))

    # only one optical similar digit
    ocr_zipcodes = sorted(ocr_zipcodes, key=lambda x: x[1])
    if len(ocr_zipcodes) > 0 and ocr_zipcodes[0][0] == 1:
        if len(ocr_zipcodes) == 1 or ocr_zipcodes[0][1] != ocr_zipcodes[1][1]:
            return ocr_zipcodes[0][0]

    # same digits but in different order
    for candidate in candidates:
        if sorted(list(zipcode)) == sorted(list(candidate)):
            return candidate

    # only differs in one digit
    found_zipcodes = []
    for candidate in candidates:
        one_differed = False
        for left, right in zip(list(zipcode), list(candidate)):
            if left != right:
                if one_differed:
                    break
                else:
                    one_differed = True
        else:
            found_zipcodes.append(candidate)
    if len(found_zipcodes) == 1:
        return found_zipcodes[0]

    # arbitrary number of optical similar digit
    if len(ocr_zipcodes) > 0:
        if len(ocr_zipcodes) == 1 or ocr_zipcodes[0][1] != ocr_zipcodes[1][1]:
            return ocr_zipcodes[0][0]

    return np.nan


def _apply_gazetter(data, city_col, zipcode_col):
    """replace city with guess if city is prefix of guess
    also replace empty city with guess
    """
    data[city_col] = data[city_col].replace("rpb", "royal palm beach")

    gazetteer = pgeocode.Nominatim("us")
    cityzip = data[[city_col, zipcode_col]].dropna(subset=[zipcode_col])
    cityzip.columns = ["city", "zipcode"]
    lookup = gazetteer.query_postal_code(cityzip.zipcode.drop_duplicates().values)
    lookup = lookup[["postal_code", "place_name", "state_code"]]
    cityzip = pd.merge(
        cityzip.reset_index(), lookup, left_on="zipcode", right_on="postal_code"
    ).set_index("voter_id")
    cityzip["place_name"] = cityzip.place_name.str.lower()
    del cityzip["postal_code"]

    # replace city with guess if city is prefix of guess (also replace empty city with guess)
    # replace obvious akronym of city with expansion
    tmp = cityzip.loc[cityzip.city != cityzip.place_name, ["city", "place_name"]]
    tmp = tmp.dropna(subset=["place_name"])
    unique = tmp.drop_duplicates()
    unique = unique[
        unique.apply(lambda s: s["place_name"].startswith(s["city"]), axis=1)
        & (unique.city == unique.place_name.str.replace(r"(\w)\w*( |$)", r"\1"))
    ]
    tmp = (
        tmp.reset_index().merge(unique, on=["city", "place_name"]).set_index("voter_id")
    )
    cityzip.city.update(tmp.place_name)

    # correct zipcodes by known mistakes
    tmp = cityzip.loc[cityzip.state_code != "FL", ["city", "zipcode"]]
    unique = tmp.drop_duplicates().copy()
    # retrieve all zipcodes associated with city
    with uszipcode.SearchEngine() as search:
        unique["zipcode_candidates"] = unique.city.apply(
            lambda x: list(
                map(
                    lambda y: str(y.zipcode),
                    search.by_city_and_state(x, "florida", returns=None) if x != "" else [],
                )
            )
        )
    cand = {
        "lake hamilton": ["33851"],
        "highland city": ["33812"],
        "sugarloaf": ["33042", "33044"]
    }
    for city, zipcodes in cand.items():
        num_entries = (unique.city == city).sum()
        unique.loc[unique.city == city, "zipcode_candidates"] = [zipcodes] * num_entries
    tmp = pd.merge(tmp.reset_index(), unique, on=["city", "zipcode"]).set_index(
        "voter_id"
    )
    corrected_zipcode = tmp.apply(lambda x: _correct_zipcode(x, zipcode_col), axis=1)
    if corrected_zipcode.shape[0] != 0:
        cityzip.zipcode.update(corrected_zipcode)

    data[city_col].update(cityzip["city"])
    data[zipcode_col].update(cityzip["zipcode"])
    return data


def process_zipcode(zipcode):
    zipcode = zipcode.fillna("")
    zipcode = zipcode.str.lower()
    zipcode = zipcode.str.replace(r"\s+", " ")
    zipcode = zipcode.str.replace(r"[^ a-z0-9]", "")
    zipcode = zipcode.str.strip()
    zipcode, zip4 = zipcode.str[:5], zipcode.str[5:]
    zipcode[zipcode.str.contains("99999")] = ""
    return zipcode, zip4


def process_city(city):
    city = city.fillna("")
    city = city.str.lower()
    city = city.str.replace(r"\s+", " ")
    city = city.str.replace(r"[^ a-z0-9]", "")
    city = city.str.strip()
    city[city.str.contains(r"\bno\b.*\bresidence\b|\bno\b.*\bcity\b")] = ""
    return city


def process_city_and_zipcode(data, city_col, zipcode_col):
    zipcode, zip4 = process_zipcode(data[zipcode_col])
    data[zipcode_col] = zipcode
    zip4_col = zipcode_col.replace("zipcode", "zip4")
    if zip4_col == zipcode_col:
        zip4_col = zipcode_col + "_zip4"
    data[zip4_col] = zip4
    data[city_col] = process_city(data[city_col])
    data = _expand_city_abbreviations(data, city_col)
    data = _apply_gazetter(data, city_col, zipcode_col)
    return data
