from functools import reduce
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool

import numpy as np
import pandas as pd

import geopy


class Nominatim(object):
    def __init__(
        self, domain="localhost:7070", scheme="http"
    ):
        self.nominatim = geopy.Nominatim(domain=domain, scheme=scheme, timeout=60)
        self.nominatim.geocode("")

    def _geocode_address(self, address):
        result = self.nominatim.geocode(address)
        if result is not None:
            return (result.longitude, result.latitude)
        else:
            return (np.nan, np.nan)

    def _geocode_addresses(self, addresses):
        with ThreadPool(cpu_count()) as pool:
            return pool.map(self._geocode_address, addresses)

    def geocode(self, address):
        index = None
        if isinstance(address, str):
            return self.nominatim.geocode(address)
        elif isinstance(address, pd.Series):
            index = address.index
            address = address.values

        if hasattr(address, "__iter__"):
            addresses = address
        else:
            raise ValueError(f"can't handle type of address: {type(address)}")

        geocoded_addresses = self._geocode_addresses(addresses)
        if index is not None:
            return pd.Series(geocoded_addresses, index=index)
        else:
            return geocoded_addresses


class Geocoder(object):
    LONG_LAT = ["longitude", "latitude"]

    def __init__(
        self, domain="localhost:7070", scheme="http"
    ):
        try:
            self.nominatim = Nominatim(domain, scheme)
        except geopy.exc.GeocoderServiceError:
            raise geopy.exc.GeocoderServiceError("geocode server not found, start the geocoder")

    @staticmethod
    def _street_name_only(series):
        return series.str.replace(r"^ *[0-9]* *", "").str.strip()

    @staticmethod
    def _extract_zipcode(series):
        return series.str.replace(r"^(?:.*?)(?:(?:\b|\D)(\d{5,5})(?:\b|\D).*)?$", r"\1")

    @staticmethod
    def _map_to_column(info, columns):
        return [info[field] for field in columns if field is not None]

    @classmethod
    def _generate_address_string(cls, info, city_col, street_col, zipcode_col):
        first_comp = cls._map_to_column(info, [street_col, city_col])
        last_comp = cls._map_to_column(info, [zipcode_col])
        address_line = first_comp + ["florida"] + last_comp
        address_line = reduce(lambda x, y: x + ", " + y, address_line)
        return address_line

    def _geocode(self, info, city_col, street_col, zipcode_col):
        fields = [city_col, street_col, zipcode_col]
        fields = [field for field in fields if field is not None]
        unique = info[fields].drop_duplicates()
        address_line = self._generate_address_string(
            unique, city_col, street_col, zipcode_col
        )
        location = self.nominatim.geocode(address_line)
        location = pd.DataFrame(location.to_list(), index=location.index)
        location.columns = self.LONG_LAT
        unique = pd.concat([unique, location], axis=1)
        index_name = info.index.name
        info = (
            info.reset_index()
            .merge(unique, on=fields, how="left")
            .set_index(index_name)
        )
        return info

    def _update(self, data, info):
        is_valid_long_lat = ~info[self.LONG_LAT].isna().any(axis=1)
        data.update(info.loc[is_valid_long_lat, self.LONG_LAT])
        info = info[~is_valid_long_lat]
        info = info.drop(columns=self.LONG_LAT, errors="ignore")
        return data, info

    def _geocode_on_fields(
        self, data, info, city_col=None, street_col=None, zipcode_col=None
    ):
        if len(info) == 0:
            return data, info

        info = self._geocode(info, city_col, street_col, zipcode_col)
        data, info = self._update(data, info)
        return data, info

    def _geocode_on_county(self, data, info, county_col):
        if len(info) == 0:
            return data, info

        unique = info[county_col].drop_duplicates()
        address_line = unique + " county, florida"
        location = self.nominatim.geocode(address_line)
        location = pd.DataFrame(location.to_list(), index=location.index)
        location.columns = self.LONG_LAT
        unique = pd.concat([unique, location], axis=1)
        index_name = info.index.name
        info = (
            info.reset_index()
            .merge(unique, on=county_col, how="left")
            .set_index(index_name)
        )
        data, info = self._update(data, info)
        return data, info

    def _geocode_florida(self, data, info):
        if len(info) == 0:
            return data, info

        location = self.nominatim.geocode("florida")
        info["longitude"] = location.longitude
        info["latitude"] = location.latitude
        data, info = self._update(data, info)
        return data, info

    def _geocode_zipcode(self, data, info, zipfield):
        if len(info) == 0:
            return data, info
        unique = info[zipfield].drop_duplicates()
        unique = unique[unique != ""]
        address_line =  "florida, " + unique
        location = self.nominatim.geocode(address_line)
        location = pd.DataFrame(location.to_list(), index=location.index)
        location.columns = self.LONG_LAT
        unique = pd.concat([unique, location], axis=1)
        index_name = info.index.name
        info = (
            info.reset_index()
            .merge(unique, on=zipfield, how="left")
            .set_index(index_name)
        )
        data, info = self._update(data, info)
        return data, info

    def geocode(self, data):
        old_index = data.index
        data = data.drop(columns=self.LONG_LAT, errors="ignore")
        data = data.reindex(columns=data.columns.append(pd.Index(self.LONG_LAT)))
        info = data[["city", "zipcode", "address", "address_addition", "county"]]
        info = info.astype(str)
        info = info.fillna("")
        info["street"] = self._street_name_only(info.address)
        info["alt_zipcode_address"] = self._extract_zipcode(info.address)
        info["alt_zipcode_addition"] = self._extract_zipcode(info.address_addition)
        info.loc[info.zipcode == "", "zipcode"] = info["alt_zipcode_address"]
        info.loc[info.zipcode == "", "zipcode"] = info["alt_zipcode_addition"]

        del info["address"]
        del info["address_addition"]

        has_nothing = (info[["county", "street", "city", "zipcode"]] == "").all(axis=1)
        nothing = info[has_nothing]
        info = info[~has_nothing]

        has_only_county = (info[["street", "city", "zipcode"]] == "").all(axis=1)
        only_county = info[has_only_county]
        info = info[~has_only_county]

        # avoid records with only zipcode getting classified by nominatim
        has_only_zipcode = (info[["street", "city"]] == "").all(axis=1)
        only_zipcode = info[has_only_zipcode]
        info = info[~has_only_zipcode]

        # try all components together
        data, info = self._geocode_on_fields(
            data, info, city_col="city", zipcode_col="zipcode", street_col="street"
        )

        # try zipcode-like information in address
        data, info = self._geocode_on_fields(
            data,
            info,
            city_col="city",
            zipcode_col="alt_zipcode_address",
            street_col="street",
        )

        # try zipcode-like information in address_addition
        data, info = self._geocode_on_fields(
            data,
            info,
            city_col="city",
            zipcode_col="alt_zipcode_addition",
            street_col="street",
        )

        # try without city
        data, info = self._geocode_on_fields(
            data, info, zipcode_col="zipcode", street_col="street"
        )
        data, info = self._geocode_on_fields(
            data, info, zipcode_col="alt_zipcode_address", street_col="street"
        )
        data, info = self._geocode_on_fields(
            data, info, zipcode_col="alt_zipcode_addition", street_col="street"
        )

        # try without zipcode
        data, info = self._geocode_on_fields(
            data, info, city_col="city", street_col="street"
        )

        # try zipcode only
        data, only_zipcode = self._geocode_zipcode(data, only_zipcode, "zipcode")
        data, only_zipcode = self._geocode_zipcode(
            data, only_zipcode, "alt_zipcode_addition"
        )
        data, only_zipcode = self._geocode_zipcode(
            data, only_zipcode, "alt_zipcode_address"
        )
        data, info = self._geocode_zipcode(data, info, "zipcode")
        data, info = self._geocode_zipcode(data, info, "alt_zipcode_addition")
        data, info = self._geocode_zipcode(data, info, "alt_zipcode_address")

        # try city only
        data, info = self._geocode_on_fields(data, info, city_col="city")

        # try county only
        data, info = self._geocode_on_county(data, info, county_col="county")
        data, only_zipcode = self._geocode_on_county(
            data, only_zipcode, county_col="county"
        )
        data, only_county = self._geocode_on_county(
            data, only_county, county_col="county"
        )
        data, only_zipcode = self._geocode_florida(data, nothing)
        data, only_county = self._geocode_florida(data, nothing)
        data, nothing = self._geocode_florida(data, nothing)
        assert len(info) == 0
        assert len(only_zipcode) == 0
        assert (data.index == old_index).all()
        return data
