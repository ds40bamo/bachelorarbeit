def process_date(data):
    data = data.str.replace(r"(\d\d/\d\d)/[013-9]0(\d\d)", r"\1/20\2")
    data = data.str.replace(r"(\d\d/\d\d)/[03-9]([1-9]\d\d)", r"\1/1\2")
    # invalid dates have to be in a certain range
    # of 500 years so they can be parsed by pandas
    data = data.str.replace(r"(\d\d/\d\d)/[013-9][0-8][0-9]{2,}", r"\1/1700")
    data = data.str.replace(r"(\d\d/\d\d)/2[1-9][0-9]{2,}", r"\1/1700")
    return data
