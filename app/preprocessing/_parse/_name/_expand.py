from collections import defaultdict
from itertools import chain, starmap

import nltk
from pygtrie import CharTrie


class TrigramExpander:
    def __init__(self, count_threshold=2, stop_words={"ann", "le", "la"}):
        self.trie = CharTrie()
        self.count_threshold = count_threshold
        self.stop_words = stop_words

    def update(self, data):
        info = data[(data.first_name.str.len() + data.middle_name.str.len()) > 2]
        fn_tokens = info.first_name
        mn_tokens = info.middle_name
        tokens = fn_tokens + mn_tokens
        grams = chain(*map(lambda x: nltk.ngrams(x, 3), tokens.values))
        grams = filter(lambda x: len(x[0]) > 1 and len(x[1]) > 1, grams)
        grams = map(lambda x: " ".join(x), grams)
        for gram in grams:
            try:
                self.trie[gram] += 1
            except KeyError:
                self.trie[gram] = 1

    def apply(self, data):
        """if a trigram is a prefix of other trigrams, which are all prefixes of each other
        than replace the trigram with the longest of these trigrams
        """
        info = data[(data.first_name.str.len() + data.middle_name.str.len()) > 2].copy()
        fn_tokens = info.first_name
        mn_tokens = info.middle_name
        tokens = fn_tokens + mn_tokens
        grams = chain(*map(lambda x: nltk.ngrams(x, 3), tokens.values))
        grams = filter(lambda x: len(x[0]) > 1 and len(x[1]) > 1, grams)
        grams = map(lambda x: " ".join(x), grams)
        gram_translator = defaultdict(lambda: defaultdict(lambda: dict()))
        for gram in grams:
            token1, token2, token3 = gram.split(" ")
            if token3 in self.stop_words:
                continue
            try:
                items = self.trie.iteritems(gram)
                items = filter(lambda x: x[1] > self.count_threshold, items)
                items = list(items)
                keys = map(lambda x: x[0], items)
                keys = list(keys)
                if len(keys) == 1 and gram in keys:
                    continue
                keys = map(lambda x: x.split(" ")[-1], keys)
                keys = sorted(keys)
                keys_pairs = nltk.ngrams(keys, 2)
                key_pairs = list(keys_pairs)
                if all(starmap(lambda x, y: y.startswith(x), key_pairs)) and all(
                    map(lambda x: x not in self.stop_words, keys)
                ):
                    keys = filter(lambda x: x[1] > 3, items)
                    keys = map(lambda x: x[0], keys)
                    keys = filter(lambda x: not x.endswith(" " + token3), keys)
                    keys = list(keys)
                    if len(keys) == 0:
                        continue
                    correction = max(keys, key=lambda x: len(x)).split(" ")[-1]
                    gram_translator[token1][token2][token3] = correction
            except KeyError:
                pass
        tokens = list(
            starmap(
                lambda fnt, mnt: self._expand(fnt, mnt, gram_translator, gram_size=3),
                zip(fn_tokens.values, mn_tokens.values),
            )
        )
        first_name = list(map(lambda x: x[0], tokens))
        info["first_name"] = first_name
        data.first_name.update(info.first_name)

        middle_name = list(map(lambda x: x[1], tokens))
        info["middle_name"] = middle_name
        data.middle_name.update(info.middle_name)

        return data

    @staticmethod
    def _expand(fn_tokens, mn_tokens, translator, gram_size):
        first_name = []
        middle_name = []

        window_size = gram_size - 1
        window = []

        iterator = chain(
            map(lambda x: (x, True), fn_tokens), map(lambda x: (x, False), mn_tokens)
        )

        for token, is_first in iterator:
            if token == "":
                continue
            elif len(window) < window_size:
                window.append(token)
            else:
                lookup = translator
                for prev_token in window:
                    tmp = lookup.get(prev_token)
                    if tmp is None:
                        break
                    lookup = tmp
                else:
                    tmp = lookup.get(token)
                    if tmp is not None:
                        token = tmp
                window = window[1:] + [token]
            if is_first:
                first_name.append(token)
            else:
                middle_name.append(token)

        return first_name, middle_name
