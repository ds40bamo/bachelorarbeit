import math

import nltk
import pandas as pd
from nltk.probability import FreqDist
from nltk.util import ngrams


class Collocator:
    MEASURE = nltk.collocations.BigramAssocMeasures.pmi

    def __init__(self):
        self.wfd = FreqDist()
        self.bfd = FreqDist()

    def update(self, documents):
        for document in documents:
            for token in document:
                self.wfd[token] += 1
            for token1, token2 in ngrams(document, 2):
                self.bfd[(token1, token2)] += 1

    def collocations(
        self, threshold=None, keywords=set(), min_second_component_length=2,
    ):
        collocator = nltk.collocations.BigramCollocationFinder(self.wfd, self.bfd)
        collocator.apply_freq_filter(math.log10(self.wfd.N()))
        bigrams = collocator.score_ngrams(self.MEASURE)
        if threshold is not None:
            bigrams = filter(lambda x: x[1] > threshold or x[0][0] in keywords, bigrams)
        bigrams = map(lambda x: x[0], bigrams)
        bigrams = filter(lambda x: len(x[1]) >= min_second_component_length, bigrams)
        bigrams = set(bigrams)
        return bigrams


class NameCollocator(Collocator):
    LAST_NAME_KEYWORDS = {
        "da",
        "de",
        "del",
        "dela",
        "della",
        "delos",
        "den",
        "der",
        "des",
        "di",
        "dos",
        "du",
        "la",
        "le",
        "los",
        "mac",
        "mc",
        "saint",
        "st",
        "van",
        "vanden",
        "vander",
        "von",
    }

    FIRST_NAME_KEYWORDS = {
        "da",
        "de",
        "del",
        "dela",
        "la",
        "le",
        "los",
        "mac",
        "mc",
        "saint",
        "st",
        "van",
    }

    def __init__(self):
        super().__init__()

    def first_name_bigrams(self, threshold=8):
        return self.collocations(threshold=threshold, keywords=self.FIRST_NAME_KEYWORDS)

    def last_name_bigrams(self, threshold=3):
        return self.collocations(threshold=threshold, keywords=self.LAST_NAME_KEYWORDS)
