from collections import Counter
from itertools import chain

import numpy as np
import pandas as pd
from symspellpy import SymSpell


class NameSplitter:
    """split names like "mariajessica" -> "maria jessica"
    for name greater than the 0.975 score quantile perform
    the split
    """

    def __init__(self):
        self.counter = Counter()

    def update(self, data):
        tokens = data.first_name + data.middle_name + data.last_name
        token_count = tokens.explode().value_counts()
        for token, count in zip(token_count.index.values, token_count.values):
            self.counter[token] += count

    def commit(self):
        spell = SymSpell(max_dictionary_edit_distance=0)
        self.nominator = 2 * np.log10(sum(self.counter.values()))
        candidate = self.counter.items()
        candidate = filter(lambda x: len(x[0]) > 2, candidate)
        candidate = filter(lambda x: x[1] > 4, candidate)
        for token, count in candidate:
            spell.create_dictionary_entry(token, count)
        self.spell = spell

        dirty = self.counter.items()
        dirty = filter(lambda x: len(x[0]) > 2, dirty)
        dirty = filter(lambda x: x[1] <= 4, dirty)
        dirty = map(lambda x: x[0], dirty)

        corrections = []
        for token in dirty:
            if " " not in token:
                candidates = spell.lookup_compound(token, max_edit_distance=0)
                candidates = map(lambda x: x.term, candidates)
                candidates = filter(lambda x: " " in x, candidates)
                candidates = map(lambda x: (x, self._compute_score(x)), candidates)
                candidates = list(candidates)
                if len(candidates) > 0:
                    correction = max(candidates, key=lambda x: x[1])
                    corrections.append(correction)

        correct_info = pd.DataFrame(
            corrections,
            index=map(lambda x: x[0].replace(" ", ""), corrections),
            columns=["correction", "score"],
        )
        correct_info["correction"] = correct_info.correction.map(
            lambda x: tuple(x.split(" "))
        )
        quantile = correct_info.score.quantile(0.975)
        update_info = correct_info[correct_info.score >= quantile]
        self.update_info = dict(zip(update_info.index, update_info.correction))
        del update_info

        for column in correct_info:
            del correct_info[column]
        del correct_info

    def apply(self, data):
        for column in ["first_name", "middle_name", "last_name"]:
            tmp = data[column].map(
                lambda x: list(
                    chain.from_iterable(map(lambda y: self.update_info.get(y, (y,)), x))
                )
            )
            del data[column]
            data[column] = tmp
        return data

    def _compute_score(self, name):
        component1, component2 = name.split(" ")
        count1 = self.counter.get(component1, 1)
        count2 = self.counter.get(component2, 1)
        return np.log10(count1) + np.log10(count2) - self.nominator
