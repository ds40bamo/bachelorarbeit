from collections import defaultdict
from itertools import chain, starmap

import numpy as np
import pandas as pd

from ._collocations import NameCollocator


class NameRearanger:
    """reorder name tokens to other column, in some cases

    Example
    -------
    first_name = []         -> ["maria"]
    middle_name = ["maria"] -> []
    last_name = ["smith"]   -> ["smith"]

    first_name = []                -> ["maria"]
    middle_name = ["maria", "ann"] -> ["ann"]
    last_name = []                 -> []


    first_name = []                -> ["maria"]
    middle_name = ["maria", "smith"] -> ["smith"]
    last_name = []                 -> []
    """

    def __init__(self):
        self.first_name_collocator = NameCollocator()
        self.last_name_collocator = NameCollocator()

    def update(self, data):
        fn_documents = data.first_name + data.middle_name
        ln_documents = data.last_name
        self.first_name_collocator.update(fn_documents)
        self.last_name_collocator.update(ln_documents)

    def commit(self):
        bigrams = (
            self.first_name_collocator.first_name_bigrams()
            | self.last_name_collocator.last_name_bigrams()
        )
        tmp = bigrams
        bigrams = defaultdict(set)
        for token1, token2 in tmp:
            bigrams[token1].add(token2)
        self.bigrams = bigrams

        bigram_splitter = {
            token1 + token2: (token1, token2)
            for token1, tokens2 in bigrams.items()
            for token2 in tokens2
        }
        self.bigram_splitter = bigram_splitter

    def apply(self, data):
        fn_tokens = data.first_name + data.middle_name
        ln_tokens = data.last_name

        tokens = zip(fn_tokens.values, ln_tokens.values)
        tokens = starmap(lambda fnt, lnt: self._merge_tokens(fnt, lnt), tokens)
        tokens = list(tokens)

        data["first_name"] = list(map(lambda x: x[0], tokens))
        data["middle_name"] = list(map(lambda x: x[1], tokens))
        data["last_name"] = list(map(lambda x: x[2], tokens))
        return data

    def _merge_tokens(
        self, fn_tokens, ln_tokens, replace_keywords={"st": "saint", "mac": "mc"}
    ):
        """split tokens that occur in bigrams like "vandyke" -> "van dyke"
        group tokens that occur in bigrams like ["mike", "van", "dyke"] -> ["mike", "van dyke"]
        
        Parameters
        ----------
        fn_tokens : iterable of strings
            first name and middle name tokens together
        ln_tokens : iterable of strings
            last name tokens
        bigrams : dict[str, set[str]]
            strings that appear as one string separated by " "
        """
        bigrams = self.bigrams
        bigram_splitter = self.bigram_splitter

        current_token_chain = None
        current_chain_is_first = None
        last_token = None

        first_name = []
        middle_name = []
        last_name = []

        fn_tokens = chain.from_iterable(
            map(lambda x: bigram_splitter.get(x, (x,)), fn_tokens)
        )
        ln_tokens = chain.from_iterable(
            map(lambda x: bigram_splitter.get(x, (x,)), ln_tokens)
        )

        iterator = chain(
            map(lambda x: (x, True), fn_tokens), map(lambda x: (x, False), ln_tokens)
        )

        for token, is_first in iterator:
            if token in replace_keywords:
                token = replace_keywords[token]
            if token == "":
                continue
            if last_token is not None:
                if last_token in bigrams and token in bigrams[last_token]:
                    current_token_chain.append(token)
                else:
                    name_part = " ".join(current_token_chain)
                    if current_chain_is_first:
                        if len(first_name) == 0:
                            first_name = [name_part]
                        else:
                            middle_name.append(name_part)
                    else:
                        last_name.append(name_part)
                    current_token_chain = [token]
            else:
                current_token_chain = [token]
            last_token = token
            current_chain_is_first = is_first

        if current_token_chain is not None:
            name_part = " ".join(current_token_chain)
            if current_chain_is_first:
                if len(first_name) == 0:
                    first_name = [name_part]
                else:
                    middle_name.append(name_part)
            else:
                last_name.append(name_part)

        return first_name, middle_name, last_name


class GenderInfo:
    def __init__(self):
        self.gender_info = defaultdict(lambda: [0, 0, 0])

    def update(self, documents, gender):
        if len(documents) != len(gender):
            raise ValueError("length of documents has to equal length of gender")
        gender_info = self.gender_info
        for tokens, gender in zip(documents, gender):
            gender = gender.lower()
            for token in tokens:
                info = gender_info[token]
                info[0] += 1
                if gender == "m":
                    info[1] += 1
                if gender == "f":
                    info[2] += 1

    def generate_info(self, measures=["balance"]):
        items = self.gender_info.items()
        names, info = zip(*items)
        stats = map(lambda x: self._generate_statistics(x, measures), info)
        info = pd.DataFrame(stats, columns=["count_"] + measures, index=names)
        info.columns.name = None
        return info

    @staticmethod
    def _generate_statistics(counts, measures):
        count, m, f = counts
        info = [count]

        for measure in measures:
            try:
                if measure == "balance":
                    info.append(abs(m - f) / (f + m))
                elif measure == "prob":
                    info.append(m / (f + m))
                else:
                    raise ValueError(f"invalid measure {measure}")
            except ZeroDivisionError:
                info.append(np.nan)
        return info


class NameLocationCorrector:
    def __init__(self):
        self.first_name_gender_info = GenderInfo()
        self.last_name_gender_info = GenderInfo()
        self.full_name_gender_info = GenderInfo()

    def update(self, data):
        gender = data.gender

        first_name_tokens = data.first_name + data.middle_name
        last_name_tokens = data.last_name
        full_name_tokens = first_name_tokens + last_name_tokens

        self.first_name_gender_info.update(first_name_tokens, gender)
        self.last_name_gender_info.update(last_name_tokens, gender)
        self.full_name_gender_info.update(full_name_tokens, gender)

    def commit(self):
        self.first_name_gender_stats = self.first_name_gender_info.generate_info(
            measures=["balance", "prob"]
        )
        self.last_name_gender_stats = self.last_name_gender_info.generate_info(
            measures=["balance"]
        )
        self.full_name_gender_stats = self.full_name_gender_info.generate_info(
            measures=["balance"]
        )

        self.first_name_threshold = self._get_balance_threshold(
            self.first_name_gender_stats, quantile=0.1
        )
        self.last_name_threshold = self._get_balance_threshold(
            self.last_name_gender_stats, quantile=0.95
        )


    def apply(self, data):
        """swap first name and last name or middle name and last name,
        when there is evidence, that the first/middle name is actually
        a last name and last name is actually a first/middle name
        """

        data = self._swap_fn_ln(data, "first_name", "last_name", 0.02)
        data = self._swap_fn_ln(data, "middle_name", "last_name", 0.15)

        return data

    @staticmethod
    def _get_balance_threshold(gender_stats, quantile):
        gender_stats = gender_stats[
            (gender_stats.count_ > np.log10(gender_stats.count_.sum()))
            & (gender_stats.index.str.len() > 1)
        ]
        threshold = gender_stats.balance.quantile(quantile)
        return threshold

    def _swap_fn_ln(
        self,
        data,
        first_name_col,
        last_name_col,
        ratio_threshold,
    ):
        # fragwürdiger name muss in entgegengesetzter spalte häufig sein
        # hypothese: name folgt der Verteilung der Spalte in der er gefunden wurde
        # es darf nicht beides plausibler vorname sein
        # der neue name darf nicht mit dem Geschlecht in Konflikt stehen

        info = data[
            (data[first_name_col].str.len() == 1) & (data[last_name_col].str.len() == 1)
        ]
        info = pd.DataFrame(
            {
                "first_name": info[first_name_col].str[0],
                "last_name": info[last_name_col].str[0],
                "gender": info.gender,
            }
        )

        info = info[info.first_name.str.len() > 2]
        info = info[info.last_name.str.len() > 2]

        unique = self._unique_stats(info)

        info = self._swap_name(info, unique, ratio_threshold)

        data[first_name_col].update(info.first_name.map(lambda x: [x]))
        data[last_name_col].update(info.last_name.map(lambda x: [x]))

        return data

    def _unique_stats(self, info):
        fn_count = self.first_name_gender_stats["count_"]
        ln_count = self.last_name_gender_stats["count_"]

        unique = info[["first_name", "last_name"]].drop_duplicates(ignore_index=True)
        unique = pd.merge(
            unique, fn_count, left_on="first_name", right_index=True, how="left"
        )
        unique = unique.rename(columns={"count_": "fn_fn_count"})
        unique = pd.merge(
            unique, ln_count, left_on="last_name", right_index=True, how="left"
        )
        unique = unique.rename(columns={"count_": "ln_ln_count"})
        unique = pd.merge(
            unique, ln_count, left_on="first_name", right_index=True, how="left"
        )
        unique = unique.rename(columns={"count_": "fn_ln_count"})
        unique = pd.merge(
            unique, fn_count, left_on="last_name", right_index=True, how="left"
        )
        unique = unique.rename(columns={"count_": "ln_fn_count"})
        unique = unique.fillna(0.5)
        unique["ratio"] = (unique.fn_fn_count * unique.ln_ln_count) / (
            unique.fn_ln_count * unique.ln_fn_count
        )

        return unique[["first_name", "last_name", "ratio"]]

    def _swap_name(self, info, unique, threshold):
        gender_prob = self.first_name_gender_stats["prob"]
        full_name_gender_stats = self.full_name_gender_stats
        fn_threshold = self.first_name_threshold
        ln_threshold = self.last_name_threshold

        unique = unique[unique.ratio < threshold]
        unique = unique.reindex(columns=["first_name", "last_name", "ratio"])

        info.index.name = "id"
        tmp = pd.merge(
            info.reset_index(), unique, on=["first_name", "last_name"]
        ).set_index("id")

        should_swap = []
        iterator = zip(
            tmp.first_name.values,
            tmp.last_name.values,
            tmp.gender.values,
            tmp.ratio.values,
        )

        for first_name, last_name, gender, ratio in iterator:
            should_swap_this = False
            try:
                stats = full_name_gender_stats.loc[first_name]
                fn_balance = stats["balance"]
                fn_count = stats["count_"]
                ln_balance = full_name_gender_stats.loc[last_name, "balance"]
                if ratio < 0.0001:
                    should_swap_this = True
                elif (
                    fn_count < 7 or fn_balance < fn_threshold
                ) and ln_balance > ln_threshold:
                    try:
                        try:
                            ln_prob = gender_prob.loc[last_name]
                        except:
                            ln_prob = 0.5
                        try:
                            fn_prob = gender_prob.loc[first_name]
                        except:
                            fn_prob = 0.5
                        gender = gender.lower()
                        if (0.25 < fn_prob < 0.75) and (
                            ln_prob < 0.01 or ln_prob > 0.99
                        ):
                            should_swap_this = True
                        elif gender == "m":
                            if ln_prob > 0.85:
                                should_swap_this = True
                        elif gender == "f":
                            if ln_prob < 0.15:
                                should_swap_this = True
                        elif ln_balance > 0.9:
                            should_swap_this = True
                    except Exception as e:
                        print(e)
            except:
                pass
            should_swap.append(should_swap_this)
        tmp = tmp[should_swap]

        if len(tmp) > 0:
            info.first_name.update(tmp.last_name)
            info.last_name.update(tmp.first_name)

        return info
