import pandas as pd

LAST_NOT_ALONE = {
    "phds",
    "dd",
    "md",
    "m d",
    "d m d",
    "vii",
    "ll",
    "mr",
}

LAST_NOT_ALONE_LAST = {
    "vi",
    "x",
    "junior",
}

NOT_ALONE = {
    "dr",
    "ix",
    "iv",
}

INSENSITIV = {
    "11",
    "111",
    "1st",
    "2nd",
    "3rd",
    "ii",
    "iii",
    "jnr",
    "viii",
    "jr",
    "lll",
    "mrs",
    "sr",
    "snr",
}

REPLACE_LOOKUP = {
    "1st": "i",
    "2nd": "ii",
    "2": "ii",
    "3": "iii",
    "4": "iv",
    "5": "v",
    "6": "vi",
    "7": "vii",
    "8": "viii",
    "9": "ix",
    "10": "x",
    "3rd": "iii",
    "l": "i",
    "ll": "ii",
    "lll": "iii",
    "1": "i",
    "11": "ii",
    "111": "iii",
    "j": "jr",
    "jnr": "jr",
    "junior": "jr",
    "snr": "sr",
    "senior": "sr",
    "m d": "md",
    "d m d": "md",
    "phds": "dr",
    "phd": "dr",
}


def _extract_token(tokens, suffixes):
    suffix_tokens = []
    other_tokens = []
    for token in tokens:
        if token in suffixes:
            suffix_tokens.append(token)
        else:
            other_tokens.append(token)
    return " ".join(other_tokens), " ".join(suffix_tokens)


def _update(data, column, names, suffixes):
    name_suff = names.map(lambda x: _extract_token(x, suffixes))
    name_suff.map(lambda x: x[1] != "")
    names = name_suff.str[0]
    suffixes = " " + name_suff.str[1]
    data[column].update(names)
    old_suffixes = data.loc[suffixes.index, "name_suffix"]
    old_suffixes += suffixes
    old_suffixes = old_suffixes.str.strip()
    data.loc[suffixes.index, "name_suffix"] = old_suffixes

    return data


def extract_name_suffix(data):
    data.name_suffix = data.name_suffix.fillna("")
    data.name_suffix = data.name_suffix.str.strip()
    abbreviations = LAST_NOT_ALONE | LAST_NOT_ALONE_LAST | NOT_ALONE | INSENSITIV
    expression = r"\b(?:" + "|".join(abbreviations) + r")\b"
    for column in ["first_name", "middle_name"]:
        names = data[column][data[column].str.contains(expression)]
        names = names.str.split(" ")
        data = _update(data, column, names, INSENSITIV)
        names = names[names.str.len() > 1]
        data = _update(data, column, names, NOT_ALONE)

    column = "last_name"
    names = data[column][data[column].str.contains(expression)]
    names = names.str.split(" ")
    data = _update(data, column, names, INSENSITIV)
    names = names[names.str.len() > 1]
    data = _update(data, column, names, NOT_ALONE | LAST_NOT_ALONE)

    index = names.index
    suffixes = map(lambda x: x if x in LAST_NOT_ALONE_LAST else "", names.str[-1])
    suffixes = " " + pd.Series(suffixes, index=index)
    data.loc[suffixes.index, "name_suffix"] += suffixes

    return data


def process_name_suffix(data):
    data.name_suffix = data.name_suffix.fillna("")
    data.name_suffix = data.name_suffix.str.lower()
    data.name_suffix = data.name_suffix.str.replace(r"[^ a-z0-9]", "")
    data.name_suffix = data.name_suffix.str.replace(r"\s+", " ")
    data.name_suffix = data.name_suffix.str.strip()
    data.name_suffix = data.name_suffix.str.split(" ")
    data.name_suffix = data.name_suffix.map(
        lambda x: set(map(lambda y: REPLACE_LOOKUP.get(y, y), x))
    )
    data.name_suffix = data.name_suffix.str.join(" ")
    return data
