NUMBER_MAP = {
    "1": "i",
    "2": "z",
    "3": "e",
    "4": "a",
    "5": "s",
    "6": "b",
    "7": "z",
    "8": "b",
    "9": "g",
}


def _process_numbers(data):
    """replace number only with " "
    replace 0 with o except when 0 is part of more than 2 numbers
    replace 1-9 with "" except, when string smaller than 5 and number at end then 1 -> i, 2 -> z, 3 -> e, 4 -> a, 5 -> s, 6 -> b -> 7 -> z, 8 -> b, 9 -> g
    replace 11 -> ii, 111 -> iii
    "Recieved ballot" special case
    replace "zx" with "" in number context
    ACP [0-9]* -> ""
    replace single number with letter
    replace all numbers with " " when both words longer than 5
    replace single letter followed with more than 2 numbers with ""
    """
    numbers = data[data.str.contains(r"[0-9]")]
    numbers = numbers.str.replace(r"\bzx[0-9]", "")
    numbers = numbers.str.replace(r"\b11\b", "ii")
    numbers = numbers.str.replace(r"\b111\b", "iii")
    numbers = numbers.str.replace(r"\b\w?[0-9]{3,}\b", " ")
    numbers = numbers.str.replace(r"[0-9]{2,}", " ")
    numbers = numbers.str.replace(r"\b(\w{5,})[0-9](\w{5,})\b", r"\1 \2")
    numbers = numbers.str.replace(r"0", "o")
    for number, translation in NUMBER_MAP.items():
        numbers = numbers.str.replace(
            r"\b(\w{1,4})" + number + r"\b", r"\1" + translation
        )
        numbers = numbers.str.replace(r"^\s*" + number + r"\s*$", translation)
    numbers = numbers.str.replace("^.*recieved.*ballot.*$", "")
    numbers = numbers.str.replace(r"ACP [0-9]+", "")
    numbers = numbers.str.replace(r"[0-9]", " ")
    data.update(numbers)
    return data


def _remove_prefix(name):
    tokens = name.split(" ")
    last_token = None
    clean_tokens = []
    for token in tokens:
        if last_token is None:
            last_token = token

        if last_token.startswith(token):
            continue
        elif token.startswith(last_token):
            last_token = token
        else:
            clean_tokens.append(last_token)
            last_token = token
    if last_token is not None:
        clean_tokens.append(last_token)
    return " ".join(clean_tokens)


def _process_punctuation(data):
    """replace \ with "" except when both strings are longer than 4
    replace " with " " except when both strings have at most length 3, then ""
    replace ', ` and , with " ", when first word is length 1 or both words are longer than 3
    replace / with " " and when one word is prefix of other, than replace
    replace ; with l
    replace everything else with " "
    """
    data = data.str.replace(r"-", " ")
    punct = data[data.str.contains(r"[^ 0-9a-z]")]
    punct = punct.str.replace(r"\b(\w{5,})\\(\w{5,})\b", r"\1 \2")
    punct = punct.str.replace(r"\\", "")
    punct = punct.str.replace(r'\b(\w{,3})"(\w{,3})\b', r"\1\2")
    punct = punct.str.replace(r'\b(\w)"(\w+)\b', r"\1 \2")
    punct = punct.str.replace(r'"', "")
    punct = punct.str.replace(r"\b(\w)['`,](\w+)\b", r"\1 \2")
    punct = punct.str.replace(r"\b(\w{4,})['`,](\w{4,})\b", r"\1 \2")
    punct = punct.str.replace(r"['`,]", "")
    punct = punct.str.replace(r"['`,]", "")
    punct = punct.str.replace(r";", "l")
    tmp = punct[punct.str.contains(r"/")]
    tmp = tmp.str.replace(r"/", " ")
    tmp = tmp.map(_remove_prefix)
    punct.update(tmp)
    punct = punct.str.replace(r"[^ 0-9a-z]", " ")
    data.update(punct)
    return data


def _normalize_space(data):
    data = data.str.replace(r"\s+", " ")
    data = data.str.strip()
    return data


def _remove_invalid_values(data):
    data[data.str.contains(r"\bno\b.*\bname\b")] = ""
    data[data.str.contains(r"\bnone\b")] = ""
    return data


def process_name(data):
    data = data.str.lower()
    data = data.fillna("")
    data = _process_punctuation(data)
    data = _process_numbers(data)
    data = _normalize_space(data)
    data = _remove_invalid_values(data)
    return data
