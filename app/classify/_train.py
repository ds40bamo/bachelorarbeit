import inspect
import logging
import random
from hashlib import sha256
from itertools import starmap

import joblib
import numpy as np
import pandas as pd
import ray
from sklearn.ensemble import RandomForestClassifier

from app.compare import apply_metrics
from app.config import DedupeConfig
from app.data import gen_path
from app.utils import gen_hash


def split_data(data, blocksize=10000):
    num_blocks = max(len(data) // blocksize, 1)
    return np.array_split(data, num_blocks)


@ray.remote
def remote_block(key, block1, block2):
    return key.block(block1, block2)


class Block:
    def __init__(self, blocks, key, num_workers, logger):
        self.blocks = blocks
        self.key = key
        self.num_workers = num_workers
        self.logger = logger
        if num_workers > 1:
            ray.init()

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        self.close()

    def close(self):
        if self.num_workers > 1:
            ray.shutdown()

    def __iter__(self):
        current_block = 1
        num_blocks = len(self.blocks)
        if self.num_workers == 1:
            for result in starmap(self.key.block, self.blocks):
                self.logger.info("%d/%d", current_block, num_blocks)
                current_block += 1
                yield result
            return

        running = []
        finnished = []
        for block1, block2 in self.blocks:
            running.append(remote_block.remote(self.key, block1, block2))
            if len(running) >= self.num_workers:
                finnished, running = ray.wait(running)
                for result in ray.get(finnished):
                    self.logger.info("%d/%d", current_block, num_blocks)
                    current_block += 1
                    yield result
        while len(running) > 0:
            finnished, running = ray.wait(running)
            for result in ray.get(finnished):
                self.logger.info("%d/%d", current_block, num_blocks)
                current_block += 1
                yield result


def _diagnoal_excluding_product(iterable):
    iterable = list(iterable)
    for i, element1 in enumerate(iterable):
        for j, element2 in enumerate(iterable):
            if i != j:
                yield (element1, element2)


def _reduce_results(pairs, data1, data2, max_num, strategy, num_random_samples):
    if strategy == "max":
        ascending = False
    elif strategy == "min":
        ascending = True
    else:
        raise ValueError("unknown strategy")

    common_cols = list(set(data1.columns) & set(data2.columns))

    num_same = (
        data1.loc[pairs["left"], common_cols].reset_index(drop=True)
        == data2.loc[pairs["right"], common_cols].reset_index(drop=True)
    ).sum(axis=1)
    num_same.name = "num_same"
    num_same = num_same.to_frame()
    num_same["left"] = pairs["left"].values
    num_same["right"] = pairs["right"].values
    num_same.sort_values("num_same", inplace=True, ascending=ascending)
    upper_bound = max_num - num_random_samples
    upper_sample, rest = (
        num_same[["left", "right"]][:upper_bound],
        num_same[["left", "right"]][upper_bound:],
    )

    if num_random_samples >= len(rest):
        random_sample = rest
    else:
        random_sample = rest.sample(num_random_samples)
    return pd.concat((upper_sample, random_sample))


def _filter_matches(results, data1, data2, match_filter_classifier, filter_different):
    clf = match_filter_classifier["clf"]
    config = match_filter_classifier["config"]
    interesting_pairs = _reduce_results(
        results, data1, data2, filter_different, "min", 0
    )
    encoded = apply_metrics(
        interesting_pairs.set_index(["left", "right"]).index, data1, data2, config
    )
    target = clf.predict(encoded)
    non_matches = interesting_pairs[target == 0]
    results = results.merge(
        non_matches, on=["left", "right"], how="outer", indicator=True
    )
    results = results[results["_merge"] != "both"]
    return results[["left", "right"]]


def _filter_non_matches(
    results, data1, data2, non_match_filter_classifier, filter_different
):
    clf = non_match_filter_classifier["clf"]
    config = non_match_filter_classifier["config"]
    interesting_pairs = _reduce_results(
        results, data1, data2, filter_different, "max", 0
    )
    encoded = apply_metrics(
        interesting_pairs.set_index(["left", "right"]).index, data1, data2, config
    )
    target = clf.predict(encoded)
    matches = interesting_pairs[target == 1]
    results = results.merge(matches, on=["left", "right"], how="outer", indicator=True)
    results = results[results["_merge"] != "both"]
    return results[["left", "right"]]


def _update_non_matches(
    non_matches,
    results,
    non_match_data1,
    non_match_data2,
    non_match_filter_classifier,
    max_num_non_matches,
    num_random_non_match_samples,
):
    result = pd.concat(results)
    result = _filter_non_matches(
        result,
        non_match_data1,
        non_match_data2,
        non_match_filter_classifier,
        filter_different=10000,
    )
    if non_matches is not None:
        non_matches = pd.concat((non_matches, result))
    else:
        non_matches = result
    non_matches = _reduce_results(
        non_matches,
        non_match_data1,
        non_match_data2,
        max_num_non_matches,
        "max",
        num_random_non_match_samples,
    )
    return non_matches


def generate_samples(
    data1,
    data2,
    key,
    match_filter_classifier,
    non_match_filter_classifier,
    max_num_matches,
    max_num_non_matches,
    num_random_match_samples,
    num_random_non_match_samples,
    blocksize=10000,
    num_extra_blocks=0,
    num_workers=16,
):
    assert len(data1) == len(data2)
    logger = logging.getLogger(inspect.currentframe().f_code.co_name)
    logger.setLevel(logging.INFO)
    blocks1 = split_data(data1, blocksize)
    blocks2 = split_data(data2, blocksize)
    num_splits = len(blocks1)

    if num_extra_blocks > 0:
        extra_blocks = random.sample(
            list(_diagnoal_excluding_product(range(num_splits))), num_extra_blocks
        )
        extra_blocks1, extra_blocks2 = zip(*extra_blocks)
        blocks1 += [blocks1[i] for i in extra_blocks1]
        blocks2 += [blocks1[i] for i in extra_blocks2]
    blocks = list(zip(blocks1, blocks2))

    match_data1, match_data2 = match_filter_classifier["config"]._apply(
        data1.copy(), data2.copy()
    )
    matches = pd.DataFrame({"left": data1.index, "right": data2.index})
    matches = _filter_matches(
        matches,
        match_data1,
        match_data2,
        match_filter_classifier,
        filter_different=10000,
    )
    matches = _reduce_results(
        matches,
        match_data1,
        match_data2,
        max_num_matches,
        "min",
        num_random_match_samples,
    )
    del match_data1
    del match_data2

    non_matches = None

    non_match_data1, non_match_data2 = non_match_filter_classifier["config"]._apply(
        data1.copy(), data2.copy()
    )
    non_matches = []
    with Block(blocks, key, num_workers, logger) as blocker:
        for i, result in enumerate(blocker, start=1):
            result = pd.DataFrame(result, columns=["left", "right"])
            result = result[result["left"] != result["right"]]
            non_matches.append(result)
    non_matches = _filter_non_matches(
        pd.concat(non_matches),
        non_match_data1,
        non_match_data2,
        non_match_filter_classifier,
        filter_different=50000,
    )
    non_matches = _reduce_results(
        non_matches,
        non_match_data1,
        non_match_data2,
        max_num_non_matches,
        "max",
        num_random_non_match_samples,
    )

    samples = pd.concat((matches, non_matches))
    target = (samples["left"].values == samples["right"].values).astype("int8")

    return samples, target


def _load_full_data(date1, date2, voter_ids, chunksize=1000000):
    chunks1 = []
    chunks2 = []
    train_path_left, train_path_right = gen_path(
        date1, date2, train=True, suffix="_train.csv"
    )

    for chunk in pd.read_csv(
        train_path_left, keep_default_na=False, dtype="str", chunksize=chunksize
    ):
        chunks1.append(chunk[chunk["voter_id"].isin(voter_ids)])

    for chunk in pd.read_csv(
        train_path_right, keep_default_na=False, dtype="str", chunksize=chunksize
    ):
        chunks2.append(chunk[chunk["voter_id"].isin(voter_ids)])

    return pd.concat(chunks1), pd.concat(chunks2)


def _generate_train_target(
    date1,
    date2,
    match_filter_classifier,
    non_match_filter_classifier,
    _config,
    max_num_matches,
    max_num_non_matches,
    num_random_match_samples,
    num_random_non_match_samples,
    blocksize,
    maxminor,
    num_extra_blocks,
    num_workers,
):
    key = _config._key
    data1, _ = _config.load_train(
        date1, date2, maxminor=maxminor, voter_id=True, skip_apply=True
    )
    voter_ids = data1["voter_id"].astype("str")
    del data1
    del _
    data1, data2 = _load_full_data(date1, date2, voter_ids)
    samples, target = generate_samples(
        data1,
        data2,
        key,
        match_filter_classifier,
        non_match_filter_classifier,
        max_num_matches,
        max_num_non_matches,
        num_random_match_samples,
        num_random_non_match_samples,
        blocksize,
        num_extra_blocks,
        num_workers,
    )
    data1 = data1.loc[samples["left"].unique()]
    data2 = data2.loc[samples["right"].unique()]
    data1, data2 = _config._apply(data1, data2)
    train = apply_metrics(
        samples.set_index(["left", "right"]).index, data1, data2, _config
    )
    return train, target


class BaseClassifier:
    MODEL_PREFIX = None

    def __init__(self, date1, date2):
        self.date1 = date1
        self.date2 = date2

    @classmethod
    def build(
        cls,
        date1,
        date2,
        match_filter_classifier,
        non_match_filter_classifier,
        _config,
        max_num_matches=50000,
        max_num_non_matches=500000,
        num_random_match_samples=10000,
        num_random_non_match_samples=100000,
        blocksize=10000,
        maxminor=500000,
        num_extra_blocks=100,
        num_workers=16,
        n_jobs=-1,
    ):
        if isinstance(_config, DedupeConfig):
            _config = _config.to_link_config()
        train, target = _generate_train_target(
            date1,
            date2,
            match_filter_classifier,
            non_match_filter_classifier,
            _config,
            max_num_matches,
            max_num_non_matches,
            num_random_match_samples,
            num_random_non_match_samples,
            blocksize,
            maxminor,
            num_extra_blocks,
            num_workers,
        )
        classifier = cls(date1, date2)
        classifier.fit(train, target, n_jobs=n_jobs)
        return classifier

    @classmethod
    def load(cls, date1, date2):
        model_path = gen_path(
            date1, date2, train=True, filename=cls.MODEL_PREFIX + "_model.pkl"
        )

        model = joblib.load(model_path)

        classifier = cls(date1, date2)
        classifier.model = model
        return classifier

    def fit(self, train, target, n_jobs=-1):
        logger = logging.getLogger("train")
        logger.setLevel(logging.INFO)
        logger.info("fitting classifier")
        model = RandomForestClassifier(n_estimators=50, n_jobs=n_jobs).fit(
            train, target
        )
        self.model = model
        return self

    def predict(self, encoded_data):
        try:
            status = self.model.predict(encoded_data)
            prob = self.model.predict_proba(encoded_data)[:, 1]
            return pd.DataFrame({"status": status, "prob": prob})
        except AttributeError:
            raise AttributeError("call fit before predict")

    @classmethod
    def exists(cls, date1, date2):
        return gen_path(
            date1, date2, train=True, filename=cls.MODEL_PREFIX + "_model.pkl"
        ).exists()

    def save(self):
        model_path = gen_path(
            self.date1,
            self.date2,
            train=True,
            filename=self.MODEL_PREFIX + "_model.pkl",
        )
        joblib.dump(self.model, model_path, compress=9)

    def hash(self):
        hasher = sha256()
        gen_hash(hasher, list(map(float, self.model.feature_importances_)))
        return hasher.hexdigest()


class DedupeClassifier(BaseClassifier):
    MODEL_PREFIX = "dedupe"


class LinkClassifier(BaseClassifier):
    MODEL_PREFIX = "link"
