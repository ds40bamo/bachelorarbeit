import inspect
import logging
import random
import warnings

import numpy as np
import pandas as pd
from itertools import compress

from app.classify._train import Block, _diagnoal_excluding_product, split_data
from app.data import gen_path, load_review_link_data
from app.compare import apply_metrics

from ._base_reviewer import Reviewer


def _load_train_data(date1, date2, config):
    return config.load_train(date1, date2, voter_id=True)

def _load_potential_matches(date1, date2, config):
    data1, data2 = _load_train_data(date1, date2, config)
    matches = pd.DataFrame({"left": data1.index, "right": data2.index})
    return matches, data1, data2

def _load_potential_non_matches(date1, date2, config, blocksize, num_extra_blocks, num_workers):
    key = config._key
    data1, data2 = _load_train_data(date1, date2, config)
    blocks1 = split_data(data1, blocksize)
    blocks2 = split_data(data2, blocksize)
    num_splits = len(blocks1)

    extra_blocks = random.sample(list(_diagnoal_excluding_product(range(num_splits))), num_extra_blocks)
    extra_blocks1, extra_blocks2 = zip(*extra_blocks)
    blocks1 += [blocks1[i] for i in extra_blocks1]
    blocks2 += [blocks1[i] for i in extra_blocks2]
    blocks = list(zip(blocks1, blocks2))

    non_matches = None

    logger = logging.getLogger(inspect.currentframe().f_code.co_name)
    logger.setLevel(logging.INFO)

    with Block(blocks, key, num_workers, logger) as blocker:
        for result in blocker:
            result = pd.DataFrame(result, columns=["left", "right"])
            result = result[result["left"] != result["right"]]
            if non_matches is not None:
                non_matches = pd.concat((non_matches, result))
            else:
                non_matches = result

    return non_matches, data1, data2


class TrainDataReviewer(Reviewer):
    def __init__(self, kind, date1, date2, data1, data2, links, config, classifier):
        self.kind = kind
        self.date1 = date1
        self.date2 = date2
        super().__init__(links, data1, data2, config, self._save_path(date1, date2, kind), classifier=classifier)

    @staticmethod
    def _save_path(date1, date2, kind):
        if kind not in {"matches", "non_matches"}:
            raise ValueError(f"unknown kind {kind}")
        return gen_path(date1, date2, train=True, filename=kind+"_reviewer.pkl")

    @staticmethod
    def create(date1, date2, config, kind, num_review=100000, blocksize=10000, num_extra_blocks=50, num_workers=16, classifier="random_forest"):
        if kind == "matches":
            links, data1, data2 = _load_potential_matches(date1, date2, config)
            ascending = True
        elif kind == "non_matches":
            links, data1, data2 = _load_potential_non_matches(date1, date2, config, blocksize, num_extra_blocks, num_workers)
            ascending = False
        else:
            raise ValueError(f"unknown kind {kind}")

        data_left = data1.loc[links.left].reset_index()
        data_right = data2.loc[links.right].reset_index()
        common_cols = list(set(data_left.columns) & set(data_right.columns))
        num_same = (data_left[common_cols] == data_right[common_cols]).sum(axis=1)
        num_same.name = "num_same"
        num_same = num_same.to_frame()
        num_same["left"] = links["left"].values
        num_same["right"] = links["right"].values
        num_same.sort_values("num_same", inplace=True, ascending=ascending)
        links = num_same[:num_review][["left", "right"]]
        links = links.set_index(["left", "right"]).index
        data1 = data1[data1.index.isin(links.get_level_values("left"))]
        data2 = data2[data2.index.isin(links.get_level_values("right"))]
        links = pd.DataFrame({"status": np.nan, "prob": np.nan}, index=links)

        return TrainDataReviewer(kind, date1, date2, data1, data2, links, config, classifier)

    @classmethod
    def load(cls, date1, date2, kind):
        return cls._load(cls._save_path(date1, date2, kind))

    def train_classifier(self, config, classifier="random_forest"):
        if not self.done:
            warnings.warn("there are instances left to label")
        left_ids = self.data1.loc[[link[0][0] for link in self.evaluated], "voter_id"].reset_index(drop=True)
        right_ids = self.data2.loc[[link[0][1] for link in self.evaluated], "voter_id"].reset_index(drop=True)
        left_ids.name = "left"
        right_ids.name = "right"
        links = pd.concat([left_ids, right_ids], axis=1).astype("str")
        data1, data2 = config.load_train_from_voter_ids(self.date1, self.date2, links)
        selector = links["left"].isin(data1["voter_id"]) & links["right"].isin(data2["voter_id"])
        target = list(compress(self.target, selector.values))
        links = links[selector]
        links = links.set_index(["left", "right"]).index
        data1 = data1.set_index("voter_id")
        data2 = data2.set_index("voter_id")
        encoded = apply_metrics(links, data1, data2, config)
        clf = self._new_estimator(classifier)
        return {"clf": clf.fit(encoded, target), "config": config}
