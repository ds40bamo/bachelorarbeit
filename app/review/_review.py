import datetime
from itertools import compress

import joblib
import numpy as np
import pandas as pd
from dateutil import parser as dateparser
from modAL import ActiveLearner
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

from app.compare import apply_metrics
from app.config import DedupeConfig
from app.data import gen_path, load_review_link_data

from ._spokeo import get_spokeo_info


def age_of(date):
    return int(
        (datetime.date.today() - dateparser.parse(date, dayfirst=False).date()).days
        // 365.25
    )


class Review:
    def __init__(self, date1, date2, data1, data2, links, config, counties):
        pd.set_option("display.max_colwidth", 65)
        if isinstance(config, DedupeConfig):
            config = config.to_link_config()
        self.date1 = date1
        self.date2 = date2
        self.data1 = data1
        self.data2 = data2
        self.config = config
        self.counties = counties
        self.encoded = apply_metrics(
            links.index, data1, data2, config.metric_config
        ).values
        self.links = [
            (row.Index, row.status, row.prob) for row in list(links.itertuples())
        ]
        self.evaluated = []
        self.evaluated_encoded = None
        self.target = []
        self.last_commit = 0
        self.learner = ActiveLearner(estimator=RandomForestClassifier(n_jobs=-1))
        self.done = False

    @staticmethod
    def create(date1, date2, config, counties):
        links, data1, data2 = load_review_link_data(date1, date2, config, counties, voter_ids=True)
        voter_ids1 = data1.loc[links.index.get_level_values("left")].voter_ids.values
        voter_ids2 = data2.loc[links.index.get_level_values("right")].voter_ids.values
        links = links[voter_ids1 != voter_ids2]
        return Review(date1, date2, data1, data2, links, config, counties)

    def pair(self, idx):
        id1, id2 = self.links[idx][0]
        instance1 = self.data1.loc[id1]
        instance2 = self.data2.loc[id2]
        comparison = pd.concat([instance1, instance2], axis=1)
        if "birth_date" in comparison.index:
            age_frame = pd.DataFrame(
                {
                    instance1.name: [age_of(instance1["birth_date"])],
                    instance2.name: [age_of(instance2["birth_date"])],
                },
                index=["age"],
            )
            comparison = comparison.append(age_frame)

        return comparison

    def query(self):
        if len(self.encoded) == 0:
            return None
        idx, _ = self.learner.query(self.encoded)
        (idx,) = idx
        return idx

    def update(self, idx, target):
        assert target in [0, 1]
        row = self.links[idx]
        del self.links[idx]
        self.evaluated.append(row)

        if self.evaluated_encoded is None:
            self.evaluated_encoded = self.encoded[[idx]]
        else:
            self.evaluated_encoded = np.concatenate(
                [self.evaluated_encoded, self.encoded[[idx]]]
            )

        self.encoded = np.delete(self.encoded, idx, axis=0)
        self.target.append(target)

    def previous(self):
        if len(self.evaluated) == 0:
            return
        row = self.evaluated[-1]
        del self.evaluated[-1]
        self.links.insert(0, row)

        self.encoded = np.concatenate([self.evaluated_encoded[[-1]], self.encoded])
        self.evaluated_encoded = self.evaluated_encoded[:-1]
        del self.target[-1]

        self.commit()

    def commit(self):
        if not self.evaluated:
            self.learner = ActiveLearner(estimator=RandomForestClassifier())
        else:
            self.learner.fit(self.evaluated_encoded, self.target)
        self.last_commit = len(self.evaluated)

    @staticmethod
    def load(date1, date2, counties):
        review_path = gen_path(date1, date2, filename="review.pkl", counties=counties)
        return joblib.load(review_path)

    def save(self):
        review_path = gen_path(self.date1, self.date2, filename="review.pkl", counties=self.counties)
        joblib.dump(self, review_path, compress=9)

    def write_results(self):
        if not self.done:
            raise ValueError("there are still pairs left to label")
        save_path = gen_path(self.date1, self.date2, counties=self.counties, filename="review_links.csv")
        links = [link for link, target in zip(self.evaluated, self.target) if target == 1]
        pd.DataFrame(links, names=["left", "right"]).to_csv(save_path, index=False)

    def _repredict_update(self, selector, target):
        evaluated_encoded = np.concatenate(
            [self.evaluated_encoded, self.encoded[selector]]
        )
        self.target.extend([target] * np.sum(selector))
        self.evaluated.extend(compress(self.links, selector))
        self.links = list(compress(self.links, ~selector))

    def repredict(self):
        clf = GridSearchCV(
            RandomForestClassifier(),
            {"n_estimators": 10 + 10 * np.arange(15)},
            cv=5,
            n_jobs=6,
        )
        clf = clf.fit(self.evaluated_encoded, self.target)
        proba = clf.predict_proba(self.encoded)[:, 1]
        matches = proba == 1.0
        non_matches = proba == 0.0
        self._repredict_update(matches, 1)
        self._repredict_update(non_matches, 0)
        self.commit()

    def get_links(self, predict_all=False, only_matches=True):
        if predict_all:
            clf = GridSearchCV(
                RandomForestClassifier(),
                {"n_estimators": 10 + 10 * np.arange(15)},
                cv=5,
                n_jobs=6,
            )
            clf = clf.fit(self.evaluated_encoded, self.target)
            extra_target = clf.predict(self.encoded)
            target = self.target.copy()
            target.extend(extra_target)
            evaluated = self.evaluated.copy()
            evaluated.extend(self.links)
            link_index = pd.MultiIndex.from_tuples(map(lambda x: x[0], evaluated))
        else:
            link_index = pd.MultiIndex.from_tuples(map(lambda x: x[0], self.evaluated))
            target = self.target
        link_index.set_name(["left", "right"])
        if only_matches:
            return link_index[np.array(target) == 1]
        else:
            return pd.Series(target, index=link_index)

    def label_loop(self):
        idx = self.query()
        if idx is None:
            print("no instances left")
            self.commit()
            self.done = True
            return
        comparison = self.pair(idx)
        spokeo_info = None
        while True:
            print(comparison)
            if "age" in comparison.index:
                left_age, right_age = comparison.loc["age"]
                if spokeo_info is not None:
                    print(spokeo_info)
                elif (
                    "first_name" in comparison.index and "last_name" in comparison.index
                ):
                    left_first_name, right_first_name = comparison.loc["first_name"]
                    left_middle_name, right_middle_name = "", ""
                    if "middle_name" in comparison.index:
                        left_middle_name, right_middle_name = comparison.loc[
                            "middle_name"
                        ]
                    left_last_name, right_last_name = comparison.loc["last_name"]
                    spokeo = get_spokeo_info(
                        left_first_name,
                        left_middle_name,
                        left_last_name,
                        left_age,
                        right_first_name,
                        right_middle_name,
                        right_last_name,
                        right_age,
                    )
                    if spokeo is not None:
                        print(spokeo)
                    else:
                        print("no spokeo entries found")
            _, status, prob = self.links[idx]
            print(f"desicion of original classifier: {status}")
            print(f"with probability: {prob:.3f}")
            print(f"instances left: {len(self.links)}")
            print(f"instances labeled: {len(self.evaluated)}")
            print(
                f"instances labeled since last commit: {len(self.evaluated) - self.last_commit}"
            )
            desicion = input(
                "same? y(es)/n(o)/p(revious)/c(ommit)/r(epredict)/s(top): "
            )
            if desicion in ["y", "n"]:
                target = 1 if desicion == "y" else 0
                self.update(idx, target)
            elif desicion == "p":
                self.previous()
            elif desicion == "s":
                break
            elif desicion == "c":
                self.commit()
                continue
            elif desicion == "r":
                self.repredict()
                continue
            else:
                print("not understood")
                print()
                continue
            idx = self.query()
            if idx is None:
                print("no instances left")
                self.commit()
                self.done = True
                return
            spokeo_info = None
            comparison = self.pair(idx)
