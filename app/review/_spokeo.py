import re
from multiprocessing.pool import ThreadPool

import pandas as pd
import requests
from bs4 import BeautifulSoup

from app.compare._metrics import JaroWinkler

split_re = re.compile(r"\s+")


symmetric_name_score = JaroWinkler().build(empty_no_info=True, multiple=True, make_symmetric=True)


def spokeo_split(line, on):
    start, *rest = line.split(on)
    if start == "" and len(rest) > 0:
        return on.join(rest).strip()
    return None


def parse_spokeo_line(line):
    line = line.lower()

    for starts in ["resides in", "lived in", "also known as"]:
        content = spokeo_split(line, starts)
        if content is not None:
            return starts, content
    return None


def score_entry(entry, fullname, age):
    fullname = split_re.split(fullname.strip())
    name = entry.get("name")
    if name is not None:
        token_name = split_re.split(name.strip())
    else:
        token_name = None
    entry_age = int(entry["age"])
    aka = entry.get("also known as")
    if aka is not None:
        token_aka = list(map(lambda x: split_re.split(x.strip()), aka.split(",")))
    else:
        token_aka = []
    if token_name is not None:
        token_aka.append(token_name)
    if len(token_aka) == 0:
        return 0.0
    i = list(map(lambda x: symmetric_name_score(x, fullname), token_aka))
    name_score = max(map(lambda x: symmetric_name_score(x, fullname), token_aka)) / 2
    age_score = int(entry_age == age)
    return 0.8 * name_score + 0.2 * age_score


def best_entrie(entries, fullname, age):
    return max(entries, key=lambda entry: score_entry(entry, fullname, age))


def query_spokeo(first_name, middle_name, last_name, age):
    if isinstance(first_name, list):
        first_name = map(lambda x: "".join(split_re.split(x)), first_name)
        first_name = " ".join(first_name)
    if isinstance(middle_name, list):
        middle_name = map(lambda x: "".join(split_re.split(x)), middle_name)
        middle_name = " ".join(middle_name)
    if isinstance(last_name, list):
        last_name = list(map(lambda x: "".join(split_re.split(x)), last_name))
        if len(last_name) > 0:
            last_name_spokeo = last_name[-1]
        else:
            last_name_spokeo = ""
        last_name = " ".join(last_name)
    response = requests.get(
        f"https://www.spokeo.com/search/{first_name}-{last_name_spokeo}",
        params={"state": "Florida", "age_range": f"{age-1}-{age+1}"},
        headers={"User-Agent": ""},
    )
    if not response.ok:
        return None
    soup = BeautifulSoup(response.text, features="lxml")
    items = soup.select("a.single-column-list-item > div")
    items = list(map(lambda x: list(map(lambda y: y.text, x.select("div"))), items))
    entries = []
    for item in items:
        name_with_age, item = item[0], item[1:]
        try:
            name, entry_age = name_with_age.split(",")
        except ValueError:
            return None
        name = name.strip().lower()
        entry_age = entry_age.strip().lower()
        entry = dict(filter(lambda x: x is not None, map(parse_spokeo_line, item)))
        entry["name"] = name
        entry["age"] = entry_age
        entries.append(entry)

    return best_entrie(entries, " ".join([first_name, middle_name, last_name]), age)


def get_spokeo_info(
    left_first_name,
    left_middle_name,
    left_last_name,
    left_age,
    right_first_name,
    right_middle_name,
    right_last_name,
    right_age,
):
    with ThreadPool(2) as pool:
        left_spokeo, right_spokeo = pool.starmap(
            query_spokeo,
            [
                (
                    "".join(split_re.split(left_first_name)),
                    left_middle_name,
                    left_last_name,
                    left_age,
                ),
                (
                    "".join(split_re.split(right_first_name)),
                    right_middle_name,
                    right_last_name,
                    right_age,
                ),
            ],
        )
    spokeo_info = [
        pd.Series(data, name=name)
        for data, name in ((left_spokeo, "left"), (right_spokeo, "right"))
        if data is not None
    ]
    if spokeo_info:
        return pd.concat(spokeo_info, axis=1).fillna("")
    return None
