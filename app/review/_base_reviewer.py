import datetime
from itertools import compress

import joblib
import numpy as np
import pandas as pd
from dateutil import parser as dateparser
from modAL import ActiveLearner
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

from app.compare import apply_metrics
from app.config import DedupeConfig
from app.data import gen_path, load_review_link_data
from abc import abstractstaticmethod

from ._spokeo import get_spokeo_info


def age_of(date):
    return int(
        (datetime.date.today() - dateparser.parse(date, dayfirst=False).date()).days
        // 365.25
    )


class Reviewer:
    def __init__(self, links, data1, data2, config, save_path, classifier="random_forest", max_proba=0.95, min_proba=0.05, use_spokeo=False):
        pd.set_option("display.max_colwidth", 65)
        self.save_path = save_path
        self.data1 = data1
        self.data2 = data2
        self.encoded = apply_metrics(
            links.index, data1, data2, config
        ).values
        self.links = [
            (row.Index, row.status, row.prob) for row in list(links.itertuples())
        ]
        self.evaluated = []
        self.evaluated_encoded = None
        self.target = []
        self.last_commit = 0
        self.classifier_name = classifier
        self.learner = ActiveLearner(estimator=self._new_estimator(self.classifier_name))
        self.done = False
        self.max_proba = max_proba
        self.min_proba = min_proba
        self.use_spokeo = use_spokeo

    @staticmethod
    def _new_estimator(classifier):
        if classifier == "random_forest":
            return RandomForestClassifier(n_estimators=50, n_jobs=-1)
        elif classifier == "svm_rbf":
            return SVC(probability=True)

    @abstractstaticmethod
    def create():
        raise NotImplementedError()

    def pair(self, idx):
        id1, id2 = self.links[idx][0]
        instance1 = self.data1.loc[id1]
        instance2 = self.data2.loc[id2]
        instance1.name = f"{instance1.name}_left"
        instance2.name = f"{instance2.name}_right"
        comparison = pd.concat([instance1, instance2], axis=1)
        if "birth_date" in comparison.index:
            age_frame = pd.DataFrame(
                {
                    instance1.name: [age_of(instance1["birth_date"])],
                    instance2.name: [age_of(instance2["birth_date"])],
                },
                index=["age"],
            )
            comparison = comparison.append(age_frame)

        return comparison

    def query(self):
        if len(self.encoded) == 0:
            return None
        idx, _ = self.learner.query(self.encoded)
        (idx,) = idx
        return idx

    def update(self, idx, target):
        assert target in [0, 1]
        row = self.links[idx]
        del self.links[idx]
        self.evaluated.append(row)

        if self.evaluated_encoded is None:
            self.evaluated_encoded = self.encoded[[idx]]
        else:
            self.evaluated_encoded = np.concatenate(
                [self.evaluated_encoded, self.encoded[[idx]]]
            )

        self.encoded = np.delete(self.encoded, idx, axis=0)
        self.target.append(target)

    def previous(self):
        if len(self.evaluated) == 0:
            return
        *self.evaluated, row = self.evaluated
        self.links.insert(0, row)

        self.encoded = np.concatenate([self.evaluated_encoded[[-1]], self.encoded])
        self.evaluated_encoded = self.evaluated_encoded[:-1]
        del self.target[-1]

        self.commit()

    def commit(self):
        if not self.evaluated:
            self.learner = ActiveLearner(estimator=self._new_estimator(self.classifier_name))
        else:
            self.learner.fit(self.evaluated_encoded, self.target)
        self.last_commit = len(self.evaluated)

    @staticmethod
    def _load(load_path):
        return joblib.load(load_path)

    def save(self):
        joblib.dump(self, self.save_path, compress=9)

    def repredict(self):
        proba = self.learner.predict_proba(self.encoded)[:, 1]
        matches = proba >= self.max_proba
        non_matches = proba <= self.min_proba
        new_evaluated = matches | non_matches
        self.evaluated_encoded = np.concatenate(
            [self.evaluated_encoded, self.encoded[matches], self.encoded[non_matches]]
        )
        self.target.extend([1] * np.sum(matches))
        self.target.extend([0] * np.sum(non_matches))
        self.evaluated.extend(compress(self.links, matches))
        self.evaluated.extend(compress(self.links, non_matches))
        self.encoded = self.encoded[~new_evaluated]
        self.links = list(compress(self.links, ~new_evaluated))
        self.commit()

    def get_links(self, predict_all=False, only_matches=True):
            if predict_all:
                extra_target = self.learner.predict(self.encoded)
                target = self.target.copy()
                target.extend(extra_target)
                evaluated = self.evaluated.copy()
                evaluated.extend(self.links)
                link_index = pd.MultiIndex.from_tuples([e[0] for e in evaluated])
            else:
                link_index = pd.MultiIndex.from_tuples([e[0] for e in self.evaluated])
                target = self.target
            link_index.set_name(["left", "right"])
            if only_matches:
                return link_index[np.array(target) == 1]
            else:
                return pd.Series(target, index=link_index)

    @staticmethod
    def _get_spokeo(comparison):
        if "age" in comparison.index:
            left_age, right_age = comparison.loc["age"]
        left_first_name, right_first_name = comparison.loc["first_name"]
        left_middle_name, right_middle_name = "", ""
        if "middle_name" in comparison.index:
            left_middle_name, right_middle_name = comparison.loc[
                "middle_name"
            ]
        left_last_name, right_last_name = comparison.loc["last_name"]
        spokeo = get_spokeo_info(
            left_first_name,
            left_middle_name,
            left_last_name,
            left_age,
            right_first_name,
            right_middle_name,
            right_last_name,
            right_age,
        )
        return spokeo

    def _print_information(self, idx):
        spokeo_info = self.spokeo_info
        comparison = self.comparison
        print(comparison)
        if self.use_spokeo:
            if spokeo_info is not None:
                print(spokeo_info)
            elif (
                "first_name" in comparison.index and "last_name" in comparison.index
            ):
                spokeo_info = self._get_spokeo(comparison)
                self.spokeo_info = spokeo_info
                if spokeo_info is not None:
                    print(spokeo_info)
                else:
                    print("no spokeo entries found")
        _, status, prob = self.links[idx]
        print(f"desicion of original classifier: {status}")
        print(f"with probability: {prob:.3f}")
        print(f"instances left: {len(self.links)}")
        print(f"instances labeled: {len(self.evaluated)}")
        print(
            f"instances labeled since last commit: {len(self.evaluated) - self.last_commit}"
        )

    def _get_action(self, idx):
        desicion = input(
            "same? y(es)/n(o)/p(revious)/c(ommit)/r(epredict)/s(top): "
        )
        if desicion in ["y", "n"]:
            target = 1 if desicion == "y" else 0
            self.update(idx, target)
            return False
        elif desicion == "p":
            self.previous()
            return False
        elif desicion == "s":
            return None
        elif desicion == "c":
            self.commit()
            return False
        elif desicion == "r":
            self.repredict()
            return False
        else:
            print("not understood")
            print()
            return True

    def label_loop(self):
        while True:
            idx = self.query()
            if idx is None:
                print("no instances left")
                self.commit()
                self.done = True
                return
            no_action = True
            self.comparison = self.pair(idx)
            self.spokeo_info = None
            while no_action:
                self._print_information(idx)
                no_action = self._get_action(idx)
                if no_action is None:
                    return
