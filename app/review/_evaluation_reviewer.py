import warnings
from itertools import product
import shutil
from pathlib import Path

import networkx as nx
import pandas as pd

from app.data import gen_path, load_review_link_data

from ._base_reviewer import Reviewer


class EvaluationReviewer(Reviewer):
    def __init__(
        self,
        links,
        date1,
        date2,
        data1,
        data2,
        config,
        counties,
        use_spokeo,
        max_proba,
        min_proba,
    ):
        self.date1 = date1
        self.date2 = date2
        self.counties = counties
        super().__init__(
            links,
            data1,
            data2,
            config,
            self._save_path(date1, date2, counties),
            use_spokeo=use_spokeo,
            max_proba=max_proba,
            min_proba=min_proba,
        )

    @staticmethod
    def _save_path(date1, date2, counties):
        return gen_path(date1, date2, counties=counties, filename="reviewer.pkl")

    @staticmethod
    def create(
        date1, date2, config, counties, use_spokeo=True, max_proba=1.0, min_proba=0.05
    ):
        links, data1, data2 = load_review_link_data(
            date1, date2, config, counties, voter_ids=True
        )
        voter_ids1 = data1.loc[links.index.get_level_values("left")].voter_ids.values
        voter_ids2 = data2.loc[links.index.get_level_values("right")].voter_ids.values
        links = links[voter_ids1 != voter_ids2]
        return EvaluationReviewer(
            links,
            date1,
            date2,
            data1,
            data2,
            config,
            counties,
            use_spokeo=use_spokeo,
            max_proba=max_proba,
            min_proba=min_proba,
        )


    def write_results(self):
        if not self.done:
            raise ValueError("there are still pairs left to label")
        save_path = gen_path(
            self.date1, self.date2, counties=self.counties, filename="review_links.csv"
        )
        links = [
            link[0] for link, target in zip(self.evaluated, self.target) if target == 1
        ]
        same_voter_ids = self.data1.voter_ids.reset_index().merge(self.data2.voter_ids.reset_index(), on="voter_ids")
        links.extend(zip(same_voter_ids.index_x.astype(int), same_voter_ids.index_y.astype(int)))
        links = [((left, "left"), (right, "right")) for left, right in links]
        graph = nx.from_edgelist(links)
        links = []
        for component in nx.connected_components(graph):
            left = []
            right = []
            for x, label in component:
                if label == "left":
                    left.append(x)
                elif label == "right":
                    right.append(x)
                else:
                    raise ValueError(f"unknown label {label}")
            links.extend(product(left, right))
        links = pd.DataFrame({"left": self.data1["voter_ids"].loc[[x for x, _ in links]].values, "right": self.data2["voter_ids"].loc[[y for _, y in links]].values})
        links.to_csv(save_path, index=False)

    def export(self, output_path):
        output_path = Path(output_path).expanduser() / f"{self.date1}_{self.date2}"
        output_path.mkdir(parents=True, exist_ok=True)

        links_src_path = gen_path(
            self.date1, self.date2, counties=self.counties, filename="review_links.csv"
        )

        src1_path = gen_path(self.date1, filename="parsed.csv", counties=self.counties, mkdir=True)
        src2_path = gen_path(self.date2, filename="parsed.csv", counties=self.counties, mkdir=True)

        dst1_path = output_path / f"{self.date1}.csv"
        dst2_path = output_path / f"{self.date2}.csv"
        links_dst_path = output_path / "links.csv"

        pd.read_csv(links_src_path).rename(columns={"left": self.date1, "right": self.date2}).to_csv(links_dst_path, header=True, index=False, mode="w")
        shutil.copyfile(src1_path, dst1_path)
        shutil.copyfile(src2_path, dst2_path)


    @classmethod
    def load(cls, date1, date2, counties):
        return cls._load(cls._save_path(date1, date2, counties))
