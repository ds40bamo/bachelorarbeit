from math import ceil

import jsonlines
import numpy as np
import pandas as pd
from scipy.optimize import linear_sum_assignment
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

from app.data import gen_path, load_dedupe_links


def neighborhood_growth_filter(distances, c, p=2.0):
    cuts = pd.cut(distances["left"], bins=ceil(len(distances) / 1000000))
    filtered = []
    for category in cuts.cat.categories:
        split = distances[cuts == category]
        ng = split.groupby("left")["distance"].min().reindex(split.left) * p
        smaller = split.set_index("left")["distance"] < ng
        num_smaller = smaller.groupby("left").sum()
        filtered.append(split[split.left.isin(num_smaller.index[num_smaller < c])])
    return pd.concat(filtered)


class CsSnCluster:
    def __init__(self, distances, c, k):
        distances = distances.append(
            distances.rename(columns={"left": "right", "right": "left"}),
            ignore_index=True,
        )
        distances = neighborhood_growth_filter(distances, c)
        distances.sort_values(
            ["left", "distance"], ascending=True, inplace=True, ignore_index=True
        )
        distances["next_is_same"] = (
            distances.distance.shift(-1) == distances.distance
        ) & (distances.left.shift(-1) == distances.left)
        self.grouped_distances = distances[["left", "right", "next_is_same"]].groupby(
            "left"
        )[["right", "next_is_same"]]
        self.instances = set(distances["left"].unique())
        self.max_size = k

    def __iter__(self):
        instances = self.instances
        grouped_distances = self.grouped_distances
        max_size = self.max_size
        try:
            while True:
                instance = instances.pop()
                cluster = self.biggest_cs_sn_cluster(
                    instance, grouped_distances, max_size
                )
                cluster = set(cluster.keys())
                instances.difference_update(cluster)
                if len(cluster) > 1:
                    yield cluster
        except KeyError:
            pass

    def biggest_cs_sn_cluster(self, instance, grouped_distances, max_size):
        instance_group = grouped_distances.get_group(instance)
        instance_right = instance_group.right
        instance_next_is_same = instance_group.next_is_same
        instance_found = set()
        instance_info = [0, instance_right, instance_next_is_same, instance_found]
        curr_cluster = {instance: instance_info}
        curr_len = 1
        queue = set()
        try:
            while True:
                if queue:
                    curr_node = queue.pop()
                else:
                    best_cluster = curr_cluster
                    curr_cluster = best_cluster.copy()
                    pos = instance_info[0]
                    next_neighbor = instance_right.iloc[pos]
                    queue.add(next_neighbor)
                    curr_len += 1
                    if curr_len > max_size:
                        return best_cluster
                    instance_found.add(next_neighbor)

                    last_was_same = instance_group.next_is_same.iloc[pos]
                    pos += 1

                    while last_was_same:
                        neighbor = instance_right.iloc[pos]
                        queue.add(neighbor)
                        curr_len += 1
                        if curr_len > max_size:
                            return best_cluster
                        instance_info[3].add(neighbor)
                        last_was_same = instance_group.next_is_same.iloc[pos]
                        pos += 1
                    instance_info[0] = pos
                    continue

                curr_node_group = grouped_distances.get_group(curr_node)
                curr_node_right = curr_node_group.right
                curr_node_found = set()
                curr_node_pos = 0
                cluster_nodes = set(curr_cluster.keys())
                while cluster_nodes:
                    neighbor = curr_node_right.iloc[curr_node_pos]
                    if neighbor in cluster_nodes:
                        cluster_nodes.remove(neighbor)
                    else:
                        if neighbor not in queue:
                            queue.add(neighbor)
                            curr_len += 1
                            if curr_len > max_size:
                                return best_cluster
                        curr_node_found.add(neighbor)
                    curr_node_pos += 1
                curr_node_next_is_same = curr_node_group.next_is_same
                last_was_same = curr_node_next_is_same.iloc[curr_node_pos - 1]
                while last_was_same:
                    neighbor = curr_node_right.iloc[curr_node_pos]
                    if neighbor not in queue:
                        queue.add(neighbor)
                        curr_len += 1
                        if curr_len > max_size:
                            return best_cluster
                    curr_node_found.add(neighbor)
                    last_was_same = curr_node_next_is_same.iloc[curr_node_pos]
                    curr_node_pos += 1
                for cluster_node, cluster_node_info in curr_cluster.items():
                    cluster_node_found = cluster_node_info[3]
                    if curr_node in cluster_node_found:
                        continue
                    pos = cluster_node_info[0]
                    cluster_node_right = cluster_node_info[1]
                    neighbor = cluster_node_right.iloc[pos]
                    while neighbor != curr_node:
                        if neighbor not in queue:
                            queue.add(neighbor)
                            curr_len += 1
                            if curr_len > max_size:
                                return best_cluster
                        cluster_node_found.add(neighbor)
                        neighbor = cluster_node_right.iloc[pos]
                        pos += 1

                    cluster_node_next_is_same = cluster_node_info[2]
                    last_was_same = cluster_node_next_is_same.iloc[pos]
                    pos += 1
                    while last_was_same:
                        neighbor = cluster_node_right.iloc[pos]
                        if neighbor not in queue:
                            queue.add(neighbor)
                            curr_len += 1
                            if curr_len > max_size:
                                return best_cluster
                        cluster_node_found.add(neighbor)
                        last_was_same = cluster_node_next_is_same.iloc[pos]
                        pos += 1
                    cluster_node_info[0] = pos
                curr_cluster[curr_node] = [
                    curr_node_pos,
                    curr_node_right,
                    curr_node_next_is_same,
                    curr_node_found,
                ]
        except (KeyError, IndexError):
            return best_cluster


def dedupe_cluster(date, c, k, counties=None, src_hash=None, dst_hash=None):
    """
    find duplicates based on links in the deduplication scenario

    Parameters
    ----------
    c : int
        maximum neighborhood growth of a node to be in a cluster
    k : int
        maximum size of a compact set (maximum size of a cluster)
    """
    distances = load_dedupe_links(date, counties=counties, _hash=src_hash)
    dedupe_path = gen_path(date, filename="clusters.jsonl", counties=counties, _hash=dst_hash)
    with jsonlines.open(dedupe_path, "w") as file:
        for cluster in CsSnCluster(distances, c, k):
            file.write(list(map(int, cluster)))


def load_links(date1, date2):
    out_path = gen_path(date1, date2, filename="links.csv")
    return pd.read_csv(
        out_path, dtype={"left": "uint32", "right": "uint32", "prob": float}
    )


def _connected_component_labels(links):
    nodes_left = pd.factorize(links.left)[0]
    nodes_right = pd.factorize(links.right)[0]
    min_right_node = nodes_left.max() + 1
    nodes_right += min_right_node
    row_col_num = nodes_right.max() + 1
    graph = csr_matrix(
        (np.ones(len(nodes_left)), (nodes_left, nodes_right)),
        shape=(row_col_num, row_col_num),
    )
    node_component_map = connected_components(graph, directed=False)[1]
    return node_component_map[nodes_left]


def _filter_trivial_links(links):
    only_one_link = links["component"].value_counts() == 1
    only_one_link_components = only_one_link[only_one_link].index
    selector = links.component.isin(only_one_link_components)
    trivial_links = links[selector]
    non_trivial_links = links[~selector]
    non_trivial_links = non_trivial_links.set_index(["left", "right"])
    return trivial_links, non_trivial_links


def _hungarian(links):
    links["component"] = _connected_component_labels(links)
    trivial_links, non_trivial_links = _filter_trivial_links(links)

    # batch components for faster computation (batch sizes bigger and smaller than 1000 seem to leed to higher computation time)
    cuts = pd.cut(
        non_trivial_links["component"], bins=ceil(len(non_trivial_links) / 1000)
    )
    cuts = cuts.cat.remove_unused_categories()
    filtered = [trivial_links]
    for category in cuts.cat.categories:
        split = non_trivial_links[cuts == category]
        nl, ll = pd.factorize(split.index.get_level_values("left"))
        nr, lr = pd.factorize(split.index.get_level_values("right"))
        m = csr_matrix((split.prob, (nl, nr)))
        arr = m.toarray()
        left, right = linear_sum_assignment(arr, maximize=True)
        is_not_zero = arr[left, right] != 0
        left = left[is_not_zero]
        right = right[is_not_zero]
        filtered.append(
            split.reindex(
                pd.MultiIndex.from_arrays(
                    (ll[left], lr[right]), names=split.index.names
                )
            )
            .dropna()
            .reset_index()
        )
    return pd.concat(filtered, ignore_index=True)[["left", "right", "prob"]]


def _greedy(links):
    links.sort_values(["prob", "left"], ascending=False, inplace=True)
    used_links = set()
    greedy_links = []
    columns = ["left", "right", "prob"]
    for _, left, right, prob in links[columns].itertuples():
        if not left in used_links and not right in used_links:
            greedy_links.append((left, right, prob))
            used_links.add(left)
            used_links.add(right)
    return pd.DataFrame(greedy_links, columns=columns)


link_approaches = {"hungarian": _hungarian, "greedy": _greedy}


def link_cluster(date1, date2, approach="hungarian", p=0.5):
    """
    find duplicates based on links in the database linkage scenario

    Parameters
    ----------
    approach : {'hungarian', 'greedy'}, default='hungarian'
    """
    if approach not in link_approaches:
        raise ValueError(f"approach '{approach}' not known, use one of {set(link_approaches)}")
    links = load_links(date1, date2)
    links = links[links.prob > p]

    link_path = gen_path(date1, date2, filename="clusters.jsonl")
    columns = ["left", "right"]
    found_links = link_approaches[approach](links)
    with jsonlines.open(link_path, "w") as file:
        for _, left, right in found_links[columns].itertuples():
            file.write(list(map(int, (left, right))))
