from app.config import DEFAULT_FILTER_CONFIG, DEFAULT_LINK_CONFIG
from app.index import Keys
date1 = "20130531"
date2 = "20200229"
keys = Keys.load(date1, date2)
key = keys.get(0.985)
DEFAULT_FILTER_CONFIG.set_key(key)
DEFAULT_LINK_CONFIG.set_key(key)
from app.review._train_data_reviewer import TrainDataReviewer
match_review = TrainDataReviewer.load(date1, date2, "matches")
non_match_review = TrainDataReviewer.load(date1, date2, "non_matches")
match_clf = match_review.train_classifier(DEFAULT_FILTER_CONFIG)
non_match_clf = non_match_review.train_classifier(DEFAULT_FILTER_CONFIG)
from app.classify import LinkClassifier

from app.preprocessing import clean_matches, combine_classify, combine_train
combine_classify(date1, date2)
clean_matches(date1, date2, match_clf)
combine_train(date1, date2)

from app.config import DEFAULT_LINK_CONFIG, DEFAULT_DEDUPE_CONFIG
from app.classify import LinkClassifier, DedupeClassifier
keys = Keys.load(date1, date2)
key = keys.get(0.985)
DEFAULT_DEDUPE_CONFIG.set_key(key)
DEFAULT_LINK_CONFIG.set_key(key)

i = DEFAULT_LINK_CONFIG.iterate_classify_chunks(date1, date2, chunksize=1000)
data1, data2 = next(i)

link_classifier = LinkClassifier.build(date1, date2, match_clf, non_match_clf, DEFAULT_LINK_CONFIG)
link_classifier.save()
dedupe_classifier = DedupeClassifier.build(date1, date2, match_clf, non_match_clf, DEFAULT_DEDUPE_CONFIG)
dedupe_classifier.save()
dedupe_classifier = DedupeClassifier.load(date1, date2)
link_classifier = LinkClassifier.load(date1, date2)
link_classifier.save()
from app.evaluation import Evaluation

evaluation = Evaluation(date1, date2, ["lee"], DEFAULT_LINK_CONFIG)
evaluation.export()
evaluation.link(link_classifier)
evaluation.get_reviewer().label_loop()
evaluation.evaluate(DEFAULT_DEDUPE_CONFIG, DEFAULT_LINK_CONFIG, dedupe_classifier, link_classifier, 10, 1, "hungarian", graphic=True)

gen_train = Evaluation(date1, date2, ["hol", "cal", "bak", "sum", "hen", "mon", "fra", "ind"], DEFAULT_LINK_CONFIG)
gen_train.export()
gen_train.link(link_classifier)
gen_train.get_reviewer().label_loop()
gen_train.get_reviewer().write_results()

from app.evaluation import dedupe_config1, dedupe_config2, dedupe_config3, dedupe_config4, link_config1, link_config2, link_config3, link_config4
dedupe_config1.set_key(key)
dedupe_config2.set_key(key)
dedupe_config3.set_key(key)
dedupe_config4.set_key(key)
link_config1.set_key(key)
link_config2.set_key(key)
link_config3.set_key(key)
link_config4.set_key(key)
dedupe_clf1 = gen_train.to_classifier(date1, date2, dedupe_config1)
link_clf1 = gen_train.to_classifier(date1, date2, link_config1)
dedupe_clf2 = gen_train.to_classifier(date1, date2, dedupe_config2)
link_clf2 = gen_train.to_classifier(date1, date2, link_config2)
dedupe_clf3 = gen_train.to_classifier(date1, date2, dedupe_config3)
link_clf3 = gen_train.to_classifier(date1, date2, link_config3)
dedupe_clf4 = gen_train.to_classifier(date1, date2, dedupe_config4)
link_clf4 = gen_train.to_classifier(date1, date2, link_config4)

link_classifier = LinkClassifier.build(date1, date2, match_clf, non_match_clf, link_config1)
dedupe_classifier = DedupeClassifier.build(date1, date2, match_clf, non_match_clf, dedupe_config1)

evaluation.evaluate(dedupe_config1, link_config1, dedupe_classifier, link_classifier, 3, 2, "greedy")
# 0.782   0.996
# 0.0267  0.65

evaluation.evaluate(dedupe_config1, link_config1, dedupe_classifier, link_classifier, 3, 2, "hungarian")
# 0.782   0.996
# 0.027   0.658

evaluation.evaluate(dedupe_config1, link_config1, dedupe_classifier, link_classifier, 3, 1, "greedy")
# 0.995   0.997
# 0.643   0.734

evaluation.evaluate(dedupe_config1, link_config1, dedupe_classifier, link_classifier, 3, 1, "hungarian")
# 0.995   0.997
# 0.64    0.741

evaluation.evaluate(dedupe_config1, link_config1, dedupe_clf1, link_clf1, 3, 2, "greedy")
# 0.892   0.997
# 0.0654  0.725

evaluation.evaluate(dedupe_config1, link_config1, dedupe_clf1, link_clf1, 3, 2, "hungarian")
# 0.892   0.997
# 0.0659  0.731

evaluation.evaluate(dedupe_config1, link_config1, dedupe_clf1, link_clf1, 3, 1, "greedy")
# 0.999   0.997
# 0.901   0.735

evaluation.evaluate(dedupe_config1, link_config1, dedupe_clf1, link_clf1, 3, 1, "hungarian")
# 0.999   0.997
# 0.9     0.741

evaluation.evaluate(dedupe_config2, link_config2, dedupe_clf2, link_clf2, 3, 2, "greedy")
# 0.89    0.997
# 0.0645  0.724

evaluation.evaluate(dedupe_config2, link_config2, dedupe_clf2, link_clf2, 3, 2, "hungarian")
# 0.89    0.997
# 0.0651  0.732

evaluation.evaluate(dedupe_config2, link_config2, dedupe_clf2, link_clf2, 3, 1, "greedy")
# 0.999   0.997
# 0.9     0.734

evaluation.evaluate(dedupe_config2, link_config2, dedupe_clf2, link_clf2, 3, 1, "hungarian")
# 0.999   0.997
# 0.9     0.741

evaluation.evaluate(dedupe_config3, link_config3, dedupe_clf3, link_clf3, 3, 2, "greedy")
# 0.915   0.997
# 0.0855  0.738

evaluation.evaluate(dedupe_config3, link_config3, dedupe_clf3, link_clf3, 3, 2, "hungarian")
# 0.915   0.997
# 0.0863  0.746

evaluation.evaluate(dedupe_config3, link_config3, dedupe_clf3, link_clf3, 3, 1, "greedy")
# 0.999   0.997
# 0.883   0.733

evaluation.evaluate(dedupe_config3, link_config3, dedupe_clf3, link_clf3, 3, 1, "hungarian")
# 0.999   0.997
# 0.883   0.739

evaluation.evaluate(dedupe_config4, link_config4, dedupe_clf4, link_clf4, 3, 2, "greedy")
# 0.902   0.997
# 0.072   0.723

evaluation.evaluate(dedupe_config4, link_config4, dedupe_clf4, link_clf4, 3, 2, "hungarian")
# 0.902   0.997
# 0.0726  0.731

evaluation.evaluate(dedupe_config4, link_config4, dedupe_clf4, link_clf4, 3, 1, "greedy")
# 0.999   0.997
# 0.899   0.735

evaluation.evaluate(dedupe_config4, link_config4, dedupe_clf4, link_clf4, 3, 1, "hungarian")
# 0.999   0.997
# 0.898   0.741

from app.statistics import data_statistics, date_frequency

date1 = "20130531"
date2 = "20200229"

import pandas as pd
pd.options.display.float_format = '{:.2f}'.format
results = data_statistics(date2)
date_frequency(date1)
date_frequency(date2)

# größe von cluster
# anzahl an missmatches
# für matches anzahl missmatches pro feld
