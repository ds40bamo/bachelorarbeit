#!/usr/bin/env python

from sys import argv, stdout
import subprocess
import ruamel.yaml
import sys
from pathlib import Path

def main(filepath):
    with open(filepath) as file:
        config = ruamel.yaml.safe_load(file)
    provider = config["provider"]
    hosts = set(provider["worker_ips"])
    hosts.add(provider["head_ip"])
    container_name = config["cluster_name"]
    ssh_user = config["auth"]["ssh_user"]
    login = ssh_user + '@{}'
    processes = {host: subprocess.Popen(["ssh", login.format(host), "docker", "stop", container_name], stderr=subprocess.STDOUT, stdout=subprocess.PIPE, encoding="utf-8") for host in hosts}
    for host, process in processes.items():
        process.wait()
        print("{:20}".format(host), process.stdout.read(), end="")

if __name__ == "__main__":
    try:
        filepath = argv[1]
    except IndexError:
        print(f"Usage: ./{Path(__file__).name} CLUSTER.YAML")
        exit(1)
    main(filepath)
